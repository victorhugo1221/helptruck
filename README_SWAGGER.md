# Manage endpoints in Swagger
**Remember: Don't forget to update the host in api/components/utils/BaseController.php!**

### Add a new module:
1. Add a new path in _api/modules_ with the name of your controller
2. Add a PHP file with "Controller" at the end of the name
3. Put your module in scanDir in the _backend/controllers/SwaggerController.php_ file

### Add a new endpoint in existing controller:
1. Add your new function in the controller (_api/modules/yourModule/controllers/YourController.php_)
2. Add the swagger statements at the top of function
3. Add your new endpoint in _api/config/routes_"

### Add a new controller in module:
1. Add your new controller in the path _api/modules/yourModule/controllers/_
2. Add your function
3. Add the swagger statements at the top of function
4. Add your endpoints in _api/config/routes_"