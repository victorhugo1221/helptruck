<?php
namespace grupow\rest\components;
use yii\base\Component;

class ActionStatus extends Component implements IStatus {

	protected $message;
    protected $errorCode;
	protected $meta;

	public function __construct($message, $errorCode=null, $meta=[]){
		$this->message = $message;
        $this->errorCode = $errorCode;
		$this->meta = $meta;
		return $this;
	}

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }
}