<?php
namespace grupow\rest;

use Metadata\Cache\CacheInterface;
use Metadata\ClassMetadata;
use ReflectionClass;

class SerializerCache implements CacheInterface {

	/**
     * @param string $prefix
     */
    private $prefix;

    /**
     * @var Cache $cache
     */
    private $cache;

    /**
     * @param string $prefix
     * @param Cache $cache
     */
    public function __construct($prefix, $cache)
    {
        $this->prefix = $prefix;
        $this->cache = $cache;
    }

    /**
     * {@inheritDoc}
     */
    public function loadClassMetadataFromCache(ReflectionClass $class)
    {
        $cache = $this->cache->get($this->prefix . $class->name);
        return false === $cache ? null : $cache;
    }

    /**
     * {@inheritDoc}
     */
    public function putClassMetadataInCache(ClassMetadata $metadata)
    {
        $this->cache->set($this->prefix . $metadata->name, $metadata);
    }

    /**
     * {@inheritDoc}
     */
    public function evictClassMetadataFromCache(ReflectionClass $class)
    {
        $this->cache->delete($this->prefix . $class->name);
    }

}