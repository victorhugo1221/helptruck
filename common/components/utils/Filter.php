<?php


namespace common\components\utils;


use yii\helpers\Html;
use yii\helpers\Url;

class Filter {

    private function prepareFieldsToFilter(array $fields, array $filter): string {
        $response = '';
        foreach ($fields as $key => $field) {
            $filterField = !(isset($filter) && isset($filter['field'])) ? NULL : $filter['field'];
            $selected = $this->fieldSelected($filterField, $key);
            $response .= '<option value="' . $key . '" ' . $selected . '>' . $field . '</option>';
        }
        return $response;
    }

    private function prepareOperationsToFilter(array $filter): string {
        $response = '';
        foreach ($this->getOperations() as $key => $operation) {
            $field = !(isset($filter) && isset($filter['operation'])) ? NULL : $filter['operation'];
            $selected = $this->fieldSelected($field, $key);
            $response .= '<option value="' . $key . '" ' . $selected . '>' . $operation . '</option>';
        }
        return $response;
    }

    public function showFilterField(array $fields, array $filter): string {
        $inputValue = !(isset($filter) && isset($filter['term'])) ? NULL : $filter['term'];
        $response = '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 p-0 input-group-prepend">
                         <select name="field" class="form-control filter-select">
                             <option value="">Campo</option>' .
            $this->prepareFieldsToFilter($fields, $filter) .
            '</select>
                     </div>
                     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 p-0 input-group-prepend">
                         <select name="operation" class="form-control filter-select">' .
            $this->prepareOperationsToFilter($filter) .
            '</select>
                     </div>
                     <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 p-0">
                         <input type="text" class="form-control filter-input" name="term" placeholder="Digite para filtrar" value="' . $inputValue . '" />
                     </div>';
        return $response;
    }

    public function showFieldButtons(string $urlDefault): string {
        $html = "<span class='pull-right mr-2'>
                     <a href='" . URL::to([$urlDefault . '/clear-filter'], TRUE) . "' class='btn btn-clear-filter btn-sm'>
                         <i class='fa fa-trash-alt'></i>
                         <span>Limpar</span>
                     </a>
                 </span>
                 <span class='pull-right'>
                     " . Html::submitButton('<i class="fa fa-search fa-fw"></i>Filtrar</a>', ['class' => 'btn btn-filter btn-sm']) . "
                 </span>";

        return $html;
    }

    public function getOperations() {
        return [
            'equal'     => 'Igual a',
            'startWith' => 'Começa com',
            'contain'   => 'Contém',
            'endWith'   => 'Termina com',
        ];
    }

    public function fieldSelected($field, $key) {
        return isset($field) && !empty($field) && $field == $key ? 'selected' : '';
    }
}