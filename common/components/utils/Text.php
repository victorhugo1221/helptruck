<?php

namespace common\components\utils;

class Text {
    static private $rules = [
        //singular     plural
        'ão' => 'ões',
        'ês' => 'eses',
        'm'  => 'ns',
        'l'  => 'is',
        'r'  => 'res',
        'x'  => 'xes',
        'z'  => 'zes',
    ];

    static private $exceptions = [
        'cidadão'  => 'cidadãos',
        'mão'      => 'mãos',
        'qualquer' => 'quaisquer',
        'campus'   => 'campi',
        'lápis'    => 'lápis',
        'ônibus'   => 'ônibus',
        'Perfil'   => 'Perfis',
    ];

    public static function pluralize(string $word): string {
        if (array_key_exists($word, self::$exceptions)) {
            return self::$exceptions[$word];
        } else {
            foreach (self::$rules as $singular => $plural) {
                if (preg_match("({$singular}$)", $word)) {
                    return preg_replace("({$singular}$)", $plural, $word);
                }
            }
        }

        if (substr($word, -1) !== 's') {
            return $word . 's';
        } else {
            return $word;
        }
    }

    public static function singularize(string $word): string {
        if (in_array($word, self::$exceptions)) {
            $invert = array_flip(self::$exceptions);
            return $invert[$word];
        } else {
            foreach (self::$rules as $singular => $plural) {
                if (preg_match("({$plural}$)", $word)) {
                    return preg_replace("({$plural}$)", $singular, $word);
                }
            }
        }

        if (substr($word, -1) == 's') {
            return substr($word, 0, -1);
        } else {
            return $word;
        }
    }

    public static function slugify(string $string): string {
        $list   = [
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'þ' => 'b',
            '&' => 'e',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            '/' => '-',
            ' ' => '-',
            '.' => '-',
        ];
        $string = strtr($string, $list);
        $string = preg_replace('/\(|\)/', '', $string);
        $string = preg_replace('/[\t\n]/', ' ', $string);
        $string = preg_replace('/\s{2,}/', ' ', $string);
        $string = preg_replace('/[^[:alnum:]]/', '-', $string);
        $string = preg_replace('/-{2,}/', '-', $string);
        $string = strtolower($string);
        return $string;
    }

    public static function maskCpf(string $cpf): string {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cpf);
    }

    public static function unmaskCpf(string $cpf): string {
        return preg_replace("/(\d{3}).(\d{3}).(\d{3})-(\d{2})/", "\$1\$2\$3\$4", $cpf);
    }

    public static function limitText(string $string, int $limit = 200): string {
        $contador = strlen($string);
        if ($contador >= $limit) {
            return substr($string, 0, strrpos(substr($string, 0, $limit), ' ')) . '...';
        } else {
            return $string;
        }
    }
}