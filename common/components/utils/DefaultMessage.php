<?php


namespace common\components\utils;


class DefaultMessage {

    public static function sucessOnInsert(string $page, string $article){
        return "{$page} criad{$article} com sucesso!";
    }

    public static function sucessOnUpdate(string $page, string $article){
        return "{$page} alterad{$article} com successo!";
    }

    public static function sucessOnDelete(string $page, string $article){
        return "{$page} deletad{$article} com sucesso!";
    }

    public static function errorOnInsert(string $page, string $article): string{
        return "Ocorreu um erro ao cadastrar {$article} {$page}!";
    }

    public static function errorOnUpdate(string $page, string $article){
        return "Ocorreu um erro ao alterar {$article} {$page}!";
    }

    public static function errorOnDelete(string $page, string $article){
        return "Ocorreu um erro ao deletar {$article} {$page}!";
    }

    public static function registerNotFound(string $page, string $article): string{
        $articleUCase = strtoupper($article);
        return "{$articleUCase} {$page} não foi encontrad{$article}.";
    }

    public static function theSearchDidNotReturnData():string{
        return "Não há resultado para o filtro realizado.";
    }
}