<?php

namespace common\components\utils;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

class CrudButton {
    public ActiveRecord $model;
    public string $page;
    public bool $showCreate = TRUE;
    public bool $showUpdate = TRUE;
    public bool $showDelete = TRUE;
    public string $permissionsToCreate = '';
    public string $permissionsToUpdate = '';
    public string $permissionsToDelete = '';
    public string $additionalButtons = '';

   public function showButton(bool $showButton, string $permissions):bool {
        if (!$showButton) {
            return FALSE;
        }
        if(!empty($permissions)){
            foreach (explode(';', $permissions) as $permission) {
                if(Yii::$app->user->can($permission) || Yii::$app->user->can('sysadmin')){
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function showCrudButtons(crudButton $crudButton): string {
        $html = "";
        $html .= "<div class='p-2 bd-highlight col-xs-12 col-sm-12 col-md-1 dv-btn-back'>
                      <a href=" . URL::to(['index'], TRUE) . " class='btn btn-back mb-2'><i class='fas fa-undo'></i> Voltar</a>
                  </div>
                  <div class='p-2 bd-highlight col-xs-12 col-sm-12 col-md-11 dv-btn-actions'>";

        $html .= $this->additionalButtons;

        if (!$crudButton->model->isNewRecord) {
            if ($this->showButton($crudButton->showDelete, $crudButton->permissionsToDelete)) {
                $html .= "<a href='#' data-url='" . URL::to([$crudButton->page . 'delete/' . $crudButton->model->id . ' '], TRUE) . "' 
                            class='btn btn-delete-outline form_remove col-xs-12 col-sm-12 col-md-2 col-xl-1 ml-xs-0 ml-sm-0 ml-md-2 mb-2'>
                            <i class='fa fa-ban pr-1'></i>Deletar
                          </a>";
            }

            if ($this->showButton($crudButton->showUpdate, $crudButton->permissionsToUpdate)) {
                $html .= Html::submitButton("<i class='fa fa-plus'></i> Salvar e ir para listagem", [
                    'class' => 'btn btn-save-and-back col-xs-12 col-sm-12 col-md-5 col-lg-4 col-xl-3 mb-2',
                    'name' => 'button_submit-and-back',
                    'value' => 'list'
                ]);

                $html .= Html::submitButton("<i class='fa fa-plus'></i> Salvar e continuar editando", [
                    'class' => 'btn btn-save col-xs-12 col-sm-12 col-md-5 col-lg-4 col-xl-3 ml-xs-0 ml-sm-0 ml-md-2 mb-2',
                    'name' => 'button_submit',
                    'value' => 'continue'
                ]);
            }
        } else {
            if ($this->showButton($crudButton->showCreate, $crudButton->permissionsToCreate)) {
                $html .= Html::submitButton("<i class='fa fa-plus'></i> Adicionar e ir para listagem", [
                    'class' => 'btn btn-save-and-back col-xs-12 col-sm-12 col-md-5 col-lg-4 col-xl-3 ml-xs-0 ml-sm-0 ml-md-2 mb-2',
                    'name' => 'button_submit-and-back',
                    'value' => 'list']);

                $html .= Html::submitButton("<i class='fa fa-plus'></i> Adicionar e continuar cadastrando", [
                    'class' => 'btn btn-save col-xs-12 col-sm-12 col-md-5 col-lg-4 col-xl-3 ml-xs-0 ml-sm-0 ml-md-2 mb-2',
                    'name' => 'button_submit',
                    'value' => 'continue'
                ]);
            }
        }
        $html .= "</div>";
        return $html;
    }
}