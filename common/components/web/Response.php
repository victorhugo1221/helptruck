<?php
namespace common\components\web;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\components\UI;
use common\modules\user\models\User;
use common\modules\traducao\models\Traducao;


class Response extends \yii\web\Response {

	protected function prepare()
	{

		
		if( $this->format != 'mustache' ) return parent::prepare();
		return '';
	}

	protected function i18n($content){

		switch(Yii::$app->language){
			case 'pt':
				$field_language = 'portugues';
				break;
			case 'en':
				$field_language = 'ingles';
				break;
			case 'es':
				$field_language = 'espanhol';
				break;
			default:
				$field_language = 'portugues';
				break;
		}

		$re = '/##([^#^\s]+)##/m';
		preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);

		if( !count($matches) ) return $content;

		$labels = [];
		foreach($matches as $label){ $labels[$label[1]] = true; }
		
		$translates_query = Traducao::find()
										->asArray()
										->where(['IN', 'chave', array_keys($labels)])
										->select("chave, $field_language as valor")
										->all();

		$translates = [];
		foreach($translates_query as $translate){ $translates[$translate['chave']] = $translate['valor']; }

		$content = preg_replace_callback($re, function($key) use ($translates){
			if(isset($translates[$key[1]]) && $translates[$key[1]] != '') return $translates[$key[1]];
			return $key[1];
		}, $content);

		return $content;
	}

	protected function getDefaults(){
		$csrfTokenName = Yii::$app->request->csrfParam;
		$csrfToken = Yii::$app->request->getCsrfToken();
			

		return [
    			'_csrfTokenName_' => $csrfTokenName ,
    			'_csrfToken_' => $csrfToken,
    			'meta' => [
					'title' => getenv('PROJECT_NAME'),
				],
				'chave_recaptcha' => getenv('CHAVE_RECAPTCHA'),
				'chave_secreta_recaptcha' => getenv('CHAVE_SECRETA_RECAPTCHA'),
				'chave_api_maps' => getenv('CHAVE_API_MAPS'),
				'video_url' => getenv('VIDEOURL'),
				'teste_owl' => isset($_GET['novo_scroll'])? true : false
		]; 
	}
    protected function sendContent()
    {


		$logged=0;$name_login='';$title='';$url='';$type=0;
        if(Yii::$app->request->isPost){
            $post               = Yii::$app->request->post();
            $userService     = Yii::$app->getModule('user')->get('userService');
			$users           = $userService->loginSite($post);
			if(isset($users['login'])){
				$logged             = $users['login'];
				$name_login         = $users['name'];
				$type               = $users['type'];
			}
        }

        if(isset(Yii::$app->user->identity->id) && Yii::$app->user->identity->id > 0)
        {
            $logged             = 1;
            $name_login         = Yii::$app->user->identity->name;
            $type               = Yii::$app->user->identity->type;
        }

        switch($type){
            case 3: 
                $title  = 'Minha conta';
                $url    = '/minhaconta';
                break;
            case 1: 
                $title  = 'Painel Lojista';
                $url    = '/painellojista';
                break;
            case 2: 
                $title  = 'Painel Profissional';
                $url    = '/painelprofisisonal';
                break;
		}


		// $session = new Session;
		// $session->open();

		// 	$session['login'] = [
		// 			'is-logged'         => $logged,
		// 			'name-login'        => $name_login,
		// 			'access-link_title' => $title,
		// 			'access-link_url'   => $url,
		// 	];

    	if(
    		!is_null(Yii::$app->request->get('_format'))
    		|| $this->format == 'json'
    	){


    		$this->content = json_encode(
    			array_merge(
    				[ 'base_url' => getenv('BASEURL') , 'logout' => !empty(Yii::$app->user->id) ? true : false ],
    				( Yii::$app->request->get('_all') == 1 )? $this->getDefaults() : [],
    				$this->data
    			),
    			( Yii::$app->request->get('_humans') == 1 )? JSON_PRETTY_PRINT : null
    		);

    		return parent::sendContent();    
    	}

    	if( $this->statusCode == 200 &&  $this->format != 'mustache'  ){
    		return parent::sendContent();
    	}

		$controller = Yii::$app->controller;
		if(!$controller) return parent::sendContent();

	    $module = $controller->module;
	    $template = $controller->template ?? '';

	    if( $this->statusCode != 200 ){
			$template = '404';

			$this->data = [
				'navbar' => [
					'modifiers' => ['light-mode', 'inverse-logo'],
					'is-logged'         => $logged,
					'name-login'        => $name_login,
					'access-link_title' => $title,
					'access-link_url'   => $url,
				],
	            'navigation' => [
	                'url' => '/erro',
	                'component' => 'notfound'
	            ],

				'notfoundnav'	=> [
					'modifiers'	=> [],
					'items'		=> [
						[
							'title'		=> '##navbar_menu_link_25_anos##',
							'url'		=> getenv('BASEURL').'sobre',
						],
						[
							'title'		=> '##navbar_menu_link_bravissima##',
							'url'		=> getenv('BASEURL').'bravissima/empreendimento',
						],
						[
							'title'		=> '##navbar_menu_link_properties##',
							'url'		=> getenv('BASEURL').'propriedades',
						],
						[
							'title'		=> '##navbar_menu_link_investments##',
							'url'		=> getenv('BASEURL').'investimentos',
						],
						[
							'title'		=> '##navbar_menu_link_tt##',
							'url'		=> getenv('BASEURL').'trends',
						],
						[
							'title'		=> '##navbar_menu_link_sports##',
							'url'		=> getenv('BASEURL').'investimentos',
						],
						[
							'title'		=> '##navbar_menu_link_contact##',
							'url'		=> getenv('BASEURL').'contato',
						]
					]
				],
			];
    	}else{
			// $template = 'views/partials/navbar';
			// $this->data = [
			// 	'navbar' => [
			// 		'is-logged'         => $logged,
			// 		'name-login'        => $name_login,
			// 		'access-link_title' => $title,
			// 		'access-link_url'   => $url,
			// 	],
			// ];
		}

	    $config = 	[
						'template_path' => $module->basePath . '/views/',
						'partials_path' => $module->basePath . '/views/partials/',
						'ui_path'       => Yii::getAlias('@app') . '/views/',
						'user'          => Yii::$app->user,
						'data'          => $this->data
					];


       	$siteService = Yii::$app->getModule('site')->get('siteService');
       	$url_linguagem_pt       = Url::to($siteService->trocarIdiomaUrl(Yii::$app->request->url,'pt'));
       	$url_linguagem_en       = Url::to($siteService->trocarIdiomaUrl(Yii::$app->request->url,'en'));
       	$url_linguagem_es       = Url::to($siteService->trocarIdiomaUrl(Yii::$app->request->url,'es'));

       	$idioma_atual = '##home-topo-linguagem_pt##';
       	if(Yii::$app->language == "en"){
       	    $idioma_atual = '##home-topo-linguagem_en##';
       	}elseif (Yii::$app->language == "es") {
       	    $idioma_atual = '##home-topo-linguagem_es##';
       	}

    	$this->content = UI::config( $config )
    		->defaults( array_merge([ 'base_url' => getenv('BASEURL'), 'logout' => !empty(Yii::$app->user->id) ? true : false ], $this->getDefaults()) )
    		->data(  $this->data )
    		->render( $template )
		;

		echo $this->i18n( $this->content );
    }
}