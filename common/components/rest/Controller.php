<?php

namespace common\components\rest;

use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\Response;

class Controller extends ActiveController {
	public function behaviors() {
		return [];
	}

	public function beforeAction($action) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		return parent::beforeAction($action);
	}

	public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		unset($actions['delete']);
		unset($actions['create']);
		unset($actions['update']);
		return $actions;
	}

	protected function sucesso($data = []) {
		return ['status' => 'success', 'data' => $data];
	}

	protected function erro($mensagem, $codigo = 401) {
		return ['status' => 'error', 'code' => $codigo, 'message' => $mensagem];
	}
}