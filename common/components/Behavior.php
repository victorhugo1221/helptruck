<?php

namespace common\components;

use common\modules\adminGw\models\Log;
use common\modules\adminGw\services\LogService;
use Yii;

abstract class Behavior extends \yii\base\Behavior {
	protected string $_log                = '';
	protected array  $_oldattributes      = [];
	protected array  $_noLog              = [];
	protected int    $_action             = 0;
	protected array  $_relationshipAfter  = [];
	protected array  $_relationshipBefore = [];

	public function beforeCreate($event) {
		$this->generateLogData(Log::CREATE);
	}

	public function beforeUpdate($event) {
		$this->generateLogData(Log::UPDATE);
	}

	public function afterSave($event) {
		$this->saveLog($this->owner->id, $this->owner->classTitle(), $this->_action, $this->_log);
	}

	protected function saveLog($id, $title, $action, $log) {
		if (!empty($id) && !empty($title) && !empty($action) && !empty($log)) {
			$logService = Yii::$app->getModule('admin-gw')->get('logService');
			$logService->saveLog($id, $title, $action, $log);
		}
	}

	private function generateLogData(int $action): void {
		$model = $this->owner;


		$this->_action = $action;
		$updatedFields = $model->getDirtyAttributes();
		$logData       = [];

		foreach ($updatedFields as $field => $newValue) {
			$oldValue   = $model->getOldAttribute($field);
			$fieldLabel = $model->getAttributeLabel($field);

			if ($field === 'deleted' && $oldValue != $newValue && $newValue != NULL) {
				$this->_action = Log::DELETE;
				break;
			}
			if (!empty($this->_noLog[$field]) || in_array($field, ['created', 'updated', 'deleted', 'id'])) {
				continue;
			}
			if ($this->_action === LOG::UPDATE) {
				if ($oldValue != $newValue) {
					$logData[$fieldLabel] = ['oldValue' => $oldValue, 'newValue' => $newValue];
				}
			} else {
				$logData[$fieldLabel] = ['oldValue' => NULL, 'newValue' => $newValue];
			}
		}

		if ($this->_action === Log::DELETE) {
			$modelFields = $model->getAttributes();
			$logData     = [];
			foreach ($modelFields as $fieldName => $fieldValue) {
				if (in_array($fieldName, ['created', 'updated', 'deleted', 'id'])) {
					continue;
				}
				$logData[$model->getAttributeLabel($fieldName)] = ['oldValue' => $fieldValue, 'newValue' => NULL];
			}
		} else {
			if (!empty($model->logRelatedOld) || !empty($model->logRelatedNew)) {
				$logData['Itens'] = ['oldValue' => !empty($model->logRelatedOld) ? $model->logRelatedOld : null,
				                     'newValue' => !empty($model->logRelatedNew) ? $model->logRelatedNew : null];
			}
		}

		if (!empty($logData)) {
			$this->_log = json_encode($logData);
		}
	}

}