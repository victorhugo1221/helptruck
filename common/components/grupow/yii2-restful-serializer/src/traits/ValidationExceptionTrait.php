<?php
namespace grupow\rest\traits;
use grupow\base\exceptions\ValidationException;
use Yii;

trait ValidationExceptionTrait {
    
    public $throwExceptionOnValidate = true;

    /**
     * @inheritdoc
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $valid = parent::validate($attributeNames = null, $clearErrors = true);
        if (!$valid) {
            $e = new ValidationException("Data Validation Failed.", 422, null, $this, $this->getFirstErrors());
            $e->model = &$this;
            $e->errors = $this->getFirstErrors();
            Yii::info([get_class($e->model), $e->errors], __METHOD__);
            if($this->throwExceptionOnValidate)
            	throw $e;
        }
        return $valid;
    }

}