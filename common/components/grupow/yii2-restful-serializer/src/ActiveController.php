<?php
namespace grupow\rest;
use grupow\base\exceptions\EntityNotFoundException;
use grupow\base\exceptions\ValidationException;
use grupow\rest\components\IFriendlyApiError;
use Yii;
use yii\web\Response;
class ActiveController extends \yii\rest\ActiveController {

    public function init(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        parent::init();
    }

    public function runAction($id, $params = []){
        try {
            $result = parent::runAction($id, $params);
        } catch (ValidationException $e) {
            // \Yii::$app->getResponse()->setStatusCode($e->getCode());
            return $this->serializeData($e->model);
        } catch (IFriendlyApiError $e) {
            Yii::$app->getResponse()->setStatusCode($e->getApiErrorCode(), $e->getMessage());
            return $e->getApiMessage();
        } catch (EntityNotFoundException $e) {
            Yii::$app->getResponse()->setStatusCode(404, $e->getMessage());
    		return null;
    	}
    	return $result;
    }

    protected function serializeData($data){
        return Yii::$app->serializer->serialize($data);
    }

    public function deserializeData($data, $type, $format = 'json', DeserializationContext $context = null){
        return Yii::$app->serializer->deserialize($data, $type, $format, $context);
    }

    public function actions(){
        $actions = parent::actions();
        $actions['index'] = [
            'class' => 'grupow\rest\actions\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];
        return $actions;
    }

}