<?php

namespace grupow\rest\components;

use yii\web\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * Returns the header collection.
     * The header collection contains incoming HTTP headers.
     * @return HeaderCollection the header collection
     */
    public function getHeaders()
    {
        $headers = parent::getHeaders();

        static $map = ['Authorization' => 'REDIRECT_HTTP_AUTHORIZATION'];

        if ($map !== null) {
            foreach ($map as $k => $v) {
                if (!$headers->get($k) && isset($_SERVER[$v])) {
                    $headers->set($k, $_SERVER[$v]);
                }
            }
            $map = null;
        }

        return $headers;
    }
}
