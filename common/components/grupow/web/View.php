<?php
namespace grupow\web;

class View extends \yii\web\View {

    public $bodySchema;

    protected $_pageSchemas = [
        'webpage'   => 'http://schema.org/WebPage',
        'itempage'  => 'http://schema.org/ItemPage',
    ];

    public function getBodySchema()
    {
        if ( empty($this->bodySchema) ) return;
        return ' itemscope="" itemtype="' . $this->_pageSchemas[$this->bodySchema] . '"';
    }

    public function itemProp($prop)
    {
        return 'itemprop="'.$prop.'"';
    }
}