<?php
namespace grupow\base\components;

use yii\base\BootstrapInterface;
use yii\helpers\{ArrayHelper, FileHelper, Html};
use yii\base\Component;

class Amp extends Component implements BootstrapInterface
{
	public $enable = false;
	public $disableParam = 'disable-amp';

	public function isEnabled()
	{
		return $this->enable;
	}

	public function bootstrap($app)
    {
    	if($app->request->get($this->disableParam, $this->enable))
			$this->enable = false;
    }

}
