<?php 
namespace grupow\base\components;

use yii\data\ActiveDataProvider;

class DataProvider extends ActiveDataProvider
{
	public $extraMeta;

	public function addExtraMeta($chave, $valor){
	    $this->extraMeta[$chave] = $valor;
	}
	
	public function getExtraMeta(){
        return $this->extraMeta;
    }


}
?>