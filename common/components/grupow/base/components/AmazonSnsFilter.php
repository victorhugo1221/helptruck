<?php
namespace grupow\base\components;
use Yii;
use yii\web\ForbiddenHttpException;

class AmazonSnsFilter extends AmazonFilter {

	public $sns_arn;
	public $arnVerifyEnabled = true;

	public function beforeAction($action){

		if(!isset($this->actions[$action->id]) || self::isSqsRequest($this->queue_url))
			return true;

		$headers = Yii::$app->request->getHeaders();
		Yii::trace($headers->toArray(), 'rotina');
		Yii::trace(Yii::$app->request->getRawBody(), 'rotina');

		$sns_topic_arn = $headers->get('x-amz-sns-topic-arn');

		// Verifica se esta request é de subscription
		if($this->isSubscriptionRequest($this->params)){
			// Faz incrição no SNS
			$this->client->request('GET', $this->params->SubscribeURL);
			return false;
		}

		// Valida se é o topic arn correto
		if(!$this->isCorrectArn($action->id, $sns_topic_arn)){
			
			Yii::trace('$action->id : '.$action->id, 'rotina');
			Yii::trace('$sns_topic_arn : '.$sns_topic_arn, 'rotina');
			Yii::trace('actions : '.$this->actions[$action->id], 'rotina');
			Yii::trace('sns_arn_grupo : '. Yii::$app->configuracao->get('sns_arn_grupo'), 'rotina');

			throw new ForbiddenHttpException(Yii::t('yii', 'Wrong arn'));
		}

		return true;
	}


	public function isSubscriptionRequest($params){
		return isset($params->Type) && $params->Type == 'SubscriptionConfirmation';
	}

	public function isCorrectArn($action, $sns_topic_arn){
        if(!$this->arnVerifyEnabled) return true;
        return !empty($sns_topic_arn) && $this->actions[$action] == $sns_topic_arn;
	}

}