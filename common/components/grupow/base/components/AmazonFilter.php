<?php
namespace grupow\base\components;
use Yii;
use yii\base\ActionFilter;

class AmazonFilter extends ActionFilter {

	public $actions;
	public $params;
	public $headers;
	public $queue_url;

	protected $client;

	public function __construct()
    {
    	$this->client = Yii::$container->get('GuzzleHttp\Client'); // GuzzleHttp\Client
		$request = Yii::$app->request;
		$this->headers = $request->getHeaders();
		$this->params = json_decode($request->getRawBody());
        return $this;
    }

	public static function isSqsRequest($queue_url, $hasReceipt = false){

		// Verifica se a request é do SQS
		$headers = Yii::$app->request->getHeaders();
		$sqsdQueue = $headers["x-aws-sqsd-queue"];//, queueConfig["QueueEndpoint"].(string)).
        // $sqsEndpoint = \Yii::$app->configuracao->get('grupo_sqs_url');
		$sqsEndpoint = $queue_url;
		return ($sqsdQueue == $sqsEndpoint) || $hasReceipt;
	}

	protected function getReceiptHandle(){

		$receiptHandle = $this->params->ReceiptHandle ?? null;
		if(!$receiptHandle && isset($this->headers['x-aws-sqsd-receipthandle'])){
			$receiptHandle = $this->headers['x-aws-sqsd-receipthandle'];
		}
		return $receiptHandle;
	}

}