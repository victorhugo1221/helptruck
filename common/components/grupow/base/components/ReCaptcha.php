<?php
namespace grupow\base\components;

use Yii;
use yii\base\BootstrapInterface;

class ReCaptcha extends \himiklab\yii2\recaptcha\ReCaptcha implements BootstrapInterface {

	public $config;

	public function init(){
		$this->config = Yii::$app->configuracao;
	}

	public function bootstrap($app){
		$this->siteKey = $this->config->get('recaptcha_sitekey');
		$this->secret = $this->config->get('recaptcha_secret');
	}

}