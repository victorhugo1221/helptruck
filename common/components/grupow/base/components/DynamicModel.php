<?php
namespace grupow\base\components;
class DynamicModel extends \yii\base\DynamicModel {

	protected $_labels;

	public function setAttributeLabels($labels){
		$this->_labels = $labels;
	}

	public function getAttributeLabel($name){
		return $this->_labels[$name] ?? $name;
	}

	public function addRules($ruleSet){
        foreach ($ruleSet as $key => $rule) {
            $attributes = (array) $rule[0];
            $validator = $rule[1];
            $options = array_slice($rule, 2);
            $this->addRule($attributes, $validator, $options);
        }
    }
}