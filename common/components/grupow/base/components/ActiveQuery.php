<?php
namespace grupow\base\components;
class ActiveQuery extends \yii\db\ActiveQuery {

    protected $_alias;

    public function setAlias($alias){
        $this->_alias = $alias ?: $this->_alias;
        $this->alias($this->_alias);
    }

    public function getAlias(){
        return $this->_alias;
    }

    public function defaultScope($alias=null){
        $this->setAlias($alias);
        $this->where=null;
        return $this;
    }
}