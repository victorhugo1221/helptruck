<?php
namespace grupow\base\traits;

use Yii;
use yii\web\Response;
use grupow\rest\components\ActionStatus;
use common\components\Service;
use yii\web\View;

trait ControllerAccessories {

   public function actionStatus($mensagem, $status=200, $meta=[]){
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->serializer->serialize(new ActionStatus($mensagem, $status, $meta));
    }

    public function formatErrors(){
        $service = new Service();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return call_user_func_array([$service, 'formatErrors'], func_get_args());
    }

}