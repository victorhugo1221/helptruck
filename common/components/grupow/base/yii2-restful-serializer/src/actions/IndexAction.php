<?php
namespace grupow\rest\actions;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\Action;

class IndexAction extends Action {

    public $querySearchMethod;
    public $preQueryHandler;

    /**
     * @return ActiveDataProvider
     */
    public function run()
    {

        // \Yii::$app->language = 'pt_BR';
        // echo \Yii::$app->unformatter->asDecimal('50.000.000,15',2);
        // exit;
        // var_dump(\Yii::$app->user->id);
        // var_dump(\Yii::$app->user->can('visualizarUsuario'));
        // exit;
        // if ($this->checkAccess) {
        //     call_user_func($this->checkAccess, $this->id);
        // }

        return $this->prepareDataProvider();
    }

    /**
     * Prepares the data provider that should return the requested collection of the models.
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider()
    {

        $requestParams = Yii::$app->request->get();
        $modelClass = $this->modelClass;

        $query = $modelClass::find();
        $searchMethod = $this->querySearchMethod ?? 'listSearch';
            // var_dump($searchMethod);
            // exit;
        // if (!empty($requestParams)) {
            $query->$searchMethod($requestParams);
        // }
        if(is_callable($this->preQueryHandler)){
            $handler = $this->preQueryHandler;
            $handler($query);
        }

        // $model = $query->one();
        // var_dump(get_class($model));
        // exit;

        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $query,
            'pagination' => [
                'params' => $requestParams,
            ],
            'sort' => [
                'params' => $requestParams,
            ],
        ]);
    }
}