<?php
namespace grupow\base\elasticsearch;

class Connection extends \yii\elasticsearch\Connection {
    protected function populateNodes() {
        $node = reset($this->nodes);
        $host = $node['http_address'];
        $protocol = isset($node['protocol']) ? $node['protocol'] : $this->defaultProtocol;
        if (strncmp($host, 'inet[/', 6) === 0) {
            $host = substr($host, 6, -1);
        }
        $response = $this->httpRequest('GET', "$protocol://$host/_nodes/_all/http");
        if (!empty($response['nodes'])) {
            $nodes = $response['nodes'];
        } else {
            $nodes = [];
        }

        foreach ($nodes as $key => &$node) {
            // Setor comentado para uso de HTTPS
            // if (!isset($node['http']['publish_address'])) {
            //     unset($nodes[$key]);
            // }

            // Linha alterada para funcionar com AWS
            $node['http_address'] = $node['http']['publish_address'] ?? "$host";

            // Linha alterada para user o protocolo definido pelo usuário
            $node['protocol'] = $protocol;
        }

        if (!empty($nodes)) {
            $this->nodes = array_values($nodes);
        } else {
            curl_close($this->_curl);
            throw new Exception('Cluster autodetection did not find any active nodes.');
        }
    }
}