<?
namespace grupow\base\components\calendar;

class CalendarioEvento extends Calendar
{
	protected $days = array();
	protected $events = array();
	

	function getEvents($y,$m = 0) 
	{
		global $application;
		$db = $application->db;

		//procura no banco o estilo
		$query = "SELECT * FROM tbleventos WHERE YEAR(EVEDATADE) = ".$y;

		if ($m > 0) {
			$query .= " AND MONTH(EVEDATADE) = ".$m;
		}

		$eventos  = $db->execute($query);
		$eventos->setFetchType(SqlResultSet::FetchHash);
		
		foreach($eventos as $evento) {
			$datainicio = dateConvert($evento['EVEDATADE']);
			$datafim = dateConvert($evento['EVEDATAATE']);

			$this->events[$evento['EVEIDPK']] = $evento;

			while($datainicio <= $datafim) {
				$this->days[printDate($datainicio)] =& $this->events[$evento['EVEIDPK']];
				$datainicio->setDate($datainicio->format('Y'), $datainicio->format('m'), $datainicio->format('d') + 1);
				
			}
		}

	}



	/*
		Generate the HTML for a given month
	*/
	function getMonthHTML($m, $y, $showYear = 1, $class = "")
	{
		//$this->getEvents($y,$m);
		return parent::getMonthHTML($m,$y,$showYear,$class);
	}

	/*
		Generate the HTML Pure without the administrative tools for a given month
	*/
	function getMonthHTMLPure($m, $y, $showYear = 1) 
	{
		global $application;
		$db = $application->db;

		$s = "";

		$a = $this->adjustDate($m, $y);
		$month = $a[0];
		$year = $a[1];		
		
		$daysInMonth = $this->getDaysInMonth($month, $year);
		$date = getdate(mktime(12, 0, 0, $month, 1, $year));
		
		$first = $date["wday"];
		$monthName = $this->monthNames[$month - 1];
		
		$prev = $this->adjustDate($month - 1, $year);
		$next = $this->adjustDate($month + 1, $year);
		
		if ($showYear == 1)
		{
			$prevMonth = $this->getCalendarLink($prev[0], $prev[1]);
			$nextMonth = $this->getCalendarLink($next[0], $next[1]);
		}
		else
		{
			$prevMonth = "";
			$nextMonth = "";
		}
		
		$header = $monthName . (($showYear > 0) ? " " . $year : "");
		
		$s .= "<table class=\"calendario\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$s .= "<caption>";
		$s .= "<div class=\"mcaption\">$header</div>"; 
		$s .= "</caption>";
		
		$s .= "<thead>";
		$s .= "<tr>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay)%7] . "</th>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay+1)%7] . "</th>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay+2)%7] . "</th>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay+3)%7] . "</th>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay+4)%7] . "</th>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay+5)%7] . "</th>";
		$s .= "<th class=\"quadrocal\" border=\"0\" align=\"center\" valign=\"top\" >" . $this->dayNames[($this->startDay+6)%7] . "</th>";
		$s .= "</tr>";
		$s .= "</thead>";
		
		// We need to work out what date to start at so that the first appears in the correct column
		$d = $this->startDay + 1 - $first;
		while ($d > 1)
		{
			$d -= 7;
		}
		
		// Make sure we know when today is, so that we can use a different CSS style
		$today = getdate(time());
		$s .= "<tbody>";		
		
		while ($d <= $daysInMonth)
		{
			$s .= "<tr border=\"0\">";
			
			for ($i = 0; $i < 7; $i++)
			{
				$tipoEvento = false;
				$class = ($year == $today["year"] && $month == $today["mon"] && $d == $today["mday"]) ? "calendarToday" : "calendar";

				$dayClass = '';
				
				/*if (array_key_exists(sprintf("%02d/%02d/%d",$d,$month,$year), $this->days)) {
					// evento
					$evento = $this->days[sprintf("%02d/%02d/%d",$d,$month,$year)];
					$tipoEvento = true;

					$dayClass .= 'evento';

					//$s .= "<td align=\"center\" valign=\"top\" class=\"evento\">";//<a href=\"evento.php?id=".$evento['EVEIDPK'].'">';
				}*/

				//procura no banco o estilo
				$query = "SELECT * FROM tblferiados WHERE ferdata = '".Date2Sql($d."/".$month."/".$year)."'";
				$pesquisaData  = $db->execute($query);
				$pesquisaData->setFetchType(SqlResultSet::FetchHash);
				
				if($pesquisaData->length()) {
					$dataAtual = $pesquisaData->next();

					//dia fechado
					if ($dataAtual['fertipo'] == 1) {
						$dayClass .= ' aberto';
						//$s .= "<td align=\"center\" valign=\"top\" class=\"fechado\">";
						//feriado
					} else if ($dataAtual['fertipo'] == 2) {
						$dayClass .= ' aberto feriado';
						//$s .= "<td align=\"center\" valign=\"top\" class=\"feriado\">";
					}
					
				} else {
					//dia aberto
					if ($d > 0 && $d <= $daysInMonth) {
						$dayClass .= ' fechado';
						//$s .= "<td align=\"center\" valign=\"top\" class=\"aberto\">";
					} else {
						//$s .= "<td>";
					}
				}
				
				$s .= "<td desenvolvimentostyle=\"padding: 0px!important;\" align=\"center\" valign=\"top\" class=\"$dayClass quadrocal\">";

				if ($d > 0 && $d <= $daysInMonth)
				{
					$link = $this->getDateLink($d, $month, $year);
					/*if($tipoEvento) {
						$evento = $this->days[sprintf("%02d/%02d/%d",$d,$month,$year)];
						$s .= "<a href=\"evento.php?id={$evento['EVEIDPK']}\" rel=\"lyteframe\" rev=\"width:500px;height:400px\" title=\"".encodeData($evento['EVETITULO'])."\">$d</a>";
					} else*/
						$s .= (($link == "") ? $d : "<a href=\"$link\">$d</a>");
				}
				else
				{
					$s .= "&nbsp;";
				}
			 $s .= "</td>";	   
				$d++;
			}
			$s .= "</tr>";
		}
		$s .= "</tbody>";
		
		$s .= "</table>";
		
		return $s;  	
	}

	function getYearHTML($year,$param,$colunas = 4)
	{
		//$this->getEvents($year);
		return parent::getYearHTML($year,$param,$colunas);
	}
	

}

?>
