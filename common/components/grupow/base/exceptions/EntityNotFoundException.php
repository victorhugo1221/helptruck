<?php
namespace grupow\base\exceptions;
use yii\web\NotFoundHttpException;

class EntityNotFoundException extends NotFoundHttpException {

	public function __construct($message="", $code=404){
		parent::__construct($message, $code);
	}

}