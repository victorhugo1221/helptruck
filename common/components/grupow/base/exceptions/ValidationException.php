<?php
namespace grupow\base\exceptions;
use Exception;
use Yii;

class ValidationException extends Exception {

	public $model;
	public $errors;

	public function __construct($message="", $code=422, $previous = null, &$model, $errors){
		Yii::info([get_class($model), $errors], __METHOD__);
		parent::__construct($message, $code, $previous);
	}

	public function getModel(){
		return $this->model;
	}

	public function getErrors(){
		return $this->errors;
	}

}