<?php
namespace grupow\base\widgets;

class ActiveForm extends \yii\widgets\ActiveForm {

	public $pjaxReload = '#main-content';
	public $enableAjaxValidation = true;
	public $validateOnChange = false;
	public $validateOnSubmit = true;
	public $enableClientValidation = false;
	public $validateOnBlur = false;

	public function run(){
		parent::run();
		if ($this->enableClientScript) {
			$view = $this->getView();
			$id = $this->options['id'];
	        $view->registerJs("
	        	jQuery(document).on('beforeSubmit','#$id', function (e) {
				    
				    var form = jQuery(this);
				    jQuery.ajax({
				        url: form.attr('action'),
				        type: form.attr('method'),
				        data: form.serialize(),
				        dataType: 'json',
				        complete: function (jqXHR, textStatus) {
				        	var successEvent = jQuery.Event('ajaxSubmitComplete');
				            form.trigger(successEvent);
				        },
				        success: function (msgs) {
				            var successEvent = jQuery.Event('ajaxSubmitSuccess');
				            successEvent.msgs = msgs;
				            form.trigger(successEvent);
				        },
				        error: function () {
				        	var successEvent = jQuery.Event('ajaxSubmitError');
				            form.trigger(successEvent);
				        }
				    });

				    return false;

				});");

	        if($this->pjaxReload){
		     	$view->registerJs("
				jQuery(document).on('ajaxSubmitSuccess','#$id', function (e) {
				    if(e.msgs.url !== null){
				        // window.location.href = msgs.url;
				        jQuery.pjax.reload('{$this->pjaxReload}', {'url' : e.msgs.url});
				    }
				});");
	        }
        }
	}

}