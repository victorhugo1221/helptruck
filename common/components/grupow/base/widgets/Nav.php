<?php 
namespace grupow\base\widgets;
/**
* 
*/
class Nav extends \yii\bootstrap\Nav
{
	
    /**
     * Renders widget items.
     */
    public function renderItems()
    {
        $items = '';
        foreach ($this->items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }
            $items .= $this->renderItem($item);
        }

        return $items;
    }
}


?>