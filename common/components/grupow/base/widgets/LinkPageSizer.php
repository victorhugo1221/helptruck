<?php
namespace grupow\base\widgets;
use yii\helpers\Html;
use nkovacs\pagesizer\LinkPageSizer as BaseLinkPageSizer;

class LinkPageSizer extends BaseLinkPageSizer {

	public $options = ['class' => 'form-control input-sm'];
    public $availableSizes = ['10'=>'10 por página', '20'=>'20 por página', '50'=>'50 por página', '100'=>'100 por página'];

	/**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPageSizeButtons()
    {
        if (count($this->availableSizes) === 0) {
            return '';
        }

        $buttons = [];
        $currentPageSize = $this->pagination->getPageSize();

        foreach ($this->availableSizes as $size => $label) {
            $buttons[]=$this->renderPageSizeButton($label, $size, null, $size==$currentPageSize);
        }

        return Html::tag('select', implode("\n", $buttons), $this->options);
    }

    /**
     * Renders a page size button.
     * You may override this method to customize the generation of page size buttons.
     * @param string $label the text label for the button
     * @param integer $pageSize the page size
     * @param string $class the CSS class for the page button.
     * @param boolean $active whether this page button is active
     * @return string the rendering result
     */
    protected function renderPageSizeButton($label, $pageSize, $class, $active)
    {
        $options = ['class' => $class === '' ? null : $class];
        if ($active) {
            $options = array_merge($options, ['selected'=>'selected']);
        }
        $options['value'] = $pageSize;
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page-size'] = $pageSize;

        return Html::tag('option', $label, $options);
    }

    // protected function getCurrentPerPage(){
    // 	$key = $this->options['name']??'per-page';
    // 	return \Yii::$app->request->get($key,'');
    // }

}