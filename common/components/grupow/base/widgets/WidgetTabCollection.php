<?php
namespace grupow\base\widgets;

use yii\base\Widget;

class WidgetTabCollection extends Widget {

	public $items;

	public function run(){

		$tabs = [];
		$widgets = [];

		foreach ($this->items as $name => $widget) {
			$visible = isset($widget['visible']) && $widget['visible'] == false ? false : true;
			if($visible){
				$tabs[] = $name;
				$widgets[] = $widget['widget'];
			}
		}
		
		return $this->render('_widgetCollection', [
			'tabs'    => $tabs,
			'widgets' => $widgets
		]);
	}

}