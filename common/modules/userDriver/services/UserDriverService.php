<?php

namespace common\modules\userDriver\services;

use common\components\Service;
use common\modules\userDriver\models\UserDriver;

class UserDriverService extends Service {
    public function init() {
        parent::init();
        $this->className = UserDriver::class;
    }
}