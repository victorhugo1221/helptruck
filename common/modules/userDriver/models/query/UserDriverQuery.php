<?php

namespace common\modules\userDriver\models\query;

use common\components\utils\ModelTrait;
use common\modules\userDriver\models\UserDriver;
use yii\db\ActiveQuery;

class UserDriverQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', UserDriver::tableName() . '.deleted', NULL]);
    }
    
    public function all($db = NULL) {
        return parent::all($db);
    }
    
    public function one($db = NULL) {
        return parent::one($db);
    }
    
    public function filter($search) {
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
