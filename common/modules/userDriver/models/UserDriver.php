<?php

namespace common\modules\userDriver\models;

use common\components\Model;
use common\modules\adminGw\models\Page;
use common\modules\adminGw\models\Translation;
use common\modules\userDriver\models\query\UserDriverQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 *
 * @property-read mixed $userRel00
 */
class UserDriver extends Model
{
    
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    
    public static array $_status = [
        self::STATUS_PENDENT => 'Aguardando ativação',
        self::STATUS_ACTIVE  => 'Ativo',
    ];
    
    public static function tableName()
    {
        return 'user_driver';
    }
    
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id','document_photo','selfie_document','maximum_travel_distance','average_rating', 'status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['card_number'], 'string', 'max' => 30],
            [['id'], 'unique'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'user_id'     => 'Usuário',
            'status'      => 'Status',
            'average_rating' =>'Nota',
            'created'     => 'Data de cadastro',
            'updated'     => 'Data da última atualização',
            'deleted'     => 'Data de exclusão',
        ];
    }
    
    public function getUserRel()
    {
        return $this->hasOne(UserDriver::className(), ['id' => 'user_id']);
    }
    
    public static function find()
    {
        return new UserDriverQuery(get_called_class());
    }
}