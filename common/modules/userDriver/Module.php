<?php

namespace common\modules\userDriver;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('userDriverService', __NAMESPACE__ . '\services\UserDriverService');
    }
}
