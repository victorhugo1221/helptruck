<?php

namespace common\modules\adminGw;

use Yii;
use yii\base\BootstrapInterface;
use yii\console\Application;

class Module extends \yii\base\Module implements BootstrapInterface {

	public $controllerNamespace = __NAMESPACE__ . '\controllers';

	public function bootstrap($app) {
		$config = Yii::$app->configuracao;
		if ($app instanceof Application) {
			$this->controllerNamespace = __NAMESPACE__ . '\commands';
			return;
		}

		if (!Yii::$app->user->isGuest) {
			$app->homeUrl = $this->get('authenticationService')->getHomeUrl(Yii::$app->user);
		}
	}

	public function init() {
		parent::init();

		$this->set('authenticationService', __NAMESPACE__ . '\services\AuthenticationService');
		$this->set('tokenService', __NAMESPACE__ . '\services\TokenService');
		$this->set('emailService', __NAMESPACE__ . '\services\EmailService');
		$this->set('permissionService', __NAMESPACE__ . '\services\PermissionService');
		$this->set('translationService', __NAMESPACE__ . '\services\TranslationService');
		$this->set('pageService', __NAMESPACE__ . '\services\PageService');
		$this->set('logService', __NAMESPACE__ . '\services\LogService');

	}
}
