<?php

namespace common\modules\adminGw\behaviors;

use common\components\Behavior;
use common\modules\adminGw\models\Page;

class PageDependenciesBehavior extends Behavior {
    
    
    public function init() {
        parent::init();
    }
    
    public function events() {
        return [
            Page::EVENT_BEFORE_INSERT     => 'beforeCreate',
            Page::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            Page::EVENT_AFTER_INSERT      => 'afterSave',
            Page::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
}