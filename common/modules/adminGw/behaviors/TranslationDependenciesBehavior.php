<?php

namespace common\modules\adminGw\behaviors;

use common\components\Behavior;
use common\modules\adminGw\models\Translation;

class TranslationDependenciesBehavior extends Behavior {
    
    
    public function init() {
        parent::init();
    }
    
    public function events() {
        return [
            Translation::EVENT_BEFORE_INSERT     => 'beforeCreate',
            Translation::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            Translation::EVENT_AFTER_INSERT      => 'afterSave',
            Translation::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
}