<?php

namespace common\modules\adminGw\models;

use common\modules\adminGw\models\query\AuthAssignmentQuery;
use Yii;
use yii\db\ActiveRecord;

class AuthAssignment extends ActiveRecord
{
    public static function tableName()
    {
        return 'auth_assignment';
    }
    
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['created_at'], 'integer'],
            [['item_name', 'user_id'], 'string', 'max' => 64],
            [['item_name', 'user_id'], 'unique', 'targetAttribute' => ['item_name', 'user_id']],
            [['item_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['item_name' => 'name']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('app.authentication.model', 'Item Name'),
            'user_id' => Yii::t('app.authentication.model', 'User ID'),
            'created_at' => Yii::t('app.authentication.model', 'Created At'),
        ];
    }
    
    public static function find()
    {
        return new AuthAssignmentQuery(get_called_class());
    }
    
    public function getItemRel() {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name']);
    }
}
