<?php

namespace common\modules\adminGw\models;

use common\components\Model;
use common\modules\adminGw\behaviors\TranslationDependenciesBehavior;
use common\modules\adminGw\models\query\TranslationQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

class Translation extends Model
{
    const LANGUAGE_PT = 1;
    const LANGUAGE_EN = 2;
    const LANGUAGE_ES = 3;
    
    public static $column_language = [
        self::LANGUAGE_PT => 'pt',
        self::LANGUAGE_EN => 'en',
        self::LANGUAGE_ES => 'es'
    ];
    
    public static $column_language_id = [
        'pt' => self::LANGUAGE_PT,
        'en' => self::LANGUAGE_EN,
        'es' => self::LANGUAGE_ES
    ];
    
    public static function tableName()
    {
        return 'translation';
    }

    public function rules()
    {
        return [
            [['pt', 'en', 'es'], 'string'],
            [['editable_by_customer'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['key'], 'string', 'max' => 255],
            [['key'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'      => 'Id',
            'key'     => 'Chave',
            'pt'      => 'Português',
            'en'      => 'Inglês',
            'es'      => 'Espanhol',
            'created' => 'Data de Cadastro',
            'updated' => 'Data da última atualização',
            'deleted' => 'Data da exclusão',
        ];
    }

    public function behaviors()
    {
        return [
            'dependencies' => [
                'class' => TranslationDependenciesBehavior::class,
            ],
            'timestamp'    => [
                'class'      => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    self::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value'      => date('Y-m-d H:i:s'),
            ],
        ];
    }

    public static function find()
    {
        return new TranslationQuery(get_called_class());
    }

    public static function getArrayOfLanguages(): array{
        return [self::$column_language[self::LANGUAGE_PT], self::$column_language[self::LANGUAGE_EN], self::$column_language[self::LANGUAGE_ES]];
    }
}
