<?php

namespace common\modules\adminGw\models;

use common\modules\adminGw\models\query\TokenQuery;
use common\modules\user\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class Token extends ActiveRecord {
    
    const ACTIVE = 0;
    const RECOVER = 1;
    const PRIVATE_KEY = 2;
    const PUBLIC_KEY = 3;
    const SEARCH = 4;
    const INACTIVE = 5;
    
    public static $_alias = [
        self::ACTIVE => 'Ativação',
        self::RECOVER => 'Recuperação',
        self::PRIVATE_KEY => 'Chave Privada',
        self::PUBLIC_KEY => 'Chave Pública',
        self::SEARCH => 'Pesquisa',
        self::INACTIVE => 'Inativo',
    ];
    
    public static function tableName() {
        return 'token';
    }
    
    public function rules() {
        return [
            [['user_id', 'uid'], 'required'],
            [['user_id', 'active', 'type', 'last_nonce'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['uid'], 'string', 'max' => 36],
        ];
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'Id do usuário',
            'uid' => 'UID',
            'active' => 'Ativo',
            'type' => 'Tipo',
            'last_nonce' => 'Data de ativação',
            'created' => 'Data de criação',
            'updated' => 'Data de atualização',
            'deleted' => 'Data de expiração',
        ];
    }
    
    public static function find() {
        return new TokenQuery(get_called_class());
    }
    
    public function getUserRel() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function __toString() {
        return (string)$this->uid;
    }
}
