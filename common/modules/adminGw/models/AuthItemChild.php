<?php

namespace common\modules\adminGw\models;

use common\modules\adminGw\models\query\AuthItemChildQuery;
use yii\db\ActiveRecord;

class AuthItemChild extends ActiveRecord {
    
    public static function tableName() {
        return 'auth_item_child';
    }
    
    public function rules() {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'string', 'max' => 64],
            [['parent', 'child'], 'unique', 'targetAttribute' => ['parent', 'child']],
            [['child'], 'exist', 'skipOnError' => TRUE, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['child' => 'name']],
            [['parent'], 'exist', 'skipOnError' => TRUE, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['parent' => 'name']],
        ];
    }
    
    public function attributeLabels() {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
        ];
    }
    
    public function getChildRel() {
        return $this->hasOne(AuthItem::className(), ['name' => 'child']);
    }
    
    public function getParentRel() {
        return $this->hasOne(AuthItem::className(), ['name' => 'parent']);
    }
    
    public static function find() {
        return new AuthItemChildQuery(get_called_class());
    }
}
