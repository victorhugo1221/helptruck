<?php

namespace common\modules\adminGw\models;

use common\components\Model;
use common\modules\adminGw\behaviors\PageDependenciesBehavior;
use common\modules\adminGw\models\query\PageQuery;
use yii\behaviors\TimestampBehavior;

class Page extends Model {
	public static function tableName() {
		return 'page';
	}

	public function rules() {
		return [
			[['identifier', 'url', 'title', 'order_in_menu', 'user_created'], 'required'],
			[['show_in_menu', 'order_in_menu', 'parent', 'user_created'], 'integer'],
			[['permissions'], 'string'],
			[['created', 'updated', 'deleted'], 'safe'],
			[['identifier'], 'string', 'max' => 45],
			[['url', 'title', 'icon'], 'string', 'max' => 100],
		];
	}

	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'identifier'    => 'Identificador',
			'url'           => 'URL',
			'title'         => 'Título (singular)',
			'show_in_menu'  => 'Exibir no menu?',
			'order_in_menu' => 'Ordem no menu',
			'permissions'   => 'Permissões',
			'parent'        => 'Menu pai',
			'icon'          => 'Ícone',
			'user_created'  => 'Usuário que criou o menu',
			'created'       => 'Data de Cadastro',
			'updated'       => 'Data da última atualização',
			'deleted'       => 'Data da exclusão',
		];
	}

	public function behaviors() {
		return [
			'dependencies' => [
				'class' => PageDependenciesBehavior::class,
			],
			'timestamp'    => [
				'class'      => TimestampBehavior::class,
				'attributes' => [
					static::EVENT_BEFORE_INSERT => ['created', 'updated'],
					static::EVENT_BEFORE_UPDATE => ['updated'],
				],
				'value'      => date('Y-m-d H:i:s'),
			],
		];
	}

	public static function find() {
		return new PageQuery(get_called_class());
	}

	public static function findChildMenu() {
		return Page::find()->andWhere(['>', 'parent', 0])->andWhere(['>', 'show_in_menu', 0])->orderBy(['order_in_menu' => SORT_ASC])->all();
	}

	public static function findParentMenu() {
		return Page::find()->andWhere(['parent' => NULL])->andWhere(['>', 'show_in_menu', 0])->orderBy(['order_in_menu' => SORT_ASC])->all();
	}
}
