<?php

namespace common\modules\adminGw\models;

use common\components\Model;
use common\modules\adminGw\models\query\LogQuery;
use common\modules\user\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Log extends Model {
	const CREATE = 1;
	const UPDATE = 2;
	const DELETE = 3;

	public static $_status
		= [
			self::CREATE => 'Cadastro',
			self::UPDATE => 'Alteração',
			self::DELETE => 'Exclusão',
		];

	public static function tableName() {
		return 'log';
	}

	public function rules() {
		return [
			[['entity_type', 'entity_id', 'action', 'data'], 'required'],
			[['user_id', 'entity_id', 'action'], 'integer'],
			[['data'], 'string'],
			[['created'], 'safe'],
			[['entity_type'], 'string', 'max' => 45],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'user_id'     => 'Usuário',
			'entity_type' => 'Entidade',
			'entity_id'   => 'Entidade',
			'action'      => 'Ação',
			'data'        => 'Dados',
			'created'     => 'Gerado em',
		];
	}

	public function getUserRel() {
		return $this->hasOne(User::class, ['id' => 'user_id']);
	}

	public static function find() {
		return new LogQuery(get_called_class());
	}
}
