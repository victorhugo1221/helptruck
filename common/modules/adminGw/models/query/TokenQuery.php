<?php

namespace common\modules\adminGw\models\query;

use common\modules\adminGw\models\Token;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\adminGw\models\Token]].
 *
 * @see \common\modules\adminGw\models\Token
 */
class TokenQuery extends ActiveQuery {
	/*public function active()
	{
			return $this->andWhere('[[status]]=1');
	}*/

	/**
	 * {@inheritdoc}
	 * @return Token[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return Token|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}
