<?php

namespace common\modules\adminGw\models\query;

use common\modules\adminGw\models\AuthAssignment;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\adminGw\models\AuthAssignment]].
 *
 * @see \common\modules\adminGw\models\AuthAssignment
 */
class AuthAssignmentQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AuthAssignment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AuthAssignment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
