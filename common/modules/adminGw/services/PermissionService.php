<?php

namespace common\modules\adminGw\services;

use common\components\Service;
use common\modules\adminGw\models\Log;
use common\modules\adminGw\models\AuthAssignment;
use common\modules\adminGw\models\AuthItemChild;
use common\modules\user\models\User;
use Yii;
use function Webmozart\Assert\Tests\StaticAnalysis\string;

class PermissionService extends Service {
    private $restrictedPermissions;

    public function init() {
        parent::init();
        $this->restrictedPermissions = explode(';', $_ENV['RESTRICTED_PERMISSIONS']);
        $user                        = new User();
        if ($user->userLoggedIsGw()) {
            $this->restrictedPermissions = "";
        }
    }

    public function getPermissionsListbox(): array {
        $arrResponse = [];
        $allParent   = AuthItemChild::find()->select(['parent'])->andWhere(['not in', 'parent', $this->restrictedPermissions])->orderBy(['parent' => SORT_ASC])->all();
        foreach ($allParent as $parent) {
            $parentName     = $parent->parentRel->description;
            $allChildParent = AuthItemChild::find()->select(['child'])->andWhere(['parent' => $parent])->andWhere(['not in', 'child', $this->restrictedPermissions])->all();

            foreach ($allChildParent as $child) {
                $childName                               = $child->childRel->description;
                $arrResponse[$parentName][$child->child] = $childName;
            }
            asort($arrResponse[$parentName]);
        }

        ksort($arrResponse);
        return $arrResponse;
    }

    public function getPermissionsByUserListbox(?int $userId = 0, bool $filterRestrictedPermission = TRUE): array {
        if ($userId == 0) {
            return [];
        }
        $arrResponse = [];

        if ($filterRestrictedPermission) {
            $userPermissions = AuthAssignment::find()->andWhere(['user_id' => $userId])->andWhere(['not in', 'item_name', $this->restrictedPermissions])->all();
        } else {
            $userPermissions = AuthAssignment::find()->andWhere(['user_id' => $userId])->all();
        }


        foreach ($userPermissions as $permission) {
            $permissionName                      = $permission->itemRel->description;
            $arrResponse[$permission->item_name] = $permissionName;
        }
        asort($arrResponse);
        return $arrResponse;
    }

    public function save(int $userId, array $permissions, array $oldPermissions = []): bool {
        $user = new User();
        if (!$user->userLoggedIsGw() && !empty($oldPermissions)) {
            foreach ($oldPermissions as $oldPermission) {
                if (in_array($this->restrictedPermissions, $oldPermission)) {
                    $permissions[] = $oldPermission;
                } else {
                    $parent = (AuthItemChild::find()->select(['parent'])->andWhere(['child' => $oldPermission])->one())->parent;
                    if (!empty($parent)) {
                        if (in_array($this->restrictedPermissions, $parent)) {
                            $permissions[] = $parent;
                        }
                    }
                }
            }
        }
        $result = TRUE;
        AuthAssignment::deleteAll(['user_id' => $userId]);
        foreach ($permissions as $permission) {
            $model             = new AuthAssignment();
            $model->item_name  = $permission;
            $model->user_id    = (string)$userId;
            $model->created_at = time();
            $result            = $result && $model->save();
        }
        if (!$result) {
            AuthAssignment::deleteAll(['user_id' => $userId]);
        }
        return $result;
    }

    public function saveLog(int $id, int $action, ?array $oldPermissions, ?array $newPermissions): void {
        $log                   = Log::find()->where(['entity_id' => $id, 'entity_type' => 'Administrador', 'action' => $action])->orderBy(['id' => SORT_DESC])->one();
        if ($log && !empty($log)) {
            $logData               = json_decode($log->data, TRUE);
            $logData['Permissões'] = ['oldValue' => $oldPermissions, 'newValue' => $newPermissions];
            $log->data             = json_encode($logData);
            $log->save();
        }
    }
}