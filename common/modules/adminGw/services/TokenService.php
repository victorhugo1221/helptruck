<?php

namespace common\modules\adminGw\services;

use common\components\Service;
use common\modules\user\models\User;
use common\modules\adminGw\models\Token;
use common\modules\adminGw\exceptions\TokenException;
use Exception;
use Ramsey\Uuid\Uuid;
use Yii;

class TokenService extends Service {

	public function __construct() {
        $this->className = Token::class;
	}

	public function createToken($user, $type) {
		$model          = Yii::createObject(Token::class); // new Token;
		$model->user_id = $user->id;
		$model->uid     = Uuid::uuid1()->toString();
		$model->type    = $type;

		// Validação status de usuário
		$this->validadeUserStatus($user->status, $type);

		$this->validate($model);
		$model->save();
		return $model;
	}

	public function validadeUserStatus($statusUser, $typeToken) {

		if ($typeToken == Token::ACTIVE && $statusUser != User::STATUS_PENDENT) {
			throw new TokenException("Status de usuário inválido para geração de token");
		} elseif ($typeToken == Token::RECOVER && ($statusUser == User::STATUS_ADMIN_INACTIVE || $statusUser == User::STATUS_SELF_INACTIVE)) {
			throw new TokenException("Status de usuário inválido para geração de token");
		} elseif (($typeToken == Token::PUBLIC_KEY || $typeToken == Token::PRIVATE_KEY) && $statusUser != User::STATUS_ACTIVE) {
			throw new TokenException("Status de usuário inválido para geração de token");
		}

		return true;
	}

	/**
	 * Método para verificar se o token está válido
	 *
	 * @return Token
	 */
	public function isValidToken($uid, $model = null, $allowNoExpiration = false) {

		if ($uid && !$model) {
			$model = Token::findOne(['uid' => $uid]);
		}
		// \Yii::info('$model->active '.$model->active);
		// \Yii::info('$this->verificarPeriodoAtivacao '.$this->verificarPeriodoAtivacao($model, $allowNoExpiration));
		// \Yii::info('$model->userRel->status '.$model->userRel->status);
		if (
			$model &&
			$model->active == Token::ACTIVE &&
			$this->verifyActivationPeriod($model, $allowNoExpiration) &&
			($model->userRel->status == User::STATUS_ACTIVE || $model->type == Token::ACTIVE)
		) {
			return $model;
		}

		return false;
	}

	public function getUserByToken($uid, $type) {
		$token = $this->getToken($uid, $type);
		$user  = null;
		if ($this->isValidToken($uid, $token, true)) {
			// $user = $this->findOne(Usuario::className(), $token->user_id);
			$user = $token->userRel;
		}
		return $user;
	}

	public function updateLastNonce($token, $nonce) {
		$token->last_nonce = $nonce;
		$token->save(false);
		return $token;
	}

	public function getToken($uid, $type) {
		return Token::findOne(['uid' => $uid, 'type' => $type]);
	}

	public function getPrivateKeyUserId($id) {
		return $this->getTokenByUserId($id, Token::PRIVATE_KEY);
	}

	public function getPublicKeyUserId($id) {
		return $this->getTokenByUserId($id, Token::PUBLIC_KEY);
	}

	public function getTokenByUserId($id, $type) {
		$token = Token::find()->where(['user_id' => $id, 'type' => $type, 'active' => Token::ACTIVE])->orderBy(['id' => SORT_DESC])->one();
		return $token;
	}

	public function verifyActivationPeriod($model, $allowNoExpiration = false) {
		if ($allowNoExpiration && empty($model->deleted)) {
			return true;
		}

		$expiration_date = strtotime($model->deleted);
		if ($expiration_date > strtotime('now')) {
			return true;
		}

		return false;
	}

	public function inativarChaveApi($id) {
		$this->inactivateTokenByUserId($id, Token::PRIVATE_KEY);
		$this->inactivateTokenByUserId($id, Token::PUBLIC_KEY);
	}

	public function gerarChaveApi($user) {
		$db = Yii::$app->db;
		try {

			$transaction = $db->beginTransaction();

			if (!$user->privateKeyRel || !$this->isValidToken(null, $user->privateKeyRel, true)) {
				if ($user->privateKeyRel) {
					$user->privateKeyRel->delete();
				}
				$token          = $this->createToken($user, Token::PRIVATE_KEY);
				$token->deleted = null;
				$token->save(false);
			}

			if (!$user->publicKeyRel || !$this->isValidToken(null, $user->publicKeyRel, true)) {
				if ($user->publicKeyRel) {
					$user->publicKeyRel->delete();
				}
				$token          = $this->createToken($user, Token::PUBLIC_KEY);
				$token->deleted = null;
				$token->save(false);
			}

			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollBack();
			throw $e;
		}

		return $user;
	}

	public function inactivateTokenByUserId($id, $type = Token::ACTIVE) {

		$tokens = Token::find()->where(['user_id' => $id, 'type' => $type])->orderBy(['id' => SORT_DESC])->all();

		try {
			foreach ($tokens as $token) {
				$this->inativarToken($token);
			}
			return $token ?? null;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function inativarTokenByUid($uid) {

		$model = Token::findOne(['uid' => $uid]);

		try {
			$this->inativarToken($model);
			return $model;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function inativarToken($model) {

		$model->active  = Token::INACTIVE;
		$model->deleted = date('Y-m-d H:i:s');
		$model->save();
		return $model;

	}

	public function removerTokensVencidos() {

		$ontem = date('Y-m-d', strtotime(date('Y-m-d')) - 86400);

		if (!preg_match("/[0-9]{2}:[0-9]{2}:[0-9]{2}$/", $ontem)) {
			$ontem = "$ontem 23:59:59";
		}

		$condicao = [
			'OR',
			// ['=', 'active', !Token::active],
			['<=', 'deleted', $ontem],
		];
		$tokens   = Token::find()->where($condicao)->all();
		if (count($tokens)) {
			foreach ($tokens as $token) {
				Yii::info("Removendo token: #" . $token->uid, 'application');
				$token->delete();
			}
		}
	}
}