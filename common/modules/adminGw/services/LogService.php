<?php

namespace common\modules\adminGw\services;

use common\components\Service;
use common\modules\adminGw\models\Log;
use Yii;

class LogService extends Service {

    public function __construct() {
        $this->className = Log::class;
        return $this;
    }

    public function saveLog(int $entity_id, string $entity_type, int $action, string $data, int $user_id = null) {
        $log = new Log();
        if (!empty($user_id)) {
            $log->user_id = $user_id;
        } elseif (Yii::$app->id == $_ENV['PROJECT_ID'] . '-backend') {
            $log->user_id = Yii::$app->user->identity->id;
        }
        $log->entity_id = $entity_id;
        $log->entity_type = $entity_type;
        $log->action = $action;
        $log->data = $data;
        $log->save();
    }
}