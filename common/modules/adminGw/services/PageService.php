<?php

namespace common\modules\adminGw\services;

use common\components\Service;
use common\modules\adminGw\models\Page;

class PageService extends Service {
    public function init() {
        parent::init();
	    $this->className = Page::class;
    }
}