<?php

namespace common\modules\adminGw\services;

use common\components\Service;
use common\components\utils\Text;
use common\modules\user\models\LoginForm;
use common\modules\user\models\User;
use common\modules\adminGw\models\Token;
use common\modules\user\models\UserClient;
use Exception;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\UnauthorizedHttpException;

class AuthenticationService extends Service {

    public function __construct() {
        $this->className = User::class;
        return $this;
    }

    public function loginAdministrator($login, bool $validatePassword = TRUE) {
        $identity = User::find()
                        ->andWhere(['email' => $login->email])
                        ->andWhere(['is_administrator' => User::IS_ADMINISTRATOR])
                        ->one();

        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }

        return $this->login($login, $identity, $validatePassword);
    }

    public function loginApp($login, bool $validatePassword = TRUE) {
        $identity = User::find()
                        ->innerJoin(UserClient::tableName(), UserClient::tableName() . '.user_id = user.id')
                        ->andFilterWhere(['or',
                                          ['email' => $login->login],
                                          ['cpf' => Text::unmaskCpf($login->login)]
                                         ])
                        ->one();
        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }

        return $this->login($login, $identity, $validatePassword);
    }

    private function login($login, $identity, bool $validatePassword = TRUE): ?User {
        Yii::$app->user->logout();
        try {
            $validPassword = (!$validatePassword || Yii::$app->getSecurity()->validatePassword($login->password, $identity->password));
        } catch (InvalidArgumentException | Exception $e) {
            $validPassword = FALSE;
        }

        if ($identity && $validPassword && in_array($identity->status, [User::STATUS_ACTIVE])) {
            Yii::$app->user->login($identity);
            $returnUrl = Yii::$app->user->getReturnUrl() ?: $this->getHomeUrl();
            $login->setReturnUrl($returnUrl);

            return $identity;
        }
        return NULL;
    }

    public function validatePassword(int $userId, string $password): int {
        try {
            $validatePassword = FALSE;
            $user             = User::findIdentity($userId);
            if ($user) {
                try {
                    $validPassword = (Yii::$app->getSecurity()->validatePassword($password, $user->password));
                } catch (InvalidArgumentException | Exception $e) {
                    $validPassword = FALSE;
                }
                return $validPassword ? 200 : 401;
            }
            return 400;
        } catch (Exception $ex) {
            return 500;
        }
    }

    public function findIdentityByAccessToken($token, $type = NULL) {
        try {

            $db           = Yii::$app->db;
            $transaction  = $db->beginTransaction();
            $tokenService = Yii::$app->getModule('admin-gw')->get('tokenService');
            $ex           = explode('/', trim($token));
            $accessToken  = $ex[0];
            $nonce        = $ex[1] ?? -1;
            $xApiKey      = Yii::$app->request->getHeaders()->get('X-Api-Key');

            $identity = $tokenService->getUserByToken($xApiKey, Token::PUBLIC_KEY);

            if (!$identity) {
                return;
            }

            $privateKey = $tokenService->getPrivateKeyUsuarioId($identity->id);
            if (!$privateKey) {
                return;
            }

            $master    = TRUE; //\Yii::$app->authManager->checkAccess($identity->id, 'grupow');
            $hash      = hash('sha256', $privateKey->uid . '/' . $xApiKey . '/' . $nonce);
            $publicKey = $tokenService->getToken($xApiKey, Token::PUBLIC_KEY);
            if (!$master && $nonce < $publicKey->last_nonce) {
                throw new UnauthorizedHttpException("Invalid nonce: must be higher than previous");
                return;
            }
            if ($accessToken !== $hash) {
                throw new UnauthorizedHttpException("Invalid credentials: hash does not match");
                return;
            }
            $tokenService->updateLastNonce($publicKey, $nonce);
            $transaction->commit();

        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $identity->trigger(User::EVENT_SUCCESS_LOGIN);

        return $identity;
    }

    public function getHomeUrl($user = NULL) {

        $homeUrl    = Yii::$app->homeUrl;
        $sistemaUrl = getenv('BASE_URL_SISTEMA');
        $siteUrl    = getenv('BASE_URL');

        if (Yii::$app->user->can('grupow')) {
            $homeUrl = $sistemaUrl . Url::to(['/dashboard']);
        } else {
            $homeUrl = Url::to(['/user/default/painel'], TRUE);
        }

        return $homeUrl;
    }
}