<?php

namespace common\modules\adminGw\logs;

use common\modules\aws\models\SqsMessage;
use stdClass;
use Yii;
use yii\web\Request;

class GrupoWLog {
    
    public const TYPE_API = 1;
    public const TYPE_APP = 2;
    public const TYPE_PANEL = 3;
    public const MAXIMUM_MESSAGE_SIZE_ACCEPTED_BY_AWS = 260000; //In bytes
    
    public static $_type = [
        self::TYPE_API   => 'API',
        self::TYPE_APP   => 'APP',
        self::TYPE_PANEL => 'PANEL',
    ];
    
    public static function findConfiguration() : array {
        $dbName = $_ENV['DB_LOG_NAME'];
        $softwareId = $_ENV['DB_LOG_SOFTWARE_ID'];
        
        $config = Yii::$app->dbLog->createCommand("
            SELECT {$dbName}.software.save_error_log, 
                   {$dbName}.software.save_log 
            FROM {$dbName}.software 
            WHERE {$dbName}.software.id = {$softwareId} AND 
                  {$dbName}.software.status > 0
        ")->queryOne();
        
        return [
            'saveErrorLog' => $config['save_error_log'],
            'saveLog'      => $config['save_log'],
        ];
    }
    
    public static function insertLogInDatabase(object $body) : bool {
        return Yii::$app->dbLog->createCommand()->insert('log', [
            'software_id' => $_ENV['DB_LOG_SOFTWARE_ID'],
            'origin_desc' => (isset($body->origin_desc) && !empty($body->origin_desc)) ? $body->origin_desc : NULL,
            'origin'      => (isset($body->origin) && !empty($body->origin)) ? $body->origin : NULL,
            'method'      => (isset($body->method) && !empty($body->method)) ? $body->method : NULL,
            'header'      => (isset($body->header) && !empty($body->header)) ? $body->header : NULL,
            'param'       => (isset($body->param) && !empty($body->param)) ? json_encode($body->param) : NULL,
            'httpCode'    => (isset($body->httpCode) && !empty($body->httpCode)) ? $body->httpCode : NULL,
            'device'      => (isset($body->device) && !empty($body->device)) ? $body->device : NULL,
            'ip_address'  => (isset($body->ip_address) && !empty($body->ip_address)) ? $body->ip_address : NULL,
            'response'    => (isset($body->response) && !empty($body->response)) ? $body->response : NULL,
            'log_date'    => (isset($body->log_date) && !empty($body->log_date)) ? $body->log_date : date('Y-m-d H:i:s'),
            'created'     => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    public static function insertErrorLogInDatabase(object $body) : bool {
        return Yii::$app->dbLog->createCommand()->insert('error', [
            'software_id'    => $_ENV['DB_LOG_SOFTWARE_ID'],
            'origin_desc'    => (isset($body->origin_desc) && !empty($body->origin_desc)) ? $body->origin_desc : NULL,
            'origin'         => (isset($body->origin) && !empty($body->origin)) ? $body->origin : NULL,
            'method'         => (isset($body->method) && !empty($body->method)) ? $body->method : NULL,
            'header'         => (isset($body->header) && !empty($body->header)) ? $body->header : NULL,
            'param'          => (isset($body->param) && !empty($body->param)) ? json_encode($body->param) : NULL,
            'device'         => (isset($body->device) && !empty($body->device)) ? $body->device : NULL,
            'ip_address'     => (isset($body->ip_address) && !empty($body->ip_address)) ? $body->ip_address : NULL,
            'exception'      => (isset($body->exception) && !empty($body->exception)) ? $body->exception : NULL,
            'exception_date' => (isset($body->exception_date) && !empty($body->exception_date)) ? $body->exception_date : date('Y-m-d H:i:s'),
            'created'        => date('Y-m-d H:i:s'),
        ])->execute();
    }
    
    public static function sendMessageLogToSqs(Request $YiiAppRequest, string $response, int $type, int $httpCode, string $ipAddress = NULL, string $device = NULL, string $origin = NULL) : bool {
        $response = FALSE;
        $body = new stdClass();
        $body->origin_desc = $origin;
        $body->origin = $type;
        $body->method = $YiiAppRequest->getMethod();
        $body->header = $YiiAppRequest->getHeaders()['authorization'];
        $body->param = $YiiAppRequest->getBodyParams();
        $body->httpCode = $httpCode;
        $body->device = $device;
        $body->ip_address = $ipAddress;
        $body->response = 'Response: ' . $response;
        $body->log_date = date('Y-m-d H:i:s');
        $response = self::insertLogInDatabase($body);
        return $response;
    }
    
    public static function sendMessageErrorLogToSqs(Request $YiiAppRequest, string $exception, int $type, string $ipAddress = NULL, string $device = NULL, string $origin = NULL) : bool {
        $response = FALSE;
        $body = new stdClass();
        $body->origin_desc = $origin;
        $body->origin = $type;
        $body->method = $YiiAppRequest->getMethod();
        $body->header = $YiiAppRequest->getHeaders()['authorization'];
        $body->param = $YiiAppRequest->getBodyParams();
        $body->device = $device;
        $body->ip_address = $ipAddress;
        $body->exception = $exception;
        $body->exception_date = date('Y-m-d H:i:s');
        $response = self::insertErrorLogInDatabase($body);
        return $response;
    }
}