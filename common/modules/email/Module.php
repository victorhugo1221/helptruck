<?php

namespace common\modules\email;

class Module extends \yii\base\Module 
{
    public $controllerNamespace = 'common\modules\email\controllers';

    public function init()
    {
        parent::init();

        $this->set('emailService', [
            'class' => 'common\modules\email\services\EmailService',
        ]);
    }
}