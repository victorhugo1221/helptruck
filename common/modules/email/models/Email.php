<?php

namespace common\modules\email\models;

class Email {
    public string  $senderEmail    = '';
    public string  $senderName     = '';
    public string  $subject        = '';
    public string  $template       = '';
    public array   $data           = [];
    public string  $recipientEmail = '';
    public string  $recipientName  = '';
    public ?string $cc             = null;
    public ?string $cco            = null;
    public ?string $attachment     = null;

    public function setTemplate(string $template): void {
        $this->template = $template;
    }

    public function setSubject(string $subject): void {
        $this->subject = $subject;
    }

    public function setSenderName(string $senderName = null): void {
        $this->senderName = $senderName ?? $_ENV['SENDER_NAME'];
    }

    public function setSenderEmail(string $senderEmail = null): void {
        $this->senderEmail = $senderEmail ?? $_ENV['SENDER_EMAIL'];
    }

    public function setRecipientName(string $recipientName): void {
        $this->recipientName = $recipientName;
    }

    public function setRecipientEmail(string $recipientEmail): void {
        $this->recipientEmail = $recipientEmail;
    }

    public function setData(array $data): void {
        $this->data = $data;
    }

    public function setCc(string $cc): void {
        $this->cc = $cc;
    }

    public function setCco(string $cco): void {
        $this->cco = $cco;
    }

    public function setAttachment(string $attachment): void {
        $this->attachment = $attachment;
    }

    public function getTemplate(): string {
        return $this->template;
    }

    public function getSubject(): string {
        return $this->subject;
    }

    public function getSenderName(): string {
        return $this->senderName;
    }

    public function getSenderEmail(): string {
        return $this->senderEmail;
    }

    public function getRecipientName(): string {
        return $this->recipientName;
    }

    public function getRecipientEmail(): string {
        return $this->recipientEmail;
    }

    public function getData(): array {
        return $this->data;
    }

    public function getCco(): ?string {
        return $this->cco;
    }

    public function getCc(): ?string {
        return $this->cc;
    }

    public function getAttachment(): ?string {
        return $this->attachment;
    }


    public function configureEmail(string $subject, string $template, string $recipientEmail, string $recipientName,
                                   array $data, string $attachment = null, string $cc = null, string $cco = null, string $senderEmail = null, string $senderName = null): void {

        $this->setSenderEmail($senderEmail);
        $this->setSenderName($senderName);

        if (!empty($cc)) {
            $this->setCc($cc);
        }

        if (!empty($cco)) {
            $this->setCco($cco);
        }

        if (!empty($attachment)) {
            $this->setAttachment($attachment);
        }

        if (!empty($data)) {
            $this->setData($data);
        }

        $this->setSubject($subject);
        $this->setTemplate($template);
        $this->setRecipientEmail($recipientEmail);
        $this->setRecipientName($recipientName);
    }

    public function getConfiguration(): array {
        $config = [];
        $config['senderEmail'] = $this->getSenderEmail();
        $config['senderName'] = $this->getSenderName();
        $config['subject'] = $this->getSubject();
        $config['template'] = $this->getTemplate();
        $config['data'] = $this->getData();
        $config['recipientEmail'] = $this->getRecipientEmail();
        $config['recipientName'] = $this->getRecipientName();
        $config['cc'] = $this->getCc();
        $config['cco'] = $this->getCco();
        $config['attachment'] = $this->getAttachment();
        return $config;
    }

}