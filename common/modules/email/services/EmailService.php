<?php

namespace common\modules\email\services;

use common\modules\email\models\Email;
use Exception;
use Yii;
use yii\web\Response;
use common\components\Service;
use function unlink;

class EmailService extends Service {

    public function __construct() {
        return $this;
    }

    public function createConfigurationToSendEmail(string $subject, string $template, string $recipientEmail, string $recipientName,
                                   array $data = null, string $attachment = null, string $cc = null, string $cco = null, string $senderEmail = null, string $senderName = null): Email {
        $email = new Email();
        $email->configureEmail($subject, $template, $recipientEmail, $recipientName, $data, $attachment, $cc, $cco, $senderEmail, $senderName);
        return $email;
    }

    public function sendEmail(Email $email):bool {
        try {
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($email->getTemplate(), $email->getConfiguration());
            $emailSend->setFrom([$email->getSenderEmail() => $email->getSenderName()]);
            $emailSend->setTo($email->getRecipientEmail());
            $emailSend->setSubject($email->getSubject());

            if (!empty($email->getCc())) {
                $emailSend->setCc($email->getCc());
            }

            if (!empty($email->getCco())) {
                $emailSend->setBcc($email->getCco());
            }

            if (!empty($email->getAttachment())) {
                $this->setAttachment($email->getAttachment());
            }
            $response = $emailSend->send();
            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
}