<?php
use yii\helpers\Url;
?>
<div>
    <p style="padding-bottom: 20px;">Olá <?=$name?>,</p>
    <p>Você foi cadastrado na <?= getenv('NOME_PROJETO') ?>.</p>
    <p><a href="<?=Url::to(['/change-password/'.$token], true);?>" style="color: #371322;font-weight: bold;text-decoration: none;padding: 30px 0px 30px 0px;text-decoration: underline;">Clique aqui</a> para definir sua senha.</p>
</div>
