<?php

namespace common\modules\payment\models\query;
use common\components\utils\ModelTrait;
use common\modules\payment\models\Payment;

class PaymentQuery extends \yii\db\ActiveQuery {
    
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Payment::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
    public function all($db = null)
    {
        return parent::all($db);
    }

   
    public function one($db = null)
    {
        return parent::one($db);
    }
}
