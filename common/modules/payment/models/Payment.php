<?php

namespace common\modules\payment\models;

use common\modules\payment\models\query\PaymentQuery;
use Yii;


class Payment extends \yii\db\ActiveRecord
{
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    
    public static array $_status = [
        self::STATUS_PENDENT => 'Aguardando ativação',
        self::STATUS_ACTIVE  => 'Ativo',
    ];
    
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'user_id', 'response', 'complaint_id'], 'required'],
            [['price'], 'number'],
            [['user_id', 'method', 'complaint_id', 'status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['response'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'user_id' => 'User ID',
            'method' => 'Method',
            'response' => 'Response',
            'complaint_id' => 'Complaint ID',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

   
    public static function find()
    {
        return new PaymentQuery(get_called_class());
    }
}
