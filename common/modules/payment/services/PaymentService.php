<?php

namespace common\modules\payment\services;

use common\components\Service;
use common\modules\payment\models\Payment;

class PaymentService extends Service {
    public function init() {
        parent::init();
        $this->className = Payment::class;
    }
}