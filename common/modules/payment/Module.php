<?php

namespace common\modules\payment;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('paymentService', __NAMESPACE__ . '\services\PaymentService');
    }
}