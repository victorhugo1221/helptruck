<?php

namespace common\modules\user\models;

use Yii;
use yii\base\Model;

class LoginForm extends Model {
	public $email;
	public $return_url;
	public $address;
	public $password;
	public $type;
	public $user;

	private $_user;
    
    public function __construct() {
        parent::__construct();
    }

	public function rules(): array {
		return [
			[['password', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => TRUE],
			[['email', 'password'], 'required'],
			['password', 'validatesIfUserExists'],
		];
	}

	public function validatesIfUserExists($attribute): bool {
		if (!$this->hasErrors()) {
			$user = $this->getUserActiveByEmail();
			if (!$user) {
				return false;
			}
		}
		return true;
	}

	public function getUserActiveByEmail(): ?User {
		if (empty($this->_user)) {
			$this->_user = User::find()->andWhere(['email' => $this->email])->andWhere(['status' => User::STATUS_ACTIVE])->one();
		}
		return $this->_user;
	}

	public function getEmail(): string {
		return $this->email;
	}

	public function setEmail(string $email) {
		$this->email = $email;
		return $this;
	}

	public function getReturnUrl() {
		return $this->return_url;
	}

	public function setReturnUrl($return_url) {
		$this->return_url = $return_url;
		return $this;
	}

	public function getUser(): ?User {
		return $this->user;
	}

	public function setUser(User $user) {
		$this->user = $user;
		return $this;
	}
}
