<?php

namespace common\modules\user\models;

use common\components\Model;
use common\modules\adminGw\models\Page;
use common\modules\adminGw\models\Translation;
use common\modules\user\models\query\UserClientQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class UserClient extends Model
{

    public static function tableName()
    {
        return 'user_client';
    }

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['card_number'], 'string', 'max' => 30],
            [['user_id'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id'     => 'Usuário',
            'card_number' => 'Carteirinha',
            'status'      => 'Status',
            'created'     => 'Data de cadastro',
            'updated'     => 'Data da última atualização',
            'deleted'     => 'Data de exclusão',
        ];
    }

    public function getUserRel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function find()
    {
        return new UserClientQuery(get_called_class());
    }
}
