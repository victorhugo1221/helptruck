<?php

namespace common\modules\user\models;

use Yii;
use yii\base\Model;
use common\modules\user\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class NewPasswordForm extends Model {
    
    const SCENARIO_LOGGED = 'scenarioLogged';
    
    public $actual_password;
    public $password;
    public $confirmation;
    
    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['password', 'confirmation'], 'trim'],
            [['actual_password'], 'validatedActualPassword', 'on' => self::SCENARIO_LOGGED],
            [['actual_password'], 'required', 'on' => self::SCENARIO_LOGGED],
            [['password'], 'required'],
            [['password'], 'string', 'min' => 6],
            [['confirmation'], 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => FALSE, 'message' => 'A confirmação de senha deve ser igual à senha'],
        ];
    }
    
    public function validatedActualPassword($attribute, $params) {
        
        $hash = User::find()->andWhere(['id' => Yii::$app->user->id])->select(['password'])->column()[0] ?? NULL;
        if (!Yii::$app->getSecurity()->validatePassword($this->$attribute, $hash)) {
            $this->addError($attribute, 'Senhas não são iguais');
        }
    }
    
    public function attributeLabels() {
        return [
            'actual_password' => 'Senha atual',
            'password' => 'Senha',
            'confirmation' => 'Confirmar senha',
        ];
    }
    
}
