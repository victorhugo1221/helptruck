<?php

namespace common\modules\user\models\query;

use common\components\utils\ModelTrait;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use yii\db\ActiveQuery;

class UserClientQuery extends ActiveQuery {
    use ModelTrait;

    public function init() {
        return $this->andWhere(['IS', UserClient::tableName() . '.deleted', NULL]);
    }
    
    public function all($db = NULL) {
        return parent::all($db);
    }
    
    public function one($db = NULL) {
        return parent::one($db);
    }
    
    public function filter($search) {
        
        if (!empty($search['status']) || (isset($search['status']) && $search['status'] === "0")) {
            $this->where(['=', 'user_client.status', $search['status']]);
            $search['status'] = NULL;
        }
        
        $this->innerJoinWith('userRel');
        if (!empty($search['term']) && !empty($search['field']) && !empty($search['operation'])) {
            switch ($search['field']) {
                case 'cpf':
                case 'name':
                case 'email':
                    $search['field'] = 'user.' . $search['field'];
                    break;
            }
        }

        $response = $this->defaultFilter($search);
        return $response;
    }
}
