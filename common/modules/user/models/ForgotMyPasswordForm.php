<?php

namespace common\modules\user\models;

use Yii;
use common\modules\user\models\User;
use yii\base\Model;

class ForgotMyPasswordForm extends Model {
    public $email;

    public function rules() {
        return [
            // username and password are both required
            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], function ($attribute, $params, $validator) {
                $user = User::find()->where(['email' => $this->$attribute])->one();
                if (isset($user) && in_array($user->status, [User::STATUS_PENDENT, User::STATUS_ADMIN_INACTIVE, User::STATUS_SELF_INACTIVE])) {
                    $message = 'O e-mail informado está inativo, entre em contato com a nossa central de relacionamento.';
                    $this->addError($attribute, $message);
                }
            }],
            [['email'], 'exist', 'targetClass' => User::className(), 'filter' => function ($query) {
                $query->andWhere(['status' => [User::STATUS_ACTIVE, User::STATUS_PENDENT]]);
            }, 'message' => 'O e-mail informado não é válido.'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'email' => 'E-mail',
        ];
    }
    
}
