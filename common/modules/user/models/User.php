<?php

namespace common\modules\user\models;

use common\components\Model;
use common\components\utils\Validation;
use common\modules\adminGw\models\Page;
use common\modules\adminGw\models\Token;
use common\modules\user\behaviors\UserDependenciesBehavior;
use common\modules\user\models\query\UserQuery;
use common\modules\userNotification\models\UserNotification;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\web\IdentityInterface;

class User extends Model implements IdentityInterface {
    
    private $auth_key;
    protected $_permissions;
    public    $new_permissions;
    
    const EVENT_SUCCESS_LOGIN = 'eventSuccessLogin';
    const EVENT_NEW_PASSWORD = 'onNewPassword';
    const EVENT_PASSWORD_RECOVERY = 'onPasswordRecovery';
    
    public const IS_NOT_ADMINISTRATOR = 0;
    public const IS_ADMINISTRATOR = 1;
    
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    public const STATUS_SELF_INACTIVE = -1;
    public const STATUS_ADMIN_INACTIVE = -2;
    
    public static $_status = [
        self::STATUS_PENDENT => 'Aguardando ativação',
        self::STATUS_ACTIVE => 'Ativo',
        self::STATUS_ADMIN_INACTIVE => 'Bloqueado pelo Administrador',
        self::STATUS_SELF_INACTIVE => 'Desativado pelo usuário',
    ];

    public static $_statusId = [
        self::STATUS_PENDENT,
        self::STATUS_ACTIVE,
        self::STATUS_ADMIN_INACTIVE,
        self::STATUS_SELF_INACTIVE,
    ];
    
    public static function tableName() {
        return 'user';
    }
    
    public function rules() {
        return [
            [['is_administrator'], 'boolean'],
            [['name', 'cpf', 'email'], 'required'],
            [['status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['cpf'], 'string', 'min' => 11, 'max' => 11],
            [['cpf'], function ($attribute) {
                if (!Validation::validateCpf($this->cpf)) {
                    $this->addError($attribute, 'CPF inválido');
                }
            }],
            [['cellphone'], 'string', 'max' => 11],
            [['email'], 'string', 'max' => 255],
            [['security_code'], 'string', 'max' => 6],
            [['password'], 'string', 'max' => 250],
            [['email'], 'unique'],
            [['cpf'], 'unique', 'message' => 'O {attribute} já está cadastrado como administrador ou cliente.'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'id' => 'Id',
            'is_administrator' => 'Usuário administrador',
            'name' => 'Nome',
            'cpf' => 'CPF',
            'email' => 'Email',
            'cellphone' => 'Celular',
            'security_code' => 'Código',
            'birth_date' => 'Data de Aniversário',
            'password' => 'Senha',
            'status' => 'Status',
            'created' => 'Data de Cadastro',
            'updated' => 'Data da última atualização',
            'deleted' => 'Data de exclusão',
        ];
    }
    
    public function beforeValidate() {
        $this->cpf = preg_replace('/[^[:digit:]]/', '', $this->cpf);
        $this->cellphone = preg_replace('/[^[:digit:]]/', '', $this->cellphone);
        return parent::beforeValidate();
    }
    
    public function behaviors() : array {
        return [
            'dependencies' => [
                'class' => UserDependenciesBehavior::class,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    self::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }
    
    public function getUserClientRel() : ActiveQuery {
        return $this->hasOne(UserClient::class, ['user_id' => 'id']);
    }

    public static function find() {
        return new UserQuery(get_called_class());
    }

    public function getTokens() {
        return $this->hasMany(Token::class, ['user_id' => 'id']);
    }
    
    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }
    
    public static function findIdentityByAccessToken($token, $type = NULL) {
        $authService = Yii::$app->getModule('adminGw')->get('authenticationService');
        $user = $authService->findIdentityByAccessToken($token, $type);
        return $user;
    }
    
    public function getId() {
        return $this->id ?? 0;
    }
    
    public function getAuthKey() {
        return $this->auth_key;
    }
    
    public function validateAuthKey($authKey) : bool {
        return $this->getAuthKey() === $authKey;
    }
    
    public function getPermissions() {
        if ($this->_permissions === NULL) {
            $this->_permissions = Yii::$app->getModule('adminGw')->get('permissionService')->getPermissionsByUserListbox($this->id);
        }
        return $this->_permissions;
    }
    
    public function getUsersGw() : array {
        $users = User::find()->select(['id'])->andWhere(['like', 'email', '%@grupow%', FALSE])->all();
        $arrUsers = [];
        foreach ($users as $user) {
            $arrUsers[] = $user->id;
        }
        return $arrUsers;
    }
    
    public function userLoggedIsGw() : bool {
        return in_array(Yii::$app->user->identity->id, $this->getUsersGw());
    }
    
    public function userIsGw($id) : bool {
        return in_array($id, $this->getUsersGw());
    }
}
