<?php

namespace common\modules\user\behaviors;

use common\components\Behavior;
use common\modules\user\models\User;
use common\modules\adminGw\models\Token;
use common\modules\adminGw\models\AuthItem;
use Yii;

class UserDependenciesBehavior extends Behavior {
    public       $email;
    public       $token;
    public array $booleanFields = ['status'];
    
    
    public function init() {
        $this->email = $this->email ?? Yii::$app->getModule('user')->get('emailService');
        $this->token = $this->token ?? Yii::$app->getModule('admin-gw')->get('tokenService');
        parent::init();
    }
    
    public function events() {
        return [
            User::EVENT_BEFORE_INSERT     => 'beforeCreate',
            User::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            User::EVENT_NEW_PASSWORD      => 'onNewPassword',
            User::EVENT_PASSWORD_RECOVERY => 'onPasswordRecovery',
            User::EVENT_AFTER_INSERT      => 'afterCreate',
            User::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
    
    public function onNewPassword($event) {
        $this->token->inactivateTokenByUserId($event->sender->id);
    }
    
    public function onPasswordRecovery($event) {
        $this->token->inactivateTokenByUserId($event->sender->id, Token::RECOVER);
        $token = $this->token->createToken($event->sender, Token::RECOVER);
        $this->email->recoveryPassword($event->sender, $token->uid);
    }
    
    public function beforeCreate($event) {
        parent::beforeCreate($event);
        $event->sender->cpf = preg_replace('/[^0-9]/', '', $event->sender->cpf);
    }
    public function beforeUpdate($event) {
        parent::beforeUpdate($event);
        $event->sender->cpf = preg_replace('/[^0-9]/', '', $event->sender->cpf);
    }
    
    public function afterSave($event) {
        $this->saveLog($this->owner->id, 'Administrador', $this->_action, $this->_log);
    }
    
    public function afterCreate($event){
        parent::afterSave($event);
        
        if($event->sender->is_administrator > 0){
            $this->token->inactivateTokenByUserId($event->sender->id, Token::RECOVER);
            $token = $this->token->createToken($event->sender, Token::RECOVER);
            $this->email->activateUser($event->sender, $token->uid);
        }
        
    }
}