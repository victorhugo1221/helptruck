<?php

namespace common\modules\user\services;

use common\modules\user\models\ForgotMyPasswordForm;
use common\components\Service;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use common\modules\user\models\NewPasswordForm;
use common\modules\adminGw\models\Token;
use Exception;
use Yii;
use yii\db\ActiveRecord;

class BaseService extends Service {
    public function requestNewPassword(ForgotMyPasswordForm $form) {
        $db = Yii::$app->db;
        try {
            $transaction = $db->beginTransaction();
            $user        = User::findONe(['email' => $form->email]);
            $user->trigger(User::EVENT_PASSWORD_RECOVERY);
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $user;
    }
    
    public function recoveryPassword(NewPasswordForm $form, Token $token) {
        $user = $token->userRel;
        return $this->recoveryUserPassword($form, $user);
    }
    
    public function recoveryUserPassword(NewPasswordForm $form, User $user) {
        $user->password = Yii::$app->security->generatePasswordHash($form->password);
        $db             = Yii::$app->db;
        try {
            $transaction = $db->beginTransaction();
            $user->detachBehavior('dependencies');
            if ($user->status == User::STATUS_PENDENT) {
                $user->status = User::STATUS_ACTIVE;
            }
            $user->save(FALSE, ['password', 'status']);
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $user;
    }
    
    public function logout() {
        Yii::$app->user->logout();
        Yii::$app->session->destroy();
    }
    
    public function manageToken($model): Token {
        $tokenService = $tokenService ?? Yii::$app->getModule('admin-gw')->get('tokenService');
        $tokenService->inactivateTokenByUserId($model->id, Token::RECOVER);
        $token = $tokenService->createToken($model, Token::RECOVER);
        return $token;
    }
    
    public function sendEmailToActivateUser($model, $token): void {
        $emailService = $emailService ?? Yii::$app->getModule('user')->get('emailService');
        $emailService->activateUser($model, $token->uid);
    }
}