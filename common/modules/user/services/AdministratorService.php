<?php

namespace common\modules\user\services;

use common\modules\user\models\User;
use yii\db\ActiveRecord;

class AdministratorService extends BaseService {
    public function createAdministrator($model): ActiveRecord {
        $model->is_administrator = User::IS_ADMINISTRATOR;
        $model->status = (empty($model->status) || $model->status != User::STATUS_ACTIVE) ? User::STATUS_PENDENT : $model->status;
        return $this->create($model);
    }
    
    public function updateAdministrator($model): bool {
        return $this->update($model);
    }
    
    public function find() {
        return User::find()->andWhere(['>', User::tableName() . '.status', -1])->andWhere(['is_administrator' => User::IS_ADMINISTRATOR]);
    }
}