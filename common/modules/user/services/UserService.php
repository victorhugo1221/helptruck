<?php

namespace common\modules\user\services;

use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use Exception;
use Yii;
use yii\db\ActiveRecord;

class UserService extends BaseService {
    public function find() {
        return UserClient::find()->andWhere(['>', User::tableName() . '.status', -1])->andWhere(['is_administrator' => User::IS_NOT_ADMINISTRATOR]);
    }

    public function findById($id) {
        return UserClient::find()->andWhere(['user_id' => $id])->one();
    }
    
    public function updateClient(User $user, UserClient $userClient): bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $saveUser = $this->update($user);
            $saveUserClient = $this->update($userClient);
            
            if ($saveUser && $saveUserClient) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function deleteClient(int $id): bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = User::findOne($id);
            $model->deleted = date('Y-m-d h:m:s');
            $model->status = User::STATUS_ADMIN_INACTIVE;
            $deleteUser = $model->save();
            
            $modelUserClient = UserClient::find()->andWhere(['user_id' => $id])->one();
            $modelUserClient->deleted = date('Y-m-d h:m:s');
            $modelUserClient->status = User::STATUS_ADMIN_INACTIVE;
            $deleteUserClient = $modelUserClient->save();
            
            if ($deleteUser && $deleteUserClient) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}

