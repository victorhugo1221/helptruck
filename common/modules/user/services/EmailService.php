<?php

namespace common\modules\user\services;

use common\components\Service;
use common\modules\user\models\User;
use Exception;
use Yii;

class EmailService extends Service {
    protected $sender;
    protected $sender_name;
    
    public function __construct() {
        $this->sender = $_ENV['SENDER_EMAIL'];
        $this->sender_name = $_ENV['SENDER_NAME'];
    }
    
    public function recoveryPassword($user, $token) {
        $template = 'layouts/recoverPassword';
        
        try {
            $subject = 'Recuperar Senha - ' . $_ENV['PROJECT_NAME'];
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name' => $user->name,
                    'token' => $token,
                ]
            )
                ->setFrom([$this->sender => $this->sender_name])
                ->setTo($user->email)
                ->setSubject($subject)
                ->send();
            
            
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return FALSE;
    }
    
    public function activateUser($user, $token) {
        $template = 'layouts/activateUser';
        
        try {
            $subject = 'Bem-vindo a ' . $_ENV['PROJECT_NAME'];
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name' => $user->name,
                    'token' => $token,
                ]
            )
                ->setFrom([$this->sender => $this->sender_name])
                ->setTo($user->email)
                ->setSubject($subject)
                ->send();
            
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return FALSE;
    }
    
    public function getUserLoggedId() {
        return !Yii::$app->user->isGuest ? Yii::$app->user->id : NULL;
    }
}