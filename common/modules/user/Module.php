<?php

namespace common\modules\user;

use yii\base\BootstrapInterface;

/**
 * log module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface {
    
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\user\controllers';
    
    public function bootstrap($app) {
        $app->urlManager->addRules([
            'login' => 'user/default/login',
            'logout' => 'user/default/logout',
            'forgot-my-password' => 'user/default/forgot-my-password',
            'change-password/<token:[\\w-]+>' => 'user/default/change-password',
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        
        $this->set('userService', ['class' => 'common\modules\user\services\userService',]);
        $this->set('administratorService', ['class' => 'common\modules\user\services\administratorService',]);
        $this->set('emailService', ['class' => 'common\modules\user\services\EmailService']);
    }
}
