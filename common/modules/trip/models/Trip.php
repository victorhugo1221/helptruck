<?php

namespace common\modules\trip\models;

use common\modules\trip\models\query\TripQuery;
use Yii;

/**
 * This is the model class for table "trip".
 *
 * @property int $id
 * @property int $user_client_id
 * @property int $user_driver_id
 * @property int $subcategory_id
 * @property float $price
 * @property string $receipt_photo
 * @property string $start_photo
 * @property string $end_photo
 * @property string $start_point
 * @property string $end_point
 * @property string|null $acceptance_date
 * @property string|null $refusal_date
 * @property string|null $cancellation_date
 * @property string|null $start_date
 * @property string|null $end_date
 * @property int $time_trip
 * @property int $status
 * @property string $created
 * @property string $updated
 * @property string|null $deleted
 */
class Trip extends \yii\db\ActiveRecord
{
    
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    public const STATUS_PAID = 2;
    public const STATUS_START_OK = 3;
    public const STATUS_END_OK = 4;
    
    public static array $_status = [
        self::STATUS_PENDENT => 'Aguardando Motoristas',
        self::STATUS_ACTIVE  => 'Motorista Confirmado',
        self::STATUS_PAID  => 'Pagamento Confirmado',
        self::STATUS_START_OK  => 'Iniciou Viagem',
        self::STATUS_END_OK  => 'Finalizou Viagem',
    ];
    public static function tableName()
    {
        return 'trip';
    }

    public function rules()
    {
        return [
            [['user_client_id', 'user_driver_id', 'subcategory_id', 'price', 'receipt_photo', 'start_photo', 'end_photo', 'start_point', 'end_point', 'time_trip'], 'required'],
            [['user_client_id', 'user_driver_id', 'subcategory_id', 'time_trip', 'status'], 'integer'],
            [['price'], 'number'],
            [['receipt_photo', 'start_photo', 'end_photo', 'start_point', 'end_point'], 'string'],
            [['acceptance_date', 'refusal_date', 'cancellation_date', 'start_date', 'end_date', 'created', 'updated', 'deleted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_client_id' => 'User Client ID',
            'user_driver_id' => 'User Driver ID',
            'subcategory_id' => 'Subcategory ID',
            'price' => 'Preco',
            'receipt_photo' => 'Receipt Photo',
            'start_photo' => 'Start Photo',
            'end_photo' => 'End Photo',
            'start_point' => 'Local de inicio',
            'end_point' => 'Local de Destino',
            'acceptance_date' => 'Acceptance Date',
            'refusal_date' => 'Refusal Date',
            'cancellation_date' => 'Cancellation Date',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'time_trip' => 'Time Trip',
            'status' => 'Status',
            'created' => 'Data criacao',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }
    
    public static function find()
    {
        return new TripQuery(get_called_class());
    }
}
