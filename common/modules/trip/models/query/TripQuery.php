<?php

namespace common\modules\trip\models\query;

use common\components\utils\ModelTrait;
use Yii;
use yii\db\ActiveQuery;
use common\modules\trip\models\Trip;

class TripQuery extends \yii\db\ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Trip::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        $response = $this->defaultFilter($search);
        return $response;
    }
    public function all($db = null)
    {
        return parent::all($db);
    }
    
    public function one($db = null)
    {
        return parent::one($db);
    }
}
