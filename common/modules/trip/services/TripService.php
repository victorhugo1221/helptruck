<?php

namespace common\modules\trip\services;

use common\components\Service;
use common\modules\trip\models\Trip;

class TripService extends Service {
    public function init() {
        parent::init();
        $this->className = Trip::class;
    }
}