<?php

namespace common\modules\plan\behaviors;

use common\components\Behavior;
use common\modules\plan\models\Plan;

class PlanBehavior extends Behavior {
    
    
    public function init() {
        parent::init();
    }
    
    public function events() {
        return [
            Plan::EVENT_BEFORE_INSERT     => 'beforeCreate',
            Plan::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            Plan::EVENT_AFTER_INSERT      => 'afterSave',
            Plan::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
}