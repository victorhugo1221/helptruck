<?php

namespace common\modules\trip;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('tripService', __NAMESPACE__ . '\services\TripService');
    }
}