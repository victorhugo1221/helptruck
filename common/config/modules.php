<?php
return [
    'adminGw' => [
        'class' => 'common\modules\adminGw\Module',
    ],
    'user' => [
        'class' => 'common\modules\user\Module'
    ],
    'email' => [
        'class' => 'common\modules\email\Module'
    ],
    'trip' => [
        'class' => 'common\modules\trip\Module'
    ],
    'payment' => [
        'class' => 'common\modules\payment\Module'
    ],
    'user-driver' => [
        'class' => 'common\modules\userDriver\Module'
    ]
    
];