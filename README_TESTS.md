# Testing with Codeception
- Download codeception.phar and codeception.bat from https://codeception.com/install and add the files in the php directory (c:\xampp\php)
- Download **chromedriver** for your version of google chrome at: (https://sites.google.com/a/chromium.org/chromedriver/downloads)
- Add your executable on Windows path (if use Windows OS)

##Generate acceptance suite (if not)
If acceptance suite do not exists
- Go to backend path or API path in terminal and execute
```sh
  codecept generate:suite acceptance
```
##Generate acceptance or API test (if not available)
- For backend acceptance test: go to backend path in terminal and execute
```sh
  codecept generate:cest acceptance YourClassToTest
```
- For API test: go to API path in terminal and execute
```sh
  codecept generate:cest api YourClassToTest
```
After this command, a file "YourClassToTestCest.php" is create in backend\test\functional or api\test\functional

Before run tests will be necessary to start **chromedriver** in listening mode, to do it execute from path of **chromedrive.exe**
```sh
  chromedriver --url-base=/wd/hub
```  

##Configuring the test in your IDE
###PhpStorm
Step 01:
- In File > Setting > PHP > Test Framework, add a new test framework
- In Codecept library, select "codecept.phar" in PHP path
- In Test Runner, select "codeception.yml" at the root of the project and after, uncheck the option "Default configuration file"

Step 02:
- In Run > Edit Configuration, add a new codeception configuration
- Put in name "Backend - Funcional test", or a name to identify the backend test
- In Test Runner, select **Type** from _Test scope_ and **acceptance** in _Type_
- Select in **Command Line > Interpretor** your php version.
- Input in **Command Line > Custom working directory**, your back end path.

Step 03:
- Verify the code exist in the archive "acceptance.suite.yml", if not, add
```sh 
extensions:
    enabled:
        - Codeception\Extension\RunProcess:
              - chromedriver --url-base=/wd/hub
```

_Observation: If you run your test project and return an error related to installing "codeception/module-webdriver", run the installation of this plugin using composer._