<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$class = 'guest-form';
if ($this->context->action->id == 'error') {
    $class .= ' error';
}
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $this->title; ?></title>
        <?php $this->head() ?>
        <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/css/inspinia-theme.css">
        <link href="<?= Url::base() ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= Url::base() ?>/css/font-awesome.css" rel="stylesheet">
        <link href="<?= Url::base() ?>/css/animate.css" rel="stylesheet">
        <link href="<?= Url::base() ?>/css/bundle.css" rel="stylesheet">
        <link href="<?= Url::base() ?>/css/desktop.css" rel="stylesheet">
        <link href="<?= Url::base() ?>/css/mobile.css" rel="stylesheet">
        <link href="<?= Url::base() ?>/css/logo.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= Url::to('/images/favicon.ico'); ?>"/>
    </head>
    <body class="background-login">
    <?php $this->beginBody() ?>
    <div class="loginColumns">
        <div class="row animated fadeInDown">
            <div class="col"></div>
            <div class="col-md-6">
                <div class="ibox-content classmade">
                    <div class="div-logo-login">
                        <img src="../images/logo.png" class="pedapp login_logo">
                    </div>
                    <?= $content; ?>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <?php $this->endBody() ?>
    <script src="<?= Url::to('/js/require.js'); ?>"></script>
    <script>(function (r) {
            r.config({packages: ['Component'], baseUrl: "<?=Url::base()?>/js/", paths: {'jquery': "jquery.min", 'popper': 'popper.min'}});
            r(['Component'], i);

            function i() {
                v = {};
                requirejs.config({
                    urlArgs: function (i, u) {
                        if (!v[u]) return '';
                        return (u.indexOf('?') === -1 ? '?' : '&') + 'v=' + v[u];
                    }
                });
            }
        })(require);</script>
    </body>
    </html>
<?php $this->endPage() ?>