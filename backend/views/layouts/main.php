<?php

use backend\assets\ThemeAssets;
use common\modules\adminGw\models\Page;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\widgets\Pjax;
use yii\helpers\Html;
use common\components\Url;
use yii\widgets\Menu;

ThemeAssets::register($this);

?>

<?= NotificationFlash::widget([
    'options' => [
        "closeButton" => TRUE,
        "debug" => FALSE,
        "progressBar" => TRUE,
        "preventDuplicates" => FALSE,
        "positionClass" => NotificationFlash::POSITION_TOP_RIGHT,
        "onclick" => NULL,
        "showDuration" => "400",
        "hideDuration" => "1000",
        "timeOut" => "7000",
        "extendedTimeOut" => "5000",
        "showEasing" => "swing",
        "hideEasing" => "linear",
        "showMethod" => "fadeIn",
        "hideMethod" => "fadeOut",
    ],
]) ?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?= $this->title; ?></title>
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>

        <link rel="icon" type="image/png" href="<?= Url::to(['/images/favicon.ico'], TRUE); ?>"/>
    </head>
    <body class="config--vertical config--borderLess menu-left--colorful menu-left--shadow background-login">
    <?php $this->beginBody(); ?>
    <div id="wrapper" data-component="base-app">
        <nav class="navbar-default navbar-static-side" role="navigation" style="position: absolute;">
            <div class="sidebar-collapse pt-2">
                <ul class="nav metismenu" id="side-menu">
                    <?php
                    $children = Page::findChildMenu();
                    $parents = Page::findParentMenu();
                    $items = [];
                    $item = [];
                    $parentIdTemp = NULL;
                    $parentArrWithChildren = [];
                    
                    foreach ($parents as $parent) {
                        $parentIdVisible = havePermission($parent->permissions);
                        $parentWasPrint = FALSE;
                        foreach ($children as $child) {
                            if (theChildBelongsToThisParent($child->parent, $parent->id)) {
                                if ($parentIdTemp != $parent->id) {
                                    $childIsVisible = havePermission($parent->permissions);
                                    $parentArrWithChildren[] = $parent->id;
                                    
                                    if (!$parentWasPrint) {
                                        $items[$parent->id] = addParentMenu($parent->title, $parent->url, $parentIdVisible, $parent->icon);
                                        $parentWasPrint = TRUE;
                                    }
                                    
                                    if (isset($child->title)) {
                                        $item[] = addChildMenu($child->title, $child->url, $childIsVisible, $child->icon);
                                    }
                                }
                            }
                        }
                        
                        if (!in_array($parent->id, $parentArrWithChildren)) {
                            $iconColor = null;
                            if (in_array($parent->identifier, ["home", "exit"])) {
                                $iconColor = "";
                            }
                            $items[$parent->id] = addParentMenu($parent->title, $parent->url, $parentIdVisible, $parent->icon, $iconColor);
                        } else {
                            $parentIdTemp = $parent->id;
                            $items[$parent->id]['items'] = $item;
                        }
                        unset($item);
                    }
                    
                    function havePermission($permission)
                    {
                        $havePermission = 0;
                        $permissions = explode(';', $permission);
                        
                        foreach ($permissions as $perm) {
                            if (Yii::$app->user->can($perm) || $perm === 'allUser') {
                                $havePermission = 1;
                            }
                        }
                        
                        return $havePermission;
                    }
                    
                    function theChildBelongsToThisParent($childParent, $parent)
                    {
                        return $childParent == $parent;
                    }
                    
                    function addParentMenu($title, $url, $visible, $icon = NULL, $iconColor = null)
                    {
                        $label = '<span class="nav-label">' . $title . '</span>';
                        if ($icon != NULL) {
                            $label = '<i class="' . $icon . '" style="color: ' . $iconColor . '"></i> <span class="nav-label">' . $title . '</span>';
                        }
                        
                        return [
                            'label' => $label,
                            'title' => $title,
                            'url' => [$url],
                            'visible' => $visible,
                        ];
                    }
                    
                    function addChildMenu($title, $url, $visible, $icon = NULL)
                    {
                        return [
                            'label' => (($icon != null) ? '<i class="' . $icon . '"></i>' : '') . $title,
                            'title' => $title,
                            'url' => [$url],
                            'visible' => $visible,
                        ];
                    }
                    
                    echo Menu::widget([
                        'options' => ['tag' => FALSE],
                        'encodeLabels' => FALSE,
                        'activateParents' => TRUE,
                        'linkTemplate' => '<a class="nav-link" href="{url}">{label}</a>',
                        'items' => $items,
                        'submenuTemplate' => "\n<ul class='nav nav-second-level collapse'>\n{items}\n</ul>\n",
                    ]);
                    ?>
                </ul>
            </div>
            <div class="menu-close-mobile d-md-none">
                <i class="fas fa-times"></i>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg" data-component="form">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top header-default pr-3 pl-3" role="navigation"
                     style="margin-bottom: 0">
                    <div class="navbar-header ">
                        <div class="row">
                            <div class="col-md-3 d-md-block d-none header-hamburguer">
                                <a class="navbar-minimalize" href="#">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </div>
                            <div class="col-3 d-md-none">
                                <img src="">
                                <div class="pedapp header-logo"></div>
                            </div>
                            <div class="col-9 header-welcome header-welcome-mobile">
                                <?php
                                $nameExplode = explode(' ', trim(Yii::$app->user->identity->name));
                                $firstName = $nameExplode[0];
                                $lastName = count($nameExplode) > 1 ? $nameExplode[count($nameExplode) - 1] : '';
                                ?>
                                Bem-vindo, <?php echo $firstName . ' ' . $lastName ?>
                            </div>
                        </div>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="d-flex h-100 align-items-center justify-content-center">
                            <div class="pedapp-horizontal header-logo d-md-block d-none"></div>
                            <div class="d-md-none h-100 header-hamburguer">
                                <a class="navbar-minimalize" href="#">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
            <div>
                <?php Pjax::begin([
                    'id' => 'main-content',
                    'timeout' => 5000,
                    'linkSelector' => '#side-menu a, #main-content a.pjax',
                    'formSelector' => FALSE,
                    'clientOptions' => ['maxCacheLength' => 0],
                ]); ?>
                <?= $content ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>


    <!-- Css e JS Yii2 -->
    <?php $this->endBody(); ?>
    <script src="https://cdn.tiny.cloud/1/z88rkmette6oqwt3zhzxj5qbc91343hehuubhzpjtkndok3k/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="<?= Url::to(['/js/require.js'], TRUE); ?>"></script>
    <script>(function (r) {
            r.config({
                packages: ['Component'],
                baseUrl: "<?=Url::base(TRUE)?>/js/",
                paths: {'jquery': "jquery-wrapper", 'popper': 'popper.min'}
            });
            r(['Component'], i);

            function i() {
                var v = {};
                requirejs.config({
                    urlArgs: function (i, u) {
                        if (!v[u]) return '';
                        return (u.indexOf('?') === -1 ? '?' : '&') + 'v=' + v[u];
                    }
                });
            }
        })(require);</script>

    </body>
    </html>
<?php $this->endPage(); ?>