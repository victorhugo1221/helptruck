window.fakecallback = function() {};

$(function() {
    $(document).on('pjax:complete', function() {
        applyMask();
    });
    applyMask();

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
});

function applyMask() {
    $('.dataMask').mask('99/99/9999');
    $('.valorMask').mask('#.##0,00', { reverse: true });
    $('.cpfMask').mask('999.999.999-99', { clearIfNotMatch: true });
    $('.telefoneMask').mask('(99) 9999-99999', { clearIfNotMatch: true });
    $('.cepMask').mask('99999-999', { clearIfNotMatch: true });
}