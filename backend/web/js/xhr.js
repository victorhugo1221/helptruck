/**
 * xhr component
 *	This component has created with the propose of ...
 */
 define('xhr', [], function(){

 	makeRequest.get = getRequest;
 	makeRequest.post = postRequest;
 	makeRequest.put = putRequest;

    // window.xhr = xhr;
 	return makeRequest;

 	function empty(){}

 	function createRequest(){
 		var timeout = null;
 		var deboucing_time = null;
 		var obj =  {
 			data: {},
 			headers: {},
 			params: {},
 			data_type:"",
 			type:"",
 			done:empty,
 			fail:empty,
 			abort:empty,
 			url: '',
 		};

 		var menu = Object.keys( obj ).reduce(function(menu, key){ menu[ key ] = setAttr.bind(null, key); return menu; },{});
 		menu.status = setStatusCallback;
 		menu.header = setObjAttr.bind(null, 'headers');
        menu.asList = setObjAttrTrue.bind(null, 'lists');
 		menu.asJson = setObjAttrTrue.bind(null, 'json');
 		menu.debounce = debounce;
 		menu.loop = loop;
 		menu.stop = stop;
 		menu.pause = pause;
 		menu.queue = queue;

 		obj.status = {};
 		obj.lists = {};
 		obj.xhr = null;

 		queue();
 		return menu;

 		function setAttr(attr, value){
 			obj[attr] = value;
 			if( ['data', 'params'].indexOf(attr) != -1 ) bounceQueue();
 			return menu;
 		}

        function setObjAttrTrue(attr, name){
            if( !obj[attr] ) obj[attr] = {};
            obj[attr][name] = true;
            return menu;
        }

 		function setObjAttr(attr, name, value){
 			if( !obj[attr] ) obj[attr] = {};
 			obj[attr][name] = value;
 			if( attr == "headers" ) bounceQueue();
 			return menu;
 		}

 		function addArrayItem(array, value){
 			if( !obj[array] ) obj[array] = [];
 			obj[array].push( value );
 			return menu;
 		}

 		function setStatusCallback( status, callback ){
 			if( !obj.status[ status ] ) obj.status[ status ] = [];
 			obj.status[ status ].push( callback );
 			return menu;
 		}

 		function loop(time){
			deboucing_time = time;
 			setStatusCallback(200, queue);
 			queue();
 			return menu;
 		}

 		function stop(){
 			ct();
 			if( obj.xhr ) obj.xhr.abort();
 			deboucing_time = null;
 			return menu;
 		}

 		function pause(){
 			ct();
            if( obj.xhr ) obj.xhr.abort();
 			return menu;
 		}


 		function ct(){ if( timeout ) clearTimeout( timeout ); }

 		function debounce(time){
 			deboucing_time = time;
 			ct();
 			return menu;
 		}

 		function bounceQueue(){
 			if( deboucing_time !== null ){
 				if( obj.xhr ) obj.xhr.abort();
 				queue();
			}
 		}

 		function queue(){
 			ct();
 			timeout = setTimeout(processRequest, deboucing_time || 0);
 		}

 		function paramsToQueryString(param){
 			return param.key + "=" + encodeURI(param.value);
 		}
 		function processRequest(){

 			var request = new XMLHttpRequest();
 			obj.xhr = request;

 			var _url = obj.url;
 			if( Object.keys(obj.params).length ) {
 				_url += "?";
 				_url += toParams( obj.params ).map( paramsToQueryString ).join('&');
 			}
			request.open(obj.type, _url, true);
			Object.keys(obj.headers).forEach(function(header){
				request.setRequestHeader( header, obj.headers[header] );
			})

			request.onload = function() {
				(obj.status[ request.status ] || []).forEach(function(fn){
                    if( obj.json ) return fn( JSON.parse(request.responseText) );
                    fn( request )
                });
			};

			request.onerror = function() {
			  console.error('Request error')
			};

      if( obj.data instanceof FormData ) return request.send( obj.data ) ;
			if( Object.keys(obj.data).length ){
        request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        return request.send(JSON.stringify(obj.data))
      }

			request.send();

 		}

 	}

		function toParams( obj, options, position ){
        if(!options) options = { tree:[], asList:[] };
        if(!options.tree) options.tree = [];
        options.tree.push( obj );

        position = position || '';
        var prefix = position.split('.').reduce( parsePosition, "" );
        var data = Object.keys( obj ).reduce( extractData, [] );

        return data;

        function extractData(result, $key, index ){

            var value = obj[$key];
            var key = ( prefix )? "[" + $key + "]" : $key;
            var alias = ( prefix )? position + "." + $key : $key;
            var is_array = Array.isArray( value );
            var is_object = isObject( value );

            if( is_array && options.asList.indexOf( alias ) != -1 ) value = value.join(',');

            if( is_object && options.tree.indexOf( value ) != -1 ){
                /* TODO: deal with possible recursion */
            }

            if( is_object && options.asList.indexOf( alias ) == -1){
                return result.concat( toParams( value, options, alias ) )
            }

            result.push({ key:prefix + key, value:value })
            return result;
        }

        function parsePosition(c,e,i,a){ return ( i>0 )? c + "[" + e + "]" : e ; }
    }

	function isObject(obj) { return obj === Object(obj); }
	function getRequest  (url){ return createRequest().type('GET').url(url) }
	function postRequest (url){ return createRequest().type('POST').url(url) }
	function putRequest  (url){ return createRequest().type('PUT').url(url) }
	function makeRequest (url){ return createRequest().url(url) }
});

