/**
 * Descrição do componente base-app
 *
 */
define('dashboard', [
	'Component',
	'jquery',
	'popper',
	'bootstrap',
	'plugins/metisMenu/jquery.metisMenu' ,
	'plugins/slimscroll/jquery.slimscroll.min',

	'inspinia'
	], function(){


	// Declarações
	var self = new Component('base-app');
	self.install = install;
	self.elements = {
		// 'element': { event: function }
	};

	// Processamento
	self.bootstrap();
	return self;

	// Funções
	function install()
	{

	}

});
