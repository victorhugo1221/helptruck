/**
 * Descrição do componente lang-form
 *
 */
define('lang-form', ['Component', 'css!./style'], function(){

	// Declarações
	var self = new Component('lang-form');
	self.install = install;
	self.elements = {
		'option': { click: selectLanguage }
	};

	// Processamento
	self.bootstrap();
	return self;

	// Funções
	function install()
	{

	}

	function selectLanguage(component, event)
	{
		component.element('option').state('active', false);
		this.BEM.state('active', true);
		var lang = this.getAttribute('data-lang');
		component.element('lang').state('active', false);
		component.element('lang--'+lang).state('active', true);
	}

});