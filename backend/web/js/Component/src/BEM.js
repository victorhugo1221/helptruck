define([], function(){
    HTMLElement.prototype.is = getElementState;
    HTMLElement.prototype.state = setElementState;
    HTMLElement.prototype.element = elementFinder;
    Object.defineProperty(HTMLElement.prototype, 'BEM', {
        get:function(){
            return elementFinder.bind(this, '_')();
        }
    })

    function elementFinder(name)
    {
        var timeout = setTimeout( doElementChanges ),
            is_htmlElement = name instanceof HTMLElement,
            is_array = name instanceof Array,
            self = this,
            options = { element_selector : name },
            menu = {
                state: setOption.bind(this, 'state'),
                modifier: setOption.bind(this, 'modifier'),
                variation: setOption.bind(this, 'variation'),
                helper: setOption.bind(this, 'helper'),
                element: elementFinder.bind(this),
                append: appendToElement.bind(this),
                first: getFirst.bind(this),
                filter: setOption.bind(this, 'filters'),
                last: getLast.bind(this),
                all: selectElements.bind(this),
                do: doFn
            };
        var define = Object.defineProperty.bind(this, menu);

        define( 'value', { get:getAttrOrEmpty.bind(self, 'value'), set:setAttr.bind(self, 'value') });
        define( 'html', { get:getAttrOrEmpty.bind(self, 'innerHTML'), set:setAttr.bind(self, 'innerHTML') });
        define( 'timeout', { get:function(){ return timeout; }, set:function(nv){ timeout = nv; return nv}});

        if( is_htmlElement || is_array ) return menu;

        options.element_selector = "." + this.component_name;
        if( name != '_' ) options.element_selector += "_" + name;

        return menu;

        function doFn(fn){
            fn( menu );
            return menu;
        }

        function appendToElement(html)
        {
            selectElements().forEach(function(el){ el.innerHTML += html });
        }

        function getFirst()
        {
            return selectElements()[0];
        }

        function getLast()
        {
            return selectElements().reduce(function(c,e){ return e || c;}, null);
        }

        function selectElements()
        {
            if( options.element_selector instanceof Array ) return options.element_selector;
            if( options.element_selector instanceof HTMLElement ) return [ options.element_selector ];
            if( options.element_selector.indexOf('_') == -1 ) return [ self ];

            var elements = [].slice.call(self.querySelectorAll(options.element_selector))
            if( options.filters ) elements = options.filters.reduce(applyFilter, elements);

            return elements;

        }

        function applyFilter(elements, filter){
            return elements.filter( filter.name );
        }


        function getAttrOrEmpty(attr)
        {
            var selection = selectElements();
            if( !selection ) return "";
            return selection[0][attr];
        }

        function setAttr(attr, value)
        {
            selectElements().forEach(function(element){ element[attr] = value; });
        }

        function setOption( opt, name, state )
        {
            if( !options[opt] )
            {
                options[opt] = [ { name:name , state:state } ];
                return menu;
            }

            options[opt].push({name:name, state:state});
            return menu;
        }

        function doElementChanges()
        {
            var elements = selectElements();
            elements.forEach(function(el){
                setStates( el , options.state     , function(e){ return 'is-' + e.name;} );
                setStates( el , options.helper    , function(e){ return 'h-' + e.name;} );
                setStates( el , options.modifier  , function(e){ return '-' + e.name;} );
                setStates( el , options.variation , function(e){ return self.component_name + "_" + name + "--" + e.name;} );
            });

            if( !elements.length )
        }
    }

    function getElementState(state)
    {
        return this.classList.contains('is-' + state);
    }

    function setElementState(state, status){
        var is = this.is(state);
        if( is && status ) return;
        if( is ^ status ) this.BEM.state(state, status);
    }

    function setStates( element, states, classFn )
    {
        if( !states || !states.length ) return;
        return states.forEach( addClass );

        function addClass(el)
        {
            if( el.state ) return element.classList.add( classFn(el) );
            element.classList.remove( classFn(el) );
        }
    }
});
