define([], function(){ 
		var handledEvents = [
			'blur', 'change', 
			'focus', 'focusin', 'focusout', 
			'select', 'submit', 'click', 
			'contextmenu', 'dblclick', 'hover', 
			'mousedown', 'mouseenter', 'mouseleave', 
			'mousemove', 'mouseout', 'mouseover', 
			'mouseup', 'keydown', 'keypress', 
			'keyup', 'resize', 'scroll', 'wheel',
            'dragenter', 'dragover', 'dragleave', 'drop',
            'ended'
		];
        handledEvents.componentEvent = componentEvent;

		handledEvents.forEach(function(event) { document.addEventListener(event, componentEvent, true); });

    function insertElement(c, element){
         var component = ( element.attributes['data-component'] )? element.attributes['data-component'].value : '' ;
        
        if( component ){
            element.component = component;
            c.last_component[ component ] = {
                element : element,
                name : component,
                elements : []
            };
            c.components.push( c.last_component[component] );
        }

        if( typeof element.className != 'string' ) return c;
        var classList = element.className.split(' ');
        var l = classList.length;
        for(var i=0; i<l; i++){
            var _class = classList[i];
            var _i = _class.indexOf('_');
            if( _i  == -1 ) continue;
            if( _class.indexOf('--') != -1 ) continue;
            if( _i != _class.lastIndexOf('_') ) continue;

            var parts = _class.split('_');
            if(!c.last_component[ parts[0] ]) continue;
            c.last_component[ parts[0] ].elements.push( {
                name : _class,
                element : element
            });
        }

        return c;
    }

    function componentEvent(event) {
        if( !event.target || !event.target.tagName || ['HTML', 'BODY'].indexOf( event.target.tagName ) != -1  ) return;

        var element = event.target;
        var element_list = [];
        var _event = event;

        while( element != document.body ){
            element_list.push( element );
            element = element.parentElement;
        }

        var data = element_list.reverse().reduce( insertElement, { 
            waiting_components : {},
            components : [],
            last_component : {}
        });

        var called = [];
        
        data.components.reverse().forEach(function(component){
            if( !require.defined(component.name) ) return;
            var module = require( component.name );
            if( !module.events[event.type] ) return;
                component.elements
                            .reverse()
                            .forEach(function(element){
                                if( !module.events[ event.type ][ element.name ] ) return;
                                if( called.indexOf( module.events[ event.type ][ element.name ] ) != -1 ) return;
                                module.events[ event.type ][ element.name ].call( element.element, component.element, _event );
                                called.push( module.events[ event.type ][ element.name ] );
                            });
        });

       return false;
    }

		return handledEvents;
	});