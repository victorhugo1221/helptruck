define( ['./src/install', './src/handled_events', './src/BEM'], function(install, handledEvents, BEM) {
    function Component(name) {
        this.name = name;
        return this
    }

    Component._events = {};
    Component.on = on.bind(Component)
    Component.off = off.bind(Component);
    Component.emit = emit.bind(Component);
    Component.prototype = {
        on: on,
        off: off,
        emit: emit,
        _events: {},
        events: {},
        elements: {},
        register: register,
        bootstrap: bootstrap,
        install: function() {}
    };
    
    var el = document.createElement('DIV');
    document.body.appendChild(el);
    document.body.removeChild(el);

    $(document).on("pjax:end", "#main-content", ajustaItensMenu);

    $('#side-menu li.active').removeClass('active');
    $('#side-menu a[href="'+  window.location.pathname +'"]').parents('li').addClass('active');
        
    window.Component = Component;
    return Component;

    function on(event, cb) {
        event = this._events[event] = this._events[event] || []
        event.push(cb);
    }

    function off(event, cb) {
        if (!this._events[event]) return;
        event = this._events[event];
        event.splice(event.indexOf(cb) >>> 0, 1);
    }

    function emit(event) {
        var list = this._events[event]
        if (!list || !list[0]) return

        var args = list.slice.call(arguments, 1)
        list.slice().map(function(i) {
            i.apply(this, args)
        })
    }

    function bootstrap() {
        var self = this;
        Object.keys(self.elements).forEach(function(target) {
            Object.keys(self.elements[target]).forEach(function(event) {
                self.register(self.name + '_' + target, event, self.elements[target][event]);

                if( handledEvents.indexOf(event) != -1 ) return;

                if( event[0] != '$' ){
                    handledEvents.push( event );
                    document.addEventListener(event, handledEvents.componentEvent, true);
                    return;
                }

                if( !jQuery ) return console.log('O evento "'+ event +'" necessita de jQuery para acontecer');

                if( event.slice(0,2) == "$$" ){
                    jQuery(document).on( event.slice(2), '.'+self.name + '_' + target, function( _event ){
                        self.elements[target][event].call( this, $(this).parents('[data-component="'+ self.name +'"]').get(0) , _event );
                    } );
                    return;
                }

                jQuery(document).on( event.slice(1), '.'+self.name + '_' + target, self.elements[target][event] );
            })
        });
    }

    function ajustaItensMenu( ev, state, xhr )
    {
        $('#side-menu li.active').removeClass('active');
        $('#side-menu a[href="'+  window.location.pathname +'"]').parents('li').addClass('active');
    }

    function register(target, event, fn) {
        if (!this.events[event]) this.events[event] = {};
        if (!this.events[event][target]) this.events[event][target] = {};
        this.events[event][target] = fn;
    }
});
