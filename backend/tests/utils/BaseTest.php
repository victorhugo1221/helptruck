<?php

namespace backend\tests\utils;

use backend\tests\AcceptanceTester;
use common\components\Model;
use common\components\utils\Text;

abstract class BaseTest {
	protected static ?Model $insertedModel = null;

	protected array $operation
		= [
			"equal",
			"startWith",
			"contain",
			"endWith",
		];

	protected string $urlDefault;
	protected string $pageName;

	protected static function _insertModel(AcceptanceTester $I, string $class, array $data, ?bool $force = true): Model {
		if ($force === true || !static::$insertedModel || empty(static::$insertedModel)) {
			$modelId               = $I->haveRecord($class, $data);
			static::$insertedModel = $I->grabRecord($class, ['id' => $modelId]);
		}
		return static::$insertedModel;
	}

	protected function _accessPage(AcceptanceTester $I): void {
		$I->wantTo("Access {$this->pageName} page");
		$I->doLogin();
		$I->amOnPage('/');
		$I->see($this->pageName);
		$I->click($this->pageName);
		$I->amOnPage($this->urlDefault);
	}

	protected function _filterByField(AcceptanceTester $I, string $field, string $operation, string $term): void {
		$I->selectOption("select[name=field]", ['value' => $field]);
		$I->selectOption("select[name=operation]", ['value' => $operation]);
		$I->fillField(['name' => 'term'], $term);
		$I->click('Filtrar');
		$I->wait(1);
	}

	protected function _clearFilter(AcceptanceTester $I, string $urlDefault): void {
		$I->see('Limpar');
		$I->click('Limpar');
		$I->amOnPage($urlDefault . '/clear-filter');
	}

	protected function _clickOnNewButton(AcceptanceTester $I, string $article = 'o'): void {
		$action = 'Nov' . $article;
		$I->see('Novo');
		$I->click('Novo');
		$I->see($action . ' ' . $this->pageName);
	}

	public function tryClearFilter(AcceptanceTester $I) {
		$this->_accessPage($I);
		$this->_clearFilter($I, $this->urlDefault);
	}

	public function tryAccessPage(AcceptanceTester $I) {
		$this->_accessPage($I);
		$I->see('Listagem de ' . Text::pluralize($this->pageName));
	}
}