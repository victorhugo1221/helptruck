<?php


namespace backend\tests\utils;


trait DataFormat
{
    private function maskCpf(string $cpf): string{
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cpf);
    }

    private function clearMaskCpf(string $cpf): string{
        return preg_replace('/[^[:digit:]]/', '', $cpf);
    }
}