<?php

namespace backend\tests;

use Codeception\Actor;
use Codeception\Step\Argument\PasswordArgument;

class AcceptanceTester extends Actor {
	use _generated\AcceptanceTesterActions;

	public function doLogin(string $email = 'atendimento@grupow.com.br', string $password = 'x') {
		$I = $this;
		// if snapshot exists - skipping login
		if ($I->loadSessionSnapshot('login')) {
			return;
		} else {

			// logging in
			$I->amOnPage('/login');
			$I->submitForm('#form-validation', ['LoginForm[email]' => $email, 'LoginForm[password]' => $password]);

			// saving snapshot
			$I->saveSessionSnapshot('login');
		}
	}
}
