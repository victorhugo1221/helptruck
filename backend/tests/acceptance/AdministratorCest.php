<?php

namespace backend\tests\acceptance;

use backend\tests\AcceptanceTester;
use backend\tests\utils\BaseTest;
use backend\tests\utils\DataFormat;
use backend\tests\utils\GenerateData;
use Codeception\Example;
use common\components\utils\DefaultMessage;
use common\modules\user\models\User;

class AdministratorCest extends BaseTest {
	use GenerateData;
	use DataFormat;

	public function _before(AcceptanceTester $I) {
		$this->pageName   = User::getPageName();
		$this->urlDefault = User::getPageUrl();
	}

	private function insertAnUserForTest(AcceptanceTester $I, int $statusClient, bool $forceInsert = false): Model {
		$time = time();
		return static::_insertModel($I, User::class, [
			'name'             => "User for test {$time}",
			'cpf'              => $this->generateAValidCPF(false),
			'email'            => "{$time}@grupow.com.br",
			'is_administrator' => User::IS_ADMINISTRATOR,
			'status'           => $statusClient,
		], $forceInsert);
	}


	public function tryToSeeTheDetailsOfARecord(AcceptanceTester $I) {
		$user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
		$this->_accessPage($I);
		$this->_filterByField($I, 'name', 'equal', $user->name);
		$I->see($user->name);
		$I->click($user->name);
		$I->amOnPage($this->urlDefault . '/update/' . $user->id);
	}

	public function tryToSeeTheDetailsOfANonExistentRecord(AcceptanceTester $I) {
		$id = 99999999999;
		$this->_accessPage($I);
		$I->amOnPage($this->urlDefault . '/update/' . $id);
		$I->amOnPage($this->urlDefault);
		$I->dontSeeRecord(User::class, ['id' => $id]);
		$I->see(DefaultMessage::registerNotFound($this->pageName, 'o'));
	}

	public function tryFilterByStatus(AcceptanceTester $I) {
		$statusList = User::$_status;
		foreach ($statusList as $statusId => $statusName) {
			$this->insertAnUserForTest($I, (int)$statusId, true);
			$this->_accessPage($I);
			$I->selectOption("select[name=status]", ['value' => $statusId]);
			$I->see('Filtrar');
			$I->click('Filtrar');
			$I->dontSee(DefaultMessage::theSearchDidNotReturnData());
			$I->see($statusName);
		}
	}

	public function tryFilterByField(AcceptanceTester $I) {
		$fields = ["name", "email"];
		$user   = $this->insertAnUserForTest($I, User::STATUS_PENDENT, true);
		foreach ($fields as $field) {
			foreach ($this->operation as $operation) {
				$this->_accessPage($I);
				$this->_filterByField($I, $field, $operation, $user->$field);
				$I->see($user->name);
			}
		}
	}

	public function tryToSaveWithMandatoryFieldsNull(AcceptanceTester $I) {
		$this->_accessPage($I);
		$this->_clickOnNewButton($I);
		$I->fillField(['name' => 'User[name]'], null);
		$I->fillField(['name' => 'User[email]'], null);
		$I->fillField(['name' => 'User[cpf]'], null);
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see('“Nome” não pode ficar em branco.');
		$I->see('“Email” não pode ficar em branco.');
		$I->see('“CPF” não pode ficar em branco.');
	}

	public function tryToValidateUniqueEmail(AcceptanceTester $I) {
		$user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
		$this->_accessPage($I);
		$this->_clickOnNewButton($I);
		$I->fillField(['name' => 'User[email]'], $user->email);
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see('Email “' . $user->email . '” já foi atribuido.');
	}

	public function tryToValidateUniqueCpf(AcceptanceTester $I) {
		$user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
		$this->_accessPage($I);
		$this->_clickOnNewButton($I);
		$I->fillField(['name' => 'User[cpf]'], $this->maskCpf($user->cpf));
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see('O CPF já está cadastrado como administrador ou cliente.');
	}

	/**
	 * @example ["000.000.000-00"]
	 * @example ["123.456.789-10"]
	 */
	public function tryToValidateIfTheCpfIsValid(AcceptanceTester $I, Example $data) {
		$this->_accessPage($I);
		$this->_clickOnNewButton($I);
		$I->fillField(['name' => 'User[cpf]'], $data[0]);
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see('CPF inválido');
	}

	public function trySaveANewRecord(AcceptanceTester $I) {
		$time = time();
		$this->_accessPage($I);
		$this->_clickOnNewButton($I);
		$I->fillField(['name' => 'User[name]'], "User for test {$time}");
		$I->fillField(['name' => 'User[email]'], "{$time}@grupow.com.br");
		$I->fillField(['name' => 'User[cpf]'], $this->generateAValidCPF(false));
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see(DefaultMessage::sucessOnInsert($this->pageName, 'o'));
		$I->seeRecord(User::class, ['email' => "{$time}@grupow.com.br"]);
	}

	public function trySaveANewRecordAndBack(AcceptanceTester $I) {
		$time = time();
		$this->_accessPage($I);
		$this->_clickOnNewButton($I);
		$I->fillField(['name' => 'User[name]'], "User for test {$time}");
		$I->fillField(['name' => 'User[email]'], "{$time}@grupow.com.br");
		$I->fillField(['name' => 'User[cpf]'], $this->generateAValidCPF(false));
		//TODO: Implement the permission logic in this test
		$I->see('Adicionar e ir para listagem');
		$I->click('button_submit-and-back');
		$I->wait(0.5);
		$I->see(DefaultMessage::sucessOnInsert($this->pageName, 'o'));
		$I->seeRecord(User::class, ['email' => "{$time}@grupow.com.br"]);
	}

	public function trySavingARegistryUpdate(AcceptanceTester $I) {
		$this->_accessPage($I);
		//TODO: Implement the permission logic in this test
		$user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
		$this->_filterByField($I, 'name', 'equal', $user->name);
		$I->see($user->name);
		$I->click($user->name);
		$I->amOnPage($this->urlDefault . '/update/' . $user->id);
		$name  = $I->grabValueFrom(['name' => 'User[name]']);
		$email = $I->grabValueFrom(['name' => 'User[email]']);
		$I->fillField(['name' => 'User[name]'], "edited_{$name}");
		$I->fillField(['name' => 'User[email]'], "edited_{$email}");
		$I->fillField(['name' => 'User[cpf]'], $this->generateAValidCPF(false));
		$I->see('Salvar e continuar editando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see(DefaultMessage::sucessOnUpdate($this->pageName, 'o'));
		$I->seeRecord(User::class, ['email' => "edited_{$email}", 'name' => "edited_{$name}", 'id' => $user->id]);
	}

	public function trySavingARegistryUpdateAndBack(AcceptanceTester $I) {
		$this->_accessPage($I);
		//TODO: Implement the permission logic in this test
		$user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
		$this->_filterByField($I, 'name', 'equal', $user->name);
		$I->see($user->name);
		$I->click($user->name);
		$I->amOnPage($this->urlDefault . '/update/' . $user->id);
		$name  = $I->grabValueFrom(['name' => 'User[name]']);
		$email = $I->grabValueFrom(['name' => 'User[email]']);
		$I->fillField(['name' => 'User[name]'], "edited_back_{$name}");
		$I->fillField(['name' => 'User[email]'], "edited_back_{$email}");
		$I->fillField(['name' => 'User[cpf]'], $this->generateAValidCPF(false));
		$I->see('Salvar e ir para listagem');
		$I->click('button_submit-and-back');
		$I->wait(0.5);
		$I->see(DefaultMessage::sucessOnUpdate($this->pageName, 'o'));
		$I->seeRecord(User::class, ['email' => "edited_{$email}", 'name' => "edited_back_{$name}", 'id' => $user->id]);
	}
}
