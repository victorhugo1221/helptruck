<?php

namespace backend\tests\acceptance;

use backend\tests\AcceptanceTester;
use backend\tests\utils\BaseTest;
use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\adminGw\models\Translation;
use common\modules\user\models\User;

class TranslationCest extends BaseTest {
	public function _before(AcceptanceTester $I) {
		$this->pageName   = Translation::getPageName();
		$this->urlDefault = Translation::getPageUrl();
	}

	private function insertATranslationForTest(AcceptanceTester $I) {
		$time = time();
		return static::_insertModel($I, Translation::class, [
			'key' => 'the-key-test-' . $time,
			'pt'  => 'Um texto para testes em <b>português</b> - ' . $time,
			'en'  => 'An <b>english</b> test text - ' . $time,
			'es'  => 'Un texto para pruebas en <b>español</b> - ' . $time,
		]);
	}

	public function tryFilterByField(AcceptanceTester $I) {
		$fields      = ["key", "pt", "en", "es"];
		$translation = $this->insertATranslationForTest($I);
		foreach ($fields as $field) {
			foreach ($this->operation as $operation) {
				$this->_accessPage($I);
				$this->_filterByField($I, $field, $operation, $translation->$field);
				$I->wait(1);
				$I->see($translation->key);
			}
		}
	}

	public function trySaveANewRecord(AcceptanceTester $I) {
		$time = time();
		$this->_accessPage($I);
		$this->_clickOnNewButton($I, 'a');
		$I->fillField(['name' => 'Translation[key]'], "TranslationTestJS-{$time}");
		$I->executeJS("tinymce.setActive(tinymce.get('translation-pt'));tinyMCE.activeEditor.setContent('Texto via JS <b>português</b> - {$time}');");
		$I->executeJS("tinymce.setActive(tinymce.get('translation-en'));tinyMCE.activeEditor.setContent('Texto via JS <b>inglês</b> - {$time}');");
		$I->executeJS("tinymce.setActive(tinymce.get('translation-es'));tinyMCE.activeEditor.setContent('Texto via JS <b>espanhol</b> - {$time}');");
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(1);
		$I->see(DefaultMessage::sucessOnInsert($this->pageName, 'a'));
		$I->seeRecord(Translation::class, ['key' => "TranslationTestJS-{$time}"]);
	}

	public function trySaveANewRecordAndBack(AcceptanceTester $I) {
		$time = time();
		$this->_accessPage($I);
		$this->_clickOnNewButton($I, 'a');
		$I->fillField(['name' => 'Translation[key]'], "TranslationTestJS-{$time}");
		$I->executeJS("tinymce.setActive(tinymce.get('translation-pt'));tinyMCE.activeEditor.setContent('Texto via JS <b>português</b> - {$time}');");
		$I->executeJS("tinymce.setActive(tinymce.get('translation-en'));tinyMCE.activeEditor.setContent('Texto via JS <b>inglês</b> - {$time}');");
		$I->executeJS("tinymce.setActive(tinymce.get('translation-es'));tinyMCE.activeEditor.setContent('Texto via JS <b>espanhol</b> - {$time}');");
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit-and-back');
		$I->wait(1);
		$I->see(DefaultMessage::sucessOnInsert($this->pageName, 'a'));
		$I->seeRecord(Translation::class, ['key' => "TranslationTestJS-{$time}"]);
		$this->tryAccessPage($I);
	}

	public function trySavingARegistryUpdate(AcceptanceTester $I) {
		$this->_accessPage($I);
		$translation = $this->insertATranslationForTest($I);
		$this->_filterByField($I, 'key', 'equal', $translation->key);
		$I->wait(1);
		$I->see($translation->key);
		$I->click($translation->key);
		$I->amOnPage($this->urlDefault . "/update/{$translation->id}");
		$I->fillField(['name' => 'Translation[key]'], "test_edit_{$translation->key}");
		$I->see('Salvar e continuar editando');
		$I->click('button_submit');
		$I->wait(1);
		$I->see(DefaultMessage::sucessOnUpdate($this->pageName, 'a'));
		$I->seeRecord(Translation::class, ['key' => "test_edit_{$translation->key}"]);
	}

	public function trySavingARegistryUpdateAndBack(AcceptanceTester $I) {
		$this->_accessPage($I);
		$translation = $this->insertATranslationForTest($I);
		$this->_filterByField($I, 'key', 'equal', $translation->key);
		$I->wait(1);
		$I->see($translation->key);
		$I->click($translation->key);
		$I->amOnPage($this->urlDefault . "/update/{$translation->id}");
		$I->fillField(['name' => 'Translation[key]'], "test_edit_{$translation->key}");
		$I->see('Salvar e continuar editando');
		$I->click('button_submit-and-back');
		$I->wait(1);
		$I->see(DefaultMessage::sucessOnUpdate($this->pageName, 'a'));
		$I->seeRecord(Translation::class, ['key' => "test_edit_{$translation->key}"]);
		$this->tryAccessPage($I);
	}

	public function tryDelete(AcceptanceTester $I) {
		$this->_accessPage($I);
		$translation = $this->insertATranslationForTest($I);
		$I->amOnPage($this->urlDefault . '/update/' . $translation->id);
		$I->see('Deletar');
		$I->click('Deletar');
		$I->see('Deseja realmente remover?');
		$I->wait(0.5);
		$I->executeJS('return $("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button").get(0).click()');
		$I->wait(1);
		$I->dontSeeRecord(Translation::class, ['key' => $translation->key]);
	}
}
