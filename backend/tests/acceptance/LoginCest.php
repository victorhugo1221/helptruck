<?php

namespace backend\tests;

use Codeception\Step\Argument\PasswordArgument;

class LoginCest {
	public function _before(AcceptanceTester $I) {
	}

	public function tryAccessPanel(AcceptanceTester $I) {
		$I->wantTo('Validate authentication');
		$I->amOnPage('/login');
		$I->canSeeElement('input', ['id' => 'loginform-email']);
		$I->canSeeElement('#loginform-password');
		$I->canSeeElement('button', ['name' => 'entrar']);
		$I->canSee('Esqueci minha senha', 'a');

		$I->submitForm('#form-validation',['LoginForm[email]'=>'atendimento@grupow.com.br','LoginForm[password]'=>'x']);

		$I->amOnPage('/');
		$I->see('Sair');
		$I->dontSee('Esqueci minha senha');
		$I->dontSee('Entrar');
	}
}
