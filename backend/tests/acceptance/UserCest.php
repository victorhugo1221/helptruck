<?php

namespace backend\tests\acceptance;

use backend\tests\AcceptanceTester;
use backend\tests\utils\BaseTest;
use backend\tests\utils\GenerateData;
use Codeception\Example;
use common\components\utils\Text;
use common\modules\profile\models\Profile;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;

class UserCest extends BaseTest
{
    use GenerateData;

    public function _before(AcceptanceTester $I) {
        $this->pageName   = UserClient::getPageName();
        $this->urlDefault = UserClient::getPageUrl();
    }

    private function insertAnUserForTest(AcceptanceTester $I, int $statusClient, int $profileId = null): User
    {
        if($profileId == null) {
            $profileId = (Profile::find()->one())->id;
        }
        
        $userId = $I->haveRecord(User::class, [
            'name'             => 'User for test ' . time(),
            'cpf'              => $this->generateAValidCPF(),
            'email'            => time() . "@grupow.com.br",
            'is_administrator' => User::IS_NOT_ADMINISTRATOR,
            'status'           => $statusClient,
        ]);

        if ($userId > 0) {
            $I->haveRecord(UserClient::class, [
                'user_id'    => $userId,
                'profile_id' => $profileId,
                'status'     => $statusClient,
            ]);
        }
        $I->seeInDatabase(User::tableName(), ['id' => $userId]);
        $user = $I->grabRecord(User::class, ['id' => $userId]);

        return $user;
    }

    private function changePassword(AcceptanceTester $I){
        $I->see('Alterar Senha');
        $I->click('Alterar Senha');
        $I->see('Alteração de Senha');
        $I->fillField(['name' => 'NewPasswordForm[password]'], '123456');
        $I->fillField(['name' => 'NewPasswordForm[confirmation]'], '654321');
        $I->see('Salvar');
        $I->click('#formModel > div > div > div.modal-footer > button.btn.btn-save');
        $I->wait(1);
    }

    public function tryAccessPage(AcceptanceTester $I)
    {
        $this->_accessPage($I);
        $pageName = UserClient::getPageName();
        $I->see('Listagem de ' .  Text::pluralize($pageName));
    }

    public function tryToSeeTheDetailsOfARecord(AcceptanceTester $I)
    {
        $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
        $this->_accessPage($I);
        $this->_filterByField($I, 'name', 'equal', $user->name);
        $I->see($user->email);
        $I->click($user->email);
        $I->amOnPage($this->urlDefault . '/update/' . $user->id);
    }

    public function tryToSeeTheDetailsOfANonExistentRecord(AcceptanceTester $I)
    {
        $id = 99999999999;
        $this->_accessPage($I);
        $I->amOnPage($this->urlDefault . '/update/' . $id);
        $I->amOnPage($this->urlDefault);
        $I->dontSeeInDatabase(User::tableName(), ['id' => $id]);
    }

    public function tryFilterByStatus(AcceptanceTester $I)
    {
        $statusList = User::$_status;
        foreach ($statusList as $statusId => $statusName) {
            $this->insertAnUserForTest($I, (int) $statusId);
            $this->_accessPage($I);
            $I->selectOption("select[name=status]", ['value' => $statusId]);
            $I->see('Filtrar');
            $I->click('Filtrar');
            $I->dontSee('Não há resultado para o filtro realizado.');
            $I->see($statusName);
        }
    }

    public function tryFilterByProfile(AcceptanceTester $I)
    {
        $profiles = Profile::find()->all();
        foreach ($profiles as $profile) {
            $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT, $profile->id);
            $this->_accessPage($I);
            $I->selectOption("select[name=profile_id]", ['value' => $profile->id]);
            $I->see('Filtrar');
            $I->click('Filtrar');
            $I->dontSee('Não há resultado para o filtro realizado.');
            $I->see($profile->name);
        }
    }

    public function tryToSeeNoResults(AcceptanceTester $I)
    {
        $this->_accessPage($I);
        $this->_filterByField($I, 'name', 'equal', 'Nothing in time ' . time());
        $I->see('Não há resultado para o filtro realizado.');
    }

    public function tryFilterByField(AcceptanceTester $I)
    {
        $fields = ["name", "email", "cpf"];
        foreach ($fields as $field) {
            foreach ($this->operation as $operation) {
                $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
                $this->_accessPage($I);
                $this->_filterByField($I, $field, $operation, $user->$field);
                $I->see($user->email);
            }
        }
    }

    public function trySavingARegistryUpdate(AcceptanceTester $I)
    {
        $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
        $this->_accessPage($I);
        $this->_filterByField($I, 'name', 'equal', $user->name);
        $I->see($user->name);
        $I->click($user->name);
        $I->amOnPage($this->urlDefault . '/update/' . $user->id);
        $I->fillField(['name' => 'User[email]'], 'test_edit_' . $user->email);
        $I->see('Salvar e continuar editando');
        $I->click('button_submit');
        $I->wait(0.5);
        $I->see('Cliente alterado com successo!');
        $I->seeInDatabase(User::tableName(), ['email' => 'test_edit_' . $user->email]);
    }

    public function tryChangePassword(AcceptanceTester $I)
    {
        $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
        $this->_accessPage($I);
        $this->_filterByField($I, 'name', 'equal', $user->name);
        $I->see($user->name);
        $I->click($user->name);
        $I->amOnPage($this->urlDefault . '/update/' . $user->id);
        $this->changePassword($I);
    }

    public function tryChangePasswordConfirmationNotEqual(AcceptanceTester $I)
    {
        $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
        $this->_accessPage($I);
        $this->_filterByField($I, 'name', 'equal', $user->name);
        $I->see($user->name);
        $I->click($user->name);
        $I->amOnPage($this->urlDefault . '/update/' . $user->id);
        $this->changePassword($I);
        $I->see('A confirmação de senha deve ser igual à senha');
    }

    public function tryDelete(AcceptanceTester $I)
    {
        $this->_accessPage($I);
        $user = $this->insertAnUserForTest($I, User::STATUS_PENDENT);
        $I->amOnPage($this->urlDefault . '/update/' . $user->id);
        $I->see('Deletar');
        $I->click('Deletar');
        $I->see('Deseja realmente remover?');
        $I->wait(0.5);
        $I->executeJS('return $("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button").get(0).click()');
        $I->wait(0.5);
        $I->seeInDatabase(User::tableName(), ['id' => $user->id, 'status' => User::STATUS_ADMIN_INACTIVE]);
    }
}
