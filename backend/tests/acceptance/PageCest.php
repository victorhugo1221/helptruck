<?php

namespace backend\tests\acceptance;

use backend\tests\AcceptanceTester;
use backend\tests\utils\BaseTest;
use common\components\Model;
use common\components\utils\DefaultMessage;
use common\modules\adminGw\models\Page;


class PageCest extends BaseTest {
	public function _before(AcceptanceTester $I) {
		$this->pageName   = Page::getPageName();
		$this->urlDefault = Page::getPageUrl();
	}

	private function insertAPageForTest(AcceptanceTester $I): Model {
		$time = time();
		return static::_insertModel($I, Page::class, [
			'identifier'    => 'PageTest' . $time,
			'url'           => '/page/' . $time,
			'title'         => 'Page test ' . $time,
			'show_in_menu'  => 0,
			'order_in_menu' => 0,
			'permissions'   => '',
			'parent'        => NULL,
			'icon'          => NULL,
			'user_created'  => 1,
		]);
	}

	public function tryFilterByField(AcceptanceTester $I) {
		$fields = ["title", "url"];
		$page   = $this->insertAPageForTest($I);
		foreach ($fields as $field) {
			foreach ($this->operation as $operation) {
				$this->_accessPage($I);
				$this->_filterByField($I, $field, $operation, $page->$field);
				$I->see($page->title);
			}
		}
	}

	public function trySaveANewRecord(AcceptanceTester $I) {
		$time = time();
		$this->_accessPage($I);
		$this->_clickOnNewButton($I, 'a');
		$I->fillField(['name' => 'Page[title]'], "Page test {$time}");
		$I->fillField(['name' => 'Page[url]'], "/page/{$time}");
		$I->fillField(['name' => 'Page[identifier]'], "PageTest{$time}");
		$I->fillField(['name' => 'Page[order_in_menu]'], 0);
		$I->fillField(['name' => 'Page[permissions]'], '');
		$I->fillField(['name' => 'Page[icon]'], '');
		$I->see('Adicionar e continuar cadastrando');
		$I->click('button_submit');
		$I->wait(0.5);
		$I->see(DefaultMessage::sucessOnInsert($this->pageName, 'a'));
		$I->seeRecord(Page::class, ['identifier' => "PageTest{$time}"]);
	}

	public function trySaveANewRecordAndBack(AcceptanceTester $I) {
		$time = time();
		$this->_accessPage($I);
		$this->_clickOnNewButton($I, 'a');
		$I->fillField(['name' => 'Page[title]'], "Page test {$time}");
		$I->fillField(['name' => 'Page[url]'], "/page/{$time}");
		$I->fillField(['name' => 'Page[identifier]'], "PageTest{$time}");
		$I->fillField(['name' => 'Page[order_in_menu]'], 0);
		$I->fillField(['name' => 'Page[permissions]'], '');
		$I->fillField(['name' => 'Page[icon]'], '');
		$I->see('Adicionar e ir para listagem');
		$I->click('button_submit-and-back');
		$I->wait(0.5);
		$I->see(DefaultMessage::sucessOnInsert($this->pageName, 'a'));
		$I->seeRecord(Page::class, ['identifier' => "PageTest{$time}"]);
	}

	public function trySavingARegistryUpdate(AcceptanceTester $I) {
		$this->_accessPage($I);
		$page = $this->insertAPageForTest($I);
		$this->_filterByField($I, 'url', 'equal', $page->url);
		$I->see($page->url);
		$I->click($page->url);
		$I->amOnPage($this->urlDefault . '/update/' . $page->id);
		$title = $I->grabValueFrom(['name' => 'Page[title]']);
		$I->fillField(['name' => 'Page[title]'], "edited_{$title}");
		$I->see('Salvar e continuar editando');
		$I->click('button_submit');
		$I->wait(0.5);
		$pageName = Page::getPageName();
		$I->see(DefaultMessage::sucessOnUpdate($pageName, 'a'));
		$I->seeRecord(Page::class, ['title' => "edited_{$title}",'id'=>$page->id]);
	}

	public function trySavingARegistryUpdateAndBack(AcceptanceTester $I) {
		$this->_accessPage($I);
		$page = $this->insertAPageForTest($I);
		$this->_filterByField($I, 'url', 'equal', $page->url);
		$I->see($page->url);
		$I->click($page->url);
		$I->amOnPage($this->urlDefault . '/update/' . $page->id);
		$title = $I->grabValueFrom(['name' => 'Page[title]']);
		$I->fillField(['name' => 'Page[title]'], "edited_back_{$title}");
		$I->see('Salvar e ir para listagem');
		$I->click('button_submit');
		$I->wait(0.5);
		$pageName = Page::getPageName();
		$I->see(DefaultMessage::sucessOnUpdate($pageName, 'a'));
		$I->seeRecord(Page::class, ['title' => "edited_back_{$title}",'id'=>$page->id]);
	}

	public function tryDelete(AcceptanceTester $I) {
		$this->_accessPage($I);
		$page = $this->insertAPageForTest($I);
		$I->amOnPage($this->urlDefault . '/update/' . $page->id);
		$I->see('Deletar');
		$I->click('Deletar');
		$I->see('Deseja realmente remover?');
		$I->wait(0.5);
		$I->executeJS('return $("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button").get(0).click()');
		$I->wait(0.5);
		$I->seeInDatabase(Page::tableName(), ['id' => $page->id, 'deleted !=' => NULL]);
	}
}
