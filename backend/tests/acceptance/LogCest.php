<?php

namespace backend\tests\acceptance;

use backend\tests\AcceptanceTester;
use backend\tests\utils\BaseTest;
use common\components\Model;
use common\components\utils\DefaultMessage;
use common\modules\adminGW\models\Log;
use common\modules\user\models\User;

class LogCest extends BaseTest {
	protected $mockData = '{"Name":{"oldValue":"teste_de_log","newValue":"teste_de_log_atualizado"}}';

	public function _before(AcceptanceTester $I) {
		$this->pageName   = Log::getPageName();
		$this->urlDefault = Log::getPageUrl();
	}

	private function insertALogForTest(AcceptanceTester $I): Model {
		$time = time();
		return static::_insertModel($I, Log::class, [
			'user_id'     => 1,
			'entity_type' => 'Cliente',
			'entity_id'   => (User::find()->one())->id,
			'action'      => 2,
			'data'        => '{"Name":{"oldValue":"teste_de_log_","newValue":"teste_de_log_atualizado"}}',
		]);
	}

	public function tryFilterByField(AcceptanceTester $I) {
		$fields = ["user", "entity_type", "data"];
		$log    = $this->insertALogForTest($I);
		foreach ($fields as $field) {
			foreach ($this->operation as $operation) {
				$this->_accessPage($I);
				$term = $field === 'user' ? (User::find()->andWhere(['id' => $log->user_id])->one()->name) : $log->$field;
				$this->_filterByField($I, $field, $operation, $term);
				$I->seeInSource('teste_de_log_atualizado');
			}
		}
	}

	public function tryFilterByAction(AcceptanceTester $I) {
		foreach (Log::$_status as $actionId => $actionName) {
			$this->_accessPage($I);
			$I->selectOption("select[name=action_id]", ['value' => $actionId]);
			$I->see('Filtrar');
			$I->click('Filtrar');
			$I->dontSee(DefaultMessage::theSearchDidNotReturnData());
			$I->see($actionName);
		}
	}

	public function tryFilterByDate(AcceptanceTester $I) {
        $this->_accessPage($I);
        $log    = $this->insertALogForTest($I);
        $I->fillField(['name' => 'startDate'], $log->created);
        $I->fillField(['name' => 'endDate'], $log->created);
        $I->see('Filtrar');
        $I->click('Filtrar');
        $I->wait('1');
        $I->dontSee(DefaultMessage::theSearchDidNotReturnData());
        $I->see(date('d/m/Y H:i:s', strtotime($log->created)));
    }
}
