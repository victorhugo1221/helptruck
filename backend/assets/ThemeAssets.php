<?php

namespace backend\assets;

use yii\web\AssetBundle;

class ThemeAssets extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        '/css/inspinia-theme.css',
        '/css/bootstrap.min.css',
        '/css/font-awesome.css',
        '/css/animate.css',
        '/css/bundle.css',
        '/inspinia-theme/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        '/js/css/plugins/toastr/toastr.min.css',
        '/libs/select2/select2.min.css',
        '/css/plugins/dualListbox/multi-select.css',
        
        
        '/css/logo.css',
        '/css/desktop.css',
        '/css/mobile.css',
    ];
    
    public $js = [
        '/inspinia-theme/jquery-mask-plugin/dist/jquery.mask.min.js',
        '/inspinia-theme/moment/min/moment.min.js',
        '/inspinia-theme/moment/locale/pt-br.js',
        '/inspinia-theme/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        '/js/plugins/toastr/toastr.min.js',
        '/libs/select2/select2.full.min.js',
        '/libs/select2/i18n/pt-BR.js',
        'js/plugins/dualListbox/jquery.multi-select.js',
        'js/plugins/dualListbox/quicksearch.js',
        'js/plugins/notiflix/dist/notiflix-aio-2.7.0.min.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
