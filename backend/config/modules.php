<?php

use yii\filters\Cors;

return [
    'gii'                      => [
        'class' => 'yii\gii\Module',
    ],
    'as corsFilter'            => [
        'class' => Cors::className(),
        'cors'  => [
            'Origin'                           => ['*'],
            'Access-Control-Request-Method'    => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
            'Access-Control-Request-Headers'   => ['*'],
            'Access-Control-Allow-Credentials' => NULL,
            'Access-Control-Max-Age'           => 86400,
            'Access-Control-Expose-Headers'    => [],
        ],
    ],
    'rbac'                     => [
        'class'         => 'mdm\admin\Module',
        'controllerMap' => [
            'assignment' => [
                'class'         => 'mdm\admin\controllers\AssignmentController',
                'userClassName' => 'common\modules\user\models\User',
                'idField'       => 'id',
                'usernameField' => 'email',
                'fullnameField' => 'name',
            ],
        ],
    ],
    'dashboard'                => [
        'class' => 'backend\modules\dashboard\Module',
    ],
    'user'                     => [
        'class' => 'backend\modules\user\Module',
    ],
    'admin-gw'                 => [
        'class' => 'backend\modules\adminGw\Module',
    ],
    'profile'                  => [
        'class' => 'backend\modules\profile\Module',
    ],
    'profileProfilePermission' => [
        'class' => 'common\modules\profileProfilePermission\Module',
    ],
    'user-driver'                  => [
        'class' => 'backend\modules\userDriver\Module',
    ],
    'trip'                  => [
        'class' => 'backend\modules\trip\Module',
    ],
    'payment'                  => [
        'class' => 'backend\modules\payment\Module',
    ],
];