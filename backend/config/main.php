<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$smtpHost = $_ENV['SMTP_HOST'];
$smtpUser = $_ENV['SMTP_USER'];
$smtpPass = $_ENV['SMTP_PASS'];
$smtpPort = $_ENV['SMTP_PORT'];
$smtpSecure = $_ENV['SMTP_SECURE'];


return [
    'id' => $_ENV['PROJECT_ID'] . '-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => require __DIR__.'/modules.php',
    'components' => [
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'de',
                ],
            ],
        ],
        'view'         => [
            'class' => 'common\components\web\View',
        ],
        'user' => [
            'loginUrl'=>['user/default/login'],
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'session' => [
            'name' => $_ENV['PROJECT_ID'] . '-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $smtpHost,
                'username' => $smtpUser,
                'password' => $smtpPass,
                'port' => $smtpPort,
                'encryption' => $smtpSecure,
                'streamOptions' => [
                    'ssl' => [
                        'verify_peer' => FALSE,
                        'allow_self_signed' => TRUE,
                    ],
                ],
            ],
            'viewPath' => '@common/modules/email',
        ],
        'errorHandler' => [
            'errorAction' => 'dashboard/dashboard/error',
        ],
        'urlManager' => require __DIR__ . '/routes.php',
    ],
    'params' => $params,
];
