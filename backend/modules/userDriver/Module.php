<?php

namespace backend\modules\userDriver;

class Module extends \common\modules\userDriver\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
    }
}
