<?php

namespace backend\modules\userDriver\controllers;

use common\modules\userProvider\models\UserProvider;
use yii\helpers\ArrayHelper;
use common\modules\profile\models\Profile;
use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\user\models\NewPasswordForm;
use common\modules\user\models\UserClient;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\modules\adminGw\models\Page;
use common\components\web\Controller;
use common\modules\userDriver\models\UserDriver;
use mdm\admin\models\Assignment;
use function Webmozart\Assert\Tests\StaticAnalysis\null;

class UserDriverController extends Controller {
    private $dataService;
    
    public function init() {
        $this->pageTitle = $this->getPageName(UserDriver::class,'user_driver');
        $this->pageIdentifier = $this->getPageUrl(UserDriver::class,'user_driver');
        $this->dataService = $this->dataService ?? Yii::$app->getModule('user-driver')->get('userDriverService');
        parent::init();
    }
    
    public function behaviors() {
        $permissionName = 'UserDriver';
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "create{$permissionName}", "update{$permissionName}", "delete{$permissionName}"],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "create{$permissionName}"],
                    ],
                    [
                        'actions' => ['update', 'change-password'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "update{$permissionName}"],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "delete{$permissionName}"],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow'   => TRUE,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex() {
        $model  = UserDriver::find();
        $filter = Yii::$app->request->get();
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
    
    
    public function actionUpdate($id) {
        $model = UserDriver::find()->andWhere(['id' => $id])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros                      = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros                      = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->update($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'user-provider', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'user-provider', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle]);
        }
    }
    
}