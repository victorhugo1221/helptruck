<?php

namespace backend\modules\payment\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\payment\models\Payment;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\web\Controller;

use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class PaymentController extends Controller {
    private $dataService;

    public function init() {
        $this->pageTitle      = $this->getPageName(Payment::class, 'Payment');
        $this->pageIdentifier = $this->getPageUrl(Payment::class, 'Payment');
        $this->dataService    = $this->dataService ?? Yii::$app->getModule('payment')->get('paymentService');
        parent::init();
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow'   => TRUE,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model  = Payment::find();
        $filter = Yii::$app->request->get();

        $filter = $this->manipulatingTheFilterInSession($filter);

        $dataProvider = new ActiveDataProvider([
                                                   'query'      => $model->filter($filter),
                                                   'pagination' => [
                                                       'pageSizeLimit' => [1, 100],
                                                   ],
                                               ]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
    

    public function actionUpdate($id) {
        $model = Payment::find()->andWhere(['id' => $id])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);

            if (Yii::$app->request->get('validation', FALSE)) {
                $erros                      = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros                      = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }

            if ($this->dataService->update($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'plan', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'plan', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle]);
        }
    }

}