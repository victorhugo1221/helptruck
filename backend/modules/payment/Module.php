<?php

namespace backend\modules\payment;

class Module extends \common\modules\payment\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}