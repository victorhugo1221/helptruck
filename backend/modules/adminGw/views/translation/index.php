<?php

use common\components\utils\DefaultMessage;
use common\components\utils\Filter;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
$filterdButton = new Filter();
$this->title = 'Listagem de ' . $pageTitle;
$this->params['breadcrumbs'][] = ['label' => $pageTitle];
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);
$urlDefault = '/admin-gw/translation';

$readonly = !Yii::$app->user->can('manageTranslation');

$fields = [
    'key'  => 'Chave',
    'pt' => 'Português',
    'en' => 'Inglês',
    'es' => 'Espanhol',
];


$filter =  empty($filter) ? [] : $filter;

?>
<div class="row wrapper border-bottom page-heading p-0">
    <div class="col-xs-12 col-sm-12 col-md-6 page-title">
        <h2><?= $this->title ?></h2>
    </div>
    <div class="breadcrumb col-xs-12 col-sm-12 col-md-6">
        <?php
        echo Breadcrumbs::widget([
            'tag'                => 'ol',
            'itemTemplate'       => "<li>{link}&nbsp;&nbsp;/&nbsp;&nbsp;</li>\n",
            'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n",
            'links'              => $this->params['breadcrumbs'],
        ]);
        ?>
    </div>
</div>

<div class="row action-area">
    <div class="col-12 button-area">
        <?php if (Yii::$app->user->can('sysadmin')) { ?>
            <a class="btn btn-new btn-new-users btn-sm m-l-sm m-0"
               href="<?= Url::to([$urlDefault . '/create'], TRUE) ?>"><i class="fa fa-plus-circle"></i>
                Novo</a>
        <?php } ?>
    </div>
    <div class="col-12 filter-area">
        <div class="row">
            <div class="col-12">
                <form id="options-form-<?= $formName ?>" method="GET" action="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-9 pb-2 input-group">
                            <?= $filterdButton->showFilterField($fields, $filter)?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-3 pb-2 pr-0 dv-filter">
                            <?= $filterdButton->showFieldButtons($urlDefault) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="mail-box">
                <div class="row p-3 pt-0">
                    <div class="col-12 pt-0">
                        <?php
                        Pjax::begin([
                            'enablePushState'    => FALSE,
                            'enableReplaceState' => FALSE,
                            'formSelector'       => '#options-form-' . $formName,
                            'submitEvent'        => 'submit',
                            'timeout'            => 5000,
                            'clientOptions'      => ['maxCacheLength' => 0],
                        ]);
                        ?>

                        <?php
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'id'    => $formName . '-table',
                                'class' => 'table table-striped table-hover',
                            ],
                            'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '.</div>',
                            'layout'       => '{items}{pager}',
                            'options'      => [
                                'class'     => 'table-responsive',
                                'data-pjax' => TRUE
                            ],
                            'columns'      => [
                                [
                                    'attribute'      => 'id',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:50px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $data->id . '</a>';
                                    },
                                ],
                                [
                                    'attribute'     => 'key',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:250px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'         => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $data->key . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'pt',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:100px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        $res = !empty($data->pt) ? "Sim" : "Não";
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $res . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'en',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:100px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        $res = !empty($data->en) ? "Sim" : "Não";
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $res . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'es',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:100px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        $res = !empty($data->es) ? "Sim" : "Não";
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $res . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'cadastrado',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . date('d/m/Y H:i:m', strtotime($data->created)) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'alterado',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        if (!empty($data->updated)) {
                                            return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . date('d/m/Y H:i:m', strtotime($data->updated)) . '</a>';
                                        } else {
                                            return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link"> - </a>';
                                        }

                                    },
                                ],
                            ],
                        ]);
                        ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>