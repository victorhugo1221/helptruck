<?php

use common\components\utils\CrudButton;
use moonland\tinymce\TinyMCE;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>

    <div class="form-content">
        <div class="form-fields">
            <div class="row">
                <div class="col">
                    <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-12" data-component="slugify">
                                    <?= $form->field($model, 'key')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source',
                                        'disabled' => !Yii::$app->user->can('sysadmin')]); ?>
                                </div>
                                <div class="col-12">
                                    <div class="ibox lang-form" data-component="lang-form">
                                        <div class="ibox mt-3 pt-1">

                                            <?php if(Yii::$app->user->can('sysadmin')){ ?>
                                                <ul class="lang-form_options" style="position: absolute;right: 15px;top: 8px;">
                                                    <li class="lang-form_option is-active" data-lang="pt"></li>
                                                    <li class="lang-form_option" data-lang="en"></li>
                                                    <li class="lang-form_option" data-lang="es"></li>
                                                </ul>
                                            <?php } ?>
                                            <h4>Texto</h4>
                                        </div>
                                        <div class="ibox-content lang-form_lang lang-form_lang--pt is-active">
                                            <div class="row">
                                                <div class="col-xl-12 p-0"
                                                     data-component="tinymce-editor-html"
                                                     data-selector="#translation-pt"
                                                     data-plugins="searchreplace autolink autosave save directionality visualblocks visualchars link  template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount textpattern noneditable help charmap quickbars"
                                                     data-toolbar="undo redo | bold italic underline strikethrough | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify |
                                                     outdent indent |  numlist bullist | forecolor backcolor casechange removeformat | link | "
                                                     data-mobile-plugins=" print preview importcss tinydrive searchreplace autolink directionality visualblocks visualchars link  charmap
                                                     hr toc insertdatetime advlist lists wordcount tinymcespellchecker textpattern noneditable help charmap mentions quickbars linkchecker emoticons advtable"
                                                     data-mobile-menu-bar="edit view format tools"
                                                >
                                                    <?= $form->field($model, 'pt')->textarea(['rows' => '6']); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ibox-content lang-form_lang lang-form_lang--en">
                                            <div class="row">
                                                <div class="col-xl-12 p-0" data-component="tinymce-editor-html" data-selector="#translation-en">
                                                    <?= $form->field($model, 'en')->textarea(['rows' => '6']); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ibox-content lang-form_lang lang-form_lang--es">
                                            <div class="row">
                                                <div class="col-xl-12 p-0" data-component="tinymce-editor-html" data-selector="#translation-es">
                                                    <?= $form->field($model, 'es')->textarea(['rows' => '6']); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-footer row m-0">
            <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
                <?php
                if (!$readonly) {
                    $crudButton = new CrudButton();
                    $crudButton->page = '/admin-gw/translation/';
                    $crudButton->model = $model;
                    $crudButton->permissionsToCreate = 'manageTranslation;createTranslation;';
                    $crudButton->permissionsToUpdate = 'manageTranslation;updateTranslation';
                    $crudButton->permissionsToDelete = 'sysadmin';
                    $crudButton->showDelete = !empty($model->editable_by_customer) ? false : true;
                    echo $crudButton->showCrudButtons($crudButton);
                }
                ?>
            </div>
        </div>
    </div>
<?php

function tinyConfiguration(string $name): array {
    return [
        'name' => "Translation[{$name}]",
        'selector' => 'textarea',
        'menubar' => FALSE,
        'height' => 100,
        'contextmenu' => 'undo redo',
        'force_p_newlines' => FALSE,
        'forced_root_block' => '',
        'toolbar' => [
            'undo redo',
            'bold italic underline strikethrough',
            'alignleft aligncenter alignright alignjustify',
            'numlist bullist',
            'link',
            'formatpainter removeformat',
        ],
    ];
}

ActiveForm::end();
?>