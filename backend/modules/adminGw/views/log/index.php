<?php

use common\components\utils\DefaultMessage;
use common\components\utils\Filter;
use common\modules\adminGw\models\Log;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;


$filterdButton = new Filter();
$this->title = 'Listagem de ' . $pageTitle;
$this->params['breadcrumbs'][] = ['label' => $pageTitle];
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);
$urlDefault = '/admin-gw/log';

$fields = [
    'user'        => 'Usuário',
    'entity_type' => 'Entidade',
    'data'        => 'Dados',
];

$filter = empty($filter) ? [] : $filter;

$start = date('d/m/Y',mktime (0, 0, 0, date("m"), date("d")-30,  date("Y")));
$end = date('d/m/Y',mktime (23, 59, 59, date("m"), date("d"),  date("Y")));

$startDate = empty($filter['startDate']) ? $start : $filter['startDate'];
$endDate = empty($filter['endDate']) ? $end : $filter['endDate'];

?>
<div class="row wrapper border-bottom page-heading p-0">
    <div class="col-xs-12 col-sm-12 col-md-6 page-title">
        <h2><?= $this->title ?></h2>
    </div>
    <div class="breadcrumb col-xs-12 col-sm-12 col-md-6">
        <?php
        echo Breadcrumbs::widget([
            'tag'                => 'ol',
            'itemTemplate'       => "<li>{link}&nbsp;&nbsp;/&nbsp;&nbsp;</li>\n",
            'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n",
            'links'              => $this->params['breadcrumbs'],
        ]);
        ?>
    </div>
</div>

<div class="row action-area">
    <div class="col-12 filter-area">
        <div class="row">
            <div class="col-12">
                <form id="options-form-<?= $formName ?>" method="GET" action="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 col-xl-6 pb-2 input-group">
                            <?= $filterdButton->showFilterField($fields, $filter) ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6 col-xl-4 pb-2">
                            <select class="input form-control filter-select" name="action_id" id="action_id">
                                <option value="">Ação</option>
                                <?php
                                foreach (Log::$_status as $actionId => $actionName) {
                                    $field = !(isset($filter) && count($filter) > 0 && isset($filter['profile'])) ? NULL : $filter['profile']; ?>
                                    <option value="<?= $actionId; ?>" <?= $filterdButton->fieldSelected($field, $actionId); ?> >
                                        <?= $actionName; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 col-xl-5 pb-2">
                            <div class="input-daterange input-group" data-component="form">
                                <input type="text" class="input-sm form-control datetimepicker text-center"
                                       data-mask="00/00/0000" name="startDate" placeholder="Data de"
                                       value="<?= $startDate ?>"/>
                                <span class="input-group-addon">até</span>
                                <input type="text" class="input-sm form-control datetimepicker text-center"
                                       data-mask="00/00/0000" name="endDate" placeholder="Data até"
                                       value="<?= $endDate ?>"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 pb-2">
                            <?= $filterdButton->showFieldButtons($urlDefault) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="mail-box p-t-sm">
                <div class="row">
                    <div class="col-12 pt-4">
                        <div class="col-12">
                            <?php
                            Pjax::begin([
                                'enablePushState'    => FALSE,
                                'enableReplaceState' => FALSE,
                                'formSelector'       => '#options-form-' . $formName,
                                'submitEvent'        => 'submit',
                                'timeout'            => 5000,
                                'clientOptions'      => ['maxCacheLength' => 0],
                            ]);
                            ?>
                            <div data-component="log-diff">
                                <?php
                                echo GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'id'    => $formName . '-table',
                                        'class' => 'table table-striped table-hover',
                                    ],
                                    'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '.</div>',
                                    'layout'       => '{items}{pager}',
                                    'options'      => [
                                        'data-pjax' => true,
                                        'class'     => 'table-responsive',
                                    ],
                                    'columns'      => [
                                        [
                                            'attribute'      => 'user_id',
                                            'format'         => 'html',
                                            'enableSorting'  => TRUE,
                                            'headerOptions'  => [
                                                'class' => 'text-left',
                                                'style' => 'min-width:150px'
                                            ],
                                            'contentOptions' => ['class' => 'text-left'],
                                            'value'          => function ($data) {
                                                return $data->userRel->name;
                                            },
                                        ],
                                        [
                                            'attribute'      => 'created',
                                            'format'         => 'html',
                                            'enableSorting'  => TRUE,
                                            'headerOptions'  => [
                                                'class' => 'text-left',
                                                'style' => 'min-width:150px'
                                            ],
                                            'contentOptions' => ['class' => 'text-left'],
                                            'value'          => function ($data) {
                                                return date('d/m/Y H:i:s', strtotime($data->created));
                                            },
                                        ],
                                        [
                                            'attribute'      => 'entity_type',
                                            'format'         => 'html',
                                            'enableSorting'  => TRUE,
                                            'headerOptions'  => [
                                                'class' => 'text-left',
                                                'style' => 'min-width:150px'
                                            ],
                                            'contentOptions' => ['class' => 'text-left'],
                                            'value'          => function ($data) {
                                                return $data->entity_type;
                                            },
                                        ],
                                        [
                                            'attribute'      => 'entity_id',
                                            'format'         => 'html',
                                            'enableSorting'  => TRUE,
                                            'headerOptions'  => [
                                                'class' => 'text-left',
                                                'style' => 'min-width:50px'
                                            ],
                                            'contentOptions' => ['class' => 'text-left'],
                                            'value'          => function ($data) {
                                                return $data->entity_id;
                                            },
                                        ],
                                        [
                                            'attribute'      => 'action',
                                            'format'         => 'html',
                                            'enableSorting'  => TRUE,
                                            'headerOptions'  => [
                                                'class' => 'text-left',
                                                'style' => 'min-width:100px'
                                            ],
                                            'contentOptions' => ['class' => 'text-left'],
                                            'value'          => function ($data) {
                                                return Log::$_status[$data->action];
                                            },
                                        ],
                                        [
                                            'attribute'      => 'data',
                                            'format'         => 'html',
                                            'enableSorting'  => false,
                                            'contentOptions' => ['class' => 'log-diff_container'],
                                            'headerOptions'  => ['style' => 'min-width:150px'],
                                            'value'          => function ($data) {

                                                $log = json_decode($data->data);
                                                $oldData = [];
                                                $newData = [];
                                                foreach ($log as $fieldName => $values) {
                                                    $oldData[$fieldName] = $values->oldValue;
                                                    $newData[$fieldName] = $values->newValue;
                                                }

                                                return base64_encode(json_encode(
                                                        [
                                                            'oldData' => $oldData,
                                                            'newData' => $newData
                                                        ]
                                                    )
                                                );
                                            },
                                        ]
                                    ],
                                ]);
                                ?>
                            </div>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
