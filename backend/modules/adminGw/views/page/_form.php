<?php

use common\components\utils\CrudButton;
use common\modules\adminGw\models\Page;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<?php
$form = ActiveForm::begin([
    'id' => 'formulário',
    'options' => [
        'class' => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly' => $readonly ? 1 : 0,
    ],
    'validationUrl' => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableAjaxValidation' => TRUE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit' => TRUE,
    'validateOnChange' => FALSE,
    'validateOnBlur' => FALSE,
]);
?>

<div class="form-content">
    <div class="form-fields">
        <div class="row">
            <div class="col">
                <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'title')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]) ?>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'parent')->dropDownList(ArrayHelper::map(Page::find()->all(), 'id', 'title'), ['prompt' => 'Selecione', 'class' => 'form-control']) ?>
                            </div>
                            <div class="col-2">
                                <label>Exibir no menu?</label>
                                <?= $form->field($model, 'show_in_menu')->checkbox(['data-component' => 'toggle', 'data-switchery' => TRUE, 'checked' => (($model->show_in_menu || $model->isNewRecord) > 0 ? 'true' : NULL)], FALSE)->label(FALSE) ?>
                            </div>
                            <div class="col-6">
                                <?= $form->field($model, 'url')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'placeholder' => '/sistema/paginas ou # para menu pai']) ?>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'icon')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]) ?>
                            </div>
                            <div class="col-2">
                                <?= $form->field($model, 'order_in_menu')->textInput(['type' => 'number', 'class' => 'form-control form_field-input', 'maxlength' => TRUE, 'placeholder' => '1,2,3...']) ?>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'identifier')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'placeholder' => 'user, notification, profile, guide ... ']) ?>
                            </div>
                            <div class="col-8">
                                <?= $form->field($model, 'permissions')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'placeholder' => 'sysadmin;gerenciarPagina;visualizarPagina;criarPagina;atualizarPagina;deletarPagina']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer">
        <div class='d-flex bd-highlight'>
            <?php if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/admin-gw/page/';
                $crudButton->model = $model;
                $crudButton->permissionsToCreate = 'sysadmin;';
                $crudButton->permissionsToUpdate = 'sysadmin;';
                $crudButton->permissionsToDelete = 'sysadmin;';
                echo $crudButton->showCrudButtons($crudButton);
            } ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
