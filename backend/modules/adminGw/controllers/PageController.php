<?php

namespace backend\modules\adminGw\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\adminGw\models\Page;
use common\modules\user\models\User;
use Yii;
use common\components\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class PageController extends Controller {
    private $dataService;
    
    public function init() {
        $this->pageTitle = $this->getPageName(Page::class);
        $this->pageIdentifier = $this->getPageUrl(Page::class);
        $this->dataService = $this->dataService ?? Yii::$app->getModule('admin-gw')->get('pageService');
        parent::init();
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow' => TRUE,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $model = Page::find();
        $filter = Yii::$app->request->get();
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
    
    public function actionCreate() {
        $model = new Page();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->create($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_CREATE);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsert($this->pageTitle, 'a'));
            }
        }
        return $this->render('create', ['model' => $model, 'pageTitle' => $this->pageTitle]);
    }
    
    public function actionUpdate($id) {
        $model = Page::findOne($id);
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->update($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_UPDATE, 'page', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_UPDATE, 'page', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle]);
        }
    }
    
    public function actionDelete($id) {
        if ($this->dataService->delete($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle, 'a'));
            return $this->redirect(['index']);
        } else {
            return ['sucesso' => FALSE, 'mensagem' => DefaultMessage::errorOnDelete($this->pageTitle, 'a')];
        }
    }
}