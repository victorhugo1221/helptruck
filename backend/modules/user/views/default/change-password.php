<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'id' => 'password-form',
    'options' => ['class' => 'm-t ajax-form'],
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
    ],
    'method' => 'POST',
    'validationUrl' => Url::to(['change-password', 'token' => (string) $token, 'validation' => TRUE], TRUE),
    'enableAjaxValidation' => TRUE,
    'enableClientValidation' => TRUE,
    'validateOnSubmit' => TRUE,
    'validateOnChange' => FALSE,
    'validateOnBlur' => FALSE,
]); ?>


<?php if ($status) { ?>
    <div class="alert alert-danger text-center -msg">
        <?php echo $status->getMessage(); ?>
    </div>
<?php } ?>

    <p>Por favor, informe sua senha</p>

<?= $form->field($model, 'password')->passwordInput(['value' => '', 'placeholder' => 'Senha'])->label(FALSE) ?>
<?= $form->field($model, 'confirmation')->passwordInput(['placeholder' => 'Confirmar Senha'])->label(FALSE) ?>




    <div class="container-fluid">
        <div class="row action-forgot-password">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0 mt-2 pt-1 forgot-password">
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <?= Html::submitButton('Salvar Senha', ['class' => 'btn btn-login-w btn-block', 'name' => 'entrar']) ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>