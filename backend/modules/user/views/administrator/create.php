<?php

use yii\widgets\Breadcrumbs;

$this->title = 'Novo ' . $pageTitle;
$this->params['breadcrumbs'][] = ['label' => $pageTitle, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$readonly = !(Yii::$app->user->can('sysadmin') || Yii::$app->user->can('manageAdministrator') || Yii::$app->user->can('createAdministrator'));
?>
<div class="row wrapper border-bottom page-heading p-0">
    <div class="col-xs-12 col-sm-12 col-md-6 page-title">
        <h2><?= $this->title ?></h2>
    </div>
    <div class="breadcrumb col-xs-12 col-sm-12 col-md-6">
        <?php
        echo Breadcrumbs::widget([
            'tag'                => 'ol',
            'itemTemplate'       => "<li>{link}&nbsp;&nbsp;/&nbsp;&nbsp;</li>\n",
            'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n",
            'links'              => $this->params['breadcrumbs'],
        ]);
        ?>
    </div>
</div>

<?= $this->renderAjax('_form', [
    'model' => $model,
    'readonly' => $readonly,
    'allPermissions' => $allPermissions,
]) ?>
