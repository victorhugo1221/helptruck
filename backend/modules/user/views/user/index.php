<?php

use common\components\utils\DefaultMessage;
use common\components\utils\Filter;
use common\components\utils\Text;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use common\modules\user\models\User;

$filterdButton = new Filter();
$this->title = 'Listagem de ' . $pageTitle;
$this->params['breadcrumbs'][] = ['label' => $pageTitle];
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);

$urlDefault = '/user/user';

$readonly = !(Yii::$app->user->can('sysadmin') || Yii::$app->user->can('manageUser'));

$fields = [
    'name'  => 'Nome',
    'email' => 'E-mail',
    'cpf'   => 'CPF',
];


$filter =  empty($filter) ? [] : $filter;

?>
<div class="row wrapper border-bottom page-heading p-0">
    <div class="col-xs-12 col-sm-12 col-md-6 page-title">
        <h2><?= $this->title ?></h2>
    </div>
    <div class="breadcrumb col-xs-12 col-sm-12 col-md-6">
        <?php
        echo Breadcrumbs::widget([
            'tag'                => 'ol',
            'itemTemplate'       => "<li>{link}&nbsp;&nbsp;/&nbsp;&nbsp;</li>\n",
            'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n",
            'links'              => $this->params['breadcrumbs'],
        ]);
        ?>
    </div>
</div>

<div class="row action-area">
    <div class="col-12 filter-area">
        <div class="row">
            <div class="col-12">
                <form id="options-form-<?= $formName ?>" method="GET" action="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 pb-2 col-lg-5 input-group">
                            <?= $filterdButton->showFilterField($fields, $filter)?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 pb-2">
                            <select class="input form-control filter-select" name="status" id="status">
                                <option value="">Status</option>
                                <?php foreach (User::$_status as $key => $status) {
                                    $field = !(isset($filter) && count($filter) > 0 && isset($filter['status'])) ? NULL : $filter['status']; ?>
                                    <option value="<?= $key; ?>" <?= $filterdButton->fieldSelected($field, $key); ?> >
                                        <?= $status; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-3 pb-2 pr-0 dv-filter">
                            <?= $filterdButton->showFieldButtons($urlDefault) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="mail-box">
                <div class="row p-3 pt-0">
                    <div class="col-12 pt-0">
                        <?php
                        Pjax::begin([
                            'enablePushState'    => FALSE,
                            'enableReplaceState' => FALSE,
                            'formSelector'       => '#options-form-' . $formName,
                            'submitEvent'        => 'submit',
                            'timeout'            => 5000,
                            'clientOptions'      => ['maxCacheLength' => 0],
                        ]);

                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'id'    => $formName . '-table',
                                'class' => 'table table-striped table-hover',
                            ],
                            'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '</div>',
                            'layout'       => '{items}{pager}',
                            'options'      => [
                                'class'     => 'table-responsive',
                                'data-pjax' => TRUE
                            ],
                            'columns'      => [
                                [
                                    'attribute'     => 'name',
                                    'label'         => 'Nome',
                                    'format'        => 'html',
                                    'enableSorting' => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'         => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->user_id], TRUE) . '" class="client-link">' . $data->userRel->name . '</a>';
                                    },
                                ],
                                [
                                    'attribute'     => 'email',
                                    'format'        => 'html',
                                    'enableSorting' => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:250px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'         => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->user_id], TRUE) . '" class="client-link">' . $data->userRel->email . '</a>';
                                    },
                                ],
                                [
                                    'attribute'     => 'cpf',
                                    'format'        => 'html',
                                    'enableSorting' => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'         => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->user_id], TRUE) . '" class="client-link">' . Text::maskCpf($data->userRel->cpf) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'     => 'profile_id',
                                    'format'        => 'html',
                                    'enableSorting' => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:200px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'         => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->user_id], TRUE) . '" class="client-link">' . Text::maskCpf($data->profileRel->name) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'status',
                                    'label'          => 'Status',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:200px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) {
                                        if ($data->status == User::STATUS_PENDENT) {
                                            return '<i class="fas fa-user-clock color-status-pendent"></i> '  . User::$_status[$data->status];
                                        } else if ($data->status == User::STATUS_SELF_INACTIVE) {
                                            return '<i class="fas fa-user-minus color-status-inactive"></i> '  . User::$_status[$data->status];
                                        } else if ($data->status == User::STATUS_ADMIN_INACTIVE) {
                                            return '<i class="fas fa-user-lock color-status-inactive"></i> '  . User::$_status[$data->status];
                                        } else {
                                            return '<i class="fas fa-user-check color-status-active"></i> '  . User::$_status[$data->status];
                                        }
                                    },
                                ],
                            ],
                        ]);

                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
