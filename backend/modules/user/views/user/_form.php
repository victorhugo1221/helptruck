<?php

use common\components\utils\CrudButton;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableAjaxValidation'   => TRUE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>


<div class="form-content">
    <div class="form-fields">
        <div class="row">
            <div class="col">
                <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Informações</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'disabled' => TRUE]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'cpf')->textInput(['maxlength' => TRUE, 'data-mask' => '999.999.999-99', 'disabled' => TRUE]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/user/user/';
                $crudButton->model = $model;
                $crudButton->permissionsToUpdate = 'manageUser;updateUser';
                $crudButton->permissionsToDelete = 'manageUser;deleteUser';
                $urlChangePassword = URL::to([$crudButton->page . 'change-password/'], TRUE);
                $crudButton->additionalButtons = (!$model->isNewRecord) ?
                    "<button type='button' class='btn btn-generic-action-outline col-xs-12 col-sm-12 col-md-3 col-lg-2 col-xl-1 ml-xs-0 ml-sm-0 ml-md-2 mb-2' data-toggle='modal' data-target='#change-password'>
                                    <i class='fas fa-key'></i> Alterar Senha
                                </button>" : "";
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


<div id="change-password" class="modal" tabindex="-1" role="dialog">
    <?php
    $form = ActiveForm::begin([
        'id'                     => 'formModel',
        'options'                => [
            'class'                 => '_model',
            'data-redirect-success' => Url::to(['index'], TRUE),
            'data-readonly'         => $readonly ? 1 : 0,
        ],
        'validationUrl'          => Url::to(['user/change-password', TRUE, 'validation' => TRUE, 'id' => $model->id]),
        'action'                 => ['user/change-password', 'id' => $model->id],
        'enableAjaxValidation'   => TRUE,
        'enableClientValidation' => FALSE,
        'validateOnSubmit'       => TRUE,
        'validateOnChange'       => FALSE,
        'validateOnBlur'         => FALSE,
    ]);
    ?>
    <div class="modal-dialog" role="document">
        <div class="modal-content" data-component="modal" data-url="<?= $crudButton->page ?> change-password">
            <div class="modal-header">
                <h5 class="modal-title">Alteração de Senha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <?= $form->field($newPassword, 'password')->passwordInput(['required' => TRUE]); ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($newPassword, 'confirmation')->passwordInput(['required' => TRUE]); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-delete-outline" data-dismiss="modal"><i class="fas fa-times"></i> Cancelar
                </button>
                <?= Html::submitButton("<i class='fa fa-plus'></i> Salvar", ['class' => 'btn btn-save', 'name' => 'button_submit', 'value' => 'list', 'form' => 'formModel']); ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
