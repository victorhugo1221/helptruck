<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use common\modules\adminGw\exceptions\TokenException;
use common\modules\adminGw\models\Token;
use common\modules\user\models\ForgotMyPasswordForm;
use common\modules\user\models\NewPasswordForm;
use common\modules\user\models\LoginForm;
use grupow\base\exceptions\EntityNotFoundException;
use grupow\base\exceptions\ValidationException;
use grupow\rest\components\ActionStatus;

use backend\controllers\Controller;

class DefaultController extends Controller {
    public $layout = '/guest';
    public $service = NULL;
    
    public function init() {
        parent::init();
        Yii::$app->errorHandler->errorAction = 'user/default/error';
        $this->service = Yii::$app->getModule('user')->get('userService');
        return $this;
    }
    
    public function behaviors() {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['change-password', 'login', 'logout', 'forgot-my-password'],
                        'allow' => TRUE,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['panel'],
                        'allow' => TRUE,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }
    
    public function actionPainel() {
        return $this->redirect(['/dashboard/dashboard/index']);
    }
    
    public function actionForgotMyPassword() {
        $service = $this->service;
        $form = new ForgotMyPasswordForm();
        if (Yii::$app->request->isPost) {
            $form->load(Yii::$app->request->post());
            try {
                if (Yii::$app->request->get('validation', FALSE)) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $service->validate($form);
                    return [];
                }
                $service->requestNewPassword($form);
                $message = 'Solicitação de troca de senha enviada com sucesso.';
                if (Yii::$app->request->isAjax) {
                    return $this->actionStatus($message);
                }
                Yii::$app->session->setFlash('success', $message);
                return $this->refresh();
            } catch (ValidationException $e) {
                if (Yii::$app->request->isAjax) {
                    return $this->formatErrors($form);
                }
            }
        }
        
        return $this->render('forgot-my-password', ['model' => $form]);
    }
    
    public function actionChangePassword($token) {
        $service = $this->service;
        $tokenService = Yii::$app->getModule('admin-gw')->get('tokenService');
        
        $status = NULL;
        
        try {
            $token = $tokenService->findByUid($token);
        } catch (EntityNotFoundException $e) {
            $status = new ActionStatus('Token inválido', 400);
        }
        
        $form = new NewPasswordForm();
        
        if (Yii::$app->request->isPost) {
            $form->load(Yii::$app->request->post());

            try {
                if (Yii::$app->request->get('validation', FALSE)) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $service->validate($form);
                    return [];
                }
                
                $service->recoveryPassword($form, $token);
                Yii::$app->session->setFlash('success', 'Sua senha foi definida com successo!');
                return $this->redirect(['/login']);
                
            } catch (ValidationException $e) {
                if (Yii::$app->request->isAjax) {
                    return $this->formatErrors($form);
                }
            } catch (TokenException $e) {
                $status = new ActionStatus($e->getMessage(), $e->getError());
            }
        }
        return $this->render('change-password', ['model' => $form, 'token' => $token, 'status' => $status]);
    }
    
    public function actionLogout() {
        $this->service->logout();
        return $this->goHome();
    }
    
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $authenticationService = Yii::$app->getModule('admin-gw')->get('authenticationService');
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $authenticationService->loginAdministrator($model);
            if ($user) {
                return $this->redirect('/');
            } else {
                $this->returnsUserOrPasswordIncorrect($model);
            }
        }
        return $this->render('login', ['model' => $model]);
    }
    
    private function returnsUserOrPasswordIncorrect($model) {
        $model->addError('login', 'Os dados informados não conferem');
    }
}
