<?php

namespace backend\modules\user\controllers;

use yii\helpers\ArrayHelper;
use common\modules\profile\models\Profile;
use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\user\models\NewPasswordForm;
use common\modules\user\models\UserClient;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\modules\adminGw\models\Page;
use common\components\web\Controller;
use common\modules\user\models\User;
use mdm\admin\models\Assignment;
use function Webmozart\Assert\Tests\StaticAnalysis\null;

class UserController extends Controller {
    private $dataService;

    public function init() {
        $this->pageTitle = $this->getPageName(UserClient::class);
        $this->pageIdentifier = $this->getPageUrl(UserClient::class);
        $this->dataService = $this->dataService ?? Yii::$app->getModule('user')->get('userService');
        parent::init();
    }

    public function behaviors() {
        $permissionName = 'User';
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "create{$permissionName}", "update{$permissionName}", "delete{$permissionName}"],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "create{$permissionName}"],
                    ],
                    [
                        'actions' => ['update', 'change-password'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "update{$permissionName}"],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ["manage{$permissionName}", "delete{$permissionName}"],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow'   => TRUE,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = $this->dataService->find();
        $filter = Yii::$app->request->get();

        $filter = $this->manipulatingTheFilterInSession($filter);

        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }

    public function actionUpdate($id) {
        $errors = [];
        $modelUser = User::find()->where(['IS', User::tableName() . '.deleted', null])->andWhere(['id' => $id])->one();
        if ($modelUser == null) {
            Yii::$app->session->setFlash('error', DefaultMessage::registerNotFound($this->pageTitle,'o'));
            return $this->redirect(['index']);
        }
        $modelUserClient = UserClient::find()->andWhere(['>', 'user_client.status', -1])->all();
        $profile = [];
        $profiles = Profile::find()->asArray()->all();
        $profiles = ArrayHelper::map($profiles, 'id', 'name');
        foreach ($modelUserClient as $item) {
            $profile[$item->profile_id] = $profiles[$item->profile_id];
        }
        $modelUser->profiles = $profile;

        $newPassword = new NewPasswordForm();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $modelUser->load($post);
            $modelUserClient->load($post);
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($modelUser));
                $errors = array_merge($errors, ActiveForm::validate($modelUserClient));
                return $errors;
            }

            if ($this->dataService->updateClient($modelUser, $modelUserClient)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle,'o'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_UPDATE, 'user', $modelUser->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle,'o'));
                return $this->redirect(['user/update/' . $modelUser->id]);
            }
        } else {
            return $this->render('update', ['model' => $modelUser, 'modelUserClient' => $modelUserClient, 'newPassword' => $newPassword, 'pageTitle' => $this->pageTitle]);
        }
    }

    public function actionDelete($id) {
        if ($this->dataService->deleteClient($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle,'o'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnDelete($this->pageTitle,'o'));
            return $this->redirect(['user/update/' . $id]);
        }
    }

    public function actionChangePassword($id) {
        $newPassword = new NewPasswordForm();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model = new NewPasswordForm();
            $model->load($post);

            if (Yii::$app->request->get('validation', FALSE)) {
                $errors = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                return $errors;
            }
        }

        $user = User::find()->andWhere(['id' => $id])->one();
        if ($this->dataService->recoveryUserPassword($newPassword, $user)) {
            Yii::$app->session->setFlash('success', 'Senha alterada com sucesso!');
            return $this->redirect(['user/update/' . $id]);
        } else {
            Yii::$app->session->setFlash('success', "Ocorreu um erro ao alterar a senha do {$this->pageTitle}!");
            return $this->redirect(['user/update/' . $id]);
        }
    }

}