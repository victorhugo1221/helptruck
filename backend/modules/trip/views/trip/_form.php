<?php

use common\components\utils\CrudButton;
use moonland\tinymce\TinyMCE;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
    
    <div class="form-content">
        <div class="form-fields">
            <div class="row">
                <div class="col">
                    <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-4" data-component="slugify">
                                    <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => !Yii::$app->user->can('sysadmin')]); ?>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-footer row m-0">
            <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
                <?php
                if (!$readonly) {
                    $crudButton = new CrudButton();
                    $crudButton->page = '/order/order/';
                    $crudButton->model = $model;
                    $crudButton->permissionsToCreate = 'manageOrder;createOrder;';
                    $crudButton->permissionsToUpdate = 'manageOrder;updateOrder';
                    $crudButton->showDelete = !empty($model->editable_by_customer) ? false : true;
                    echo $crudButton->showCrudButtons($crudButton);
                }
                ?>
            </div>
        </div>
    </div>
<?php

function tinyConfiguration(string $name): array {
    return [
        'name' => "Order[{$name}]",
        'selector' => 'textarea',
        'menubar' => FALSE,
        'height' => 100,
        'contextmenu' => 'undo redo',
        'force_p_newlines' => FALSE,
        'forced_root_block' => '',
        'toolbar' => [
            'undo redo',
            'bold italic underline strikethrough',
            'alignleft aligncenter alignright alignjustify',
            'numlist bullist',
            'link',
            'formatpainter removeformat',
        ],
    ];
}

ActiveForm::end();
?>