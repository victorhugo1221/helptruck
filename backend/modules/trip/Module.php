<?php

namespace backend\modules\trip;

class Module extends \common\modules\trip\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}