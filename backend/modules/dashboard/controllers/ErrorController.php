<?php
namespace frontend\modules\site\controllers;

use Yii;
use yii\web\Controller;

class ErrorController extends Controller
{

    public function behaviors()
    {
        return [];
    }

    public function actionIndex()
    {
        $this->view->title = 'Erro';
        
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null)
            return $this->render('index', ['exception' => $exception]);
        return $this->redirect('/');
    }
}
