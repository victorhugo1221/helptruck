<?php
namespace backend\modules\dashboard\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class DashboardController extends Controller
{

    public function init()
    {
        $this->view->title = $_ENV['PROJECT_NAME'];
        parent::init();
    }


    public function behaviors()
    {
        return [
            'access'     => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index',],
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionError()
    {
        $this->layout = false;
        
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null)
            return $this->render('error', [ 'exception' => $exception ]);
        return $this->redirect('/');
    }
}