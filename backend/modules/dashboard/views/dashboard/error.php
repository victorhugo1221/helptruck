<?php

use backend\assets\ThemeAssets;
use common\components\Url;
    ThemeAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= Yii::$app->configuration->get('_backend_default_title'); ?> - Erro</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <link rel="apple-touch-icon" sizes="180x180" href="<?= Url::to([Yii::$app->configuration->get('_backend_applce_icon')], true); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= Url::to([Yii::$app->configuration->get('_backend_favicon_png-16x16')], true); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= Url::to([Yii::$app->configuration->get('_backend_favicon_png-32x32')], true); ?>">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    	<?php $this->head() ?>
    </head>
    <body class="bg-error-01">
		<?= $this->beginBody(); ?>
            <? //= $this->render('@app/views/layouts/preloader.php'); ?>
            <div class="container-fluid h-100 error-01">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-12">
                        <div class="error-container mx-auto text-center">
                            <h1><?= $exception->getCode() > 0 ? $exception->getCode() : 'Oops!' ?></h1>
                            <h2>Encontramos um erro...</h2>
                            <p>Mas tem muitas outras páginas pra você ver.</p>
                            <a href="<?= Url::to(['/'], true); ?>" class="btn btn-shadow">
                                Voltar para o painel
                            </a>
                        </div> 
                    </div>
                    <?php if (YII_DEBUG) { ?>
                        <div class="debug" style="
                            position: fixed;
                            top: 50%;
                            left: 50%;
                            padding: 40px;
                            font-size: 14px;
                            transform: translate(-50%, -50%);
                            background: #ffffff;
                            line-height: 22px;
                            max-width: 90%;
                        ">
                            <div class="debug" style="color: red; font-size: 24px; margin-bottom: 10px;">Debug</div>
                            <div class="message" style="front-size: 16px;"><?= $exception->getMessage(); ?></div>
                            <div class="file"><?= $exception->getFile(); ?>:<?= $exception->getLine(); ?></div>
                            <div class="stack"><pre><?= $exception->getTraceAsString(); ?></pre></div>
                        </div>
                    <?php } ?>
                </div> 
            </div>
		<?= $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>