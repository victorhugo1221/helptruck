<?php


namespace backend\controllers;
use yii\web\Controller;
use yii\helpers\Url;
use Yii;

class SwaggerController extends Controller {
    public function actions(): array
    {
        return [
            'docs' => [
                'class' => 'yii2mod\swagger\SwaggerUIRenderer',
                'restUrl' => Url::to(['swagger/json']),
            ],
            'json' => [
                'class' => 'yii2mod\swagger\OpenAPIRenderer',
                'cache' => NULL,
                'scanDir' => [
                    Yii::getAlias('@api/components/utils/'),
                    Yii::getAlias('@api/modules/user/controllers/'),
                    Yii::getAlias('@common/modules/user/models/'),
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
}