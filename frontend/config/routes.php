<?php

return [
    'class' => 'codemix\localeurls\UrlManager',
    'languages' => ['pt', 'en', 'es'],
    'enableDefaultLanguageUrlCode' => false,
    'enableLanguagePersistence' => true,
    'enableLanguageDetection' => false,

    'enablePrettyUrl'     => true,
    'enableStrictParsing' => false,
    'showScriptName'      => false,
    'rules' => [
        ''								=>	'site/site/index',
        'debug/<controller>/<action>' => 'debug/<controller>/<action>'
    ],
];
