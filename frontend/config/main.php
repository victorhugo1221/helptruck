<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$baseUrl = $_ENV['BASEURL'];

$splitCamel = function ($str) {
    $splitCamelArray = preg_split('/(?=[A-Z])/', $str);
    $array = implode('-', $splitCamelArray);
    return strtolower($array);
};

$modules = [
    'debug' => [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.2.109']
    ],
];

$_real = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);
foreach (glob(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR) as $element) {
    $explode = explode(DIRECTORY_SEPARATOR, $element);
    $split = end($explode);
    $realPath = realpath($element);
    $modules[$splitCamel($split)] =
        implode('\\',
            explode(DIRECTORY_SEPARATOR,
                implode('',
                    explode($_real, $realPath)
                )
            )
        ) . '\\Module';
}

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'debug'],
    'language'            => 'pt',
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'request'      => [
            'csrfParam' => '_csrf-frontend',
            'parsers'   => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'view'         => [
            'class' => 'common\components\web\View',
        ],
        'session'      => [
            'name' => '_app_session',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/site/error',
        ],
        'urlManager'   => require __DIR__ . '/routes.php'
    ],
    'modules'             => $modules,
    'params'              => $params,
];
