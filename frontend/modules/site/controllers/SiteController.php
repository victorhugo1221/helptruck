<?php

namespace frontend\modules\site\controllers;

use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use common\models\Pais;
use common\components\Utils;
use yii\helpers\Url;
use yii\web\Response;


class SiteController extends Controller {
    public function actionIndex() {
        return $this->render('index');
    }
}
