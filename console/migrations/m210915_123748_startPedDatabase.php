<?php

use yii\db\Migration;

class m210915_123748_startPedDatabase extends Migration {
    //region Create tables
    
    private function _createCategory() : void {
        $this->createTable('category', [
            'id'      => $this->integer()->append('auto_increment')->notNull(),
            'name'    => $this->tinyInteger()->notNull(),
            'image'   => $this->text()->notNull(),
            'status'  => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted' => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createComplaint() : void {
        $this->createTable('complaint', [
            'id'          => $this->integer()->append('auto_increment')->notNull(),
            'title'       => $this->string(30)->notNull(),
            'description' => $this->string(255)->notNull(),
            'user_id'     => $this->integer()->notNull(),
            'status'      => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_complaint-user_id', 'complaint', 'user_id');
    }
    
    private function _createPayment() : void {
        $this->createTable('payment', [
            'id'           => $this->integer()->append('auto_increment')->notNull(),
            'price'          => $this->decimal(9, 2)->notNull(),
            'user_id'     => $this->integer()->notNull(),
            'method'       => $this->tinyInteger()->defaultValue(1)->notNull(),
            'response'     => $this->string(255)->notNull(),
            'complaint_id' => $this->integer()->notNull(),
            'status'       => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'      => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'      => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'      => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    
    private function _createUserDriver() : void {
        $this->createTable('user_driver', [
            'id'                              => $this->integer()->append('auto_increment')->notNull(),
            'document_photo'                  => $this->text()->notNull(),
            'selfie_document'                 => $this->text()->notNull(),
            'maximum_travel_distance'         => $this->decimal(9, 2)->null(),
            'average_rating'                  => $this->decimal(2, 1)->null(),
            'last_average_rating_calculation' => $this->dateTime()->null(),
            'status'             => $this->tinyInteger()->notNull(),
            'acceptance_of_system_rules'      => $this->dateTime()->null(),
            'firebase_token'                  => $this->string(255)->null(),
            'city_id'                         => $this->integer()->notNull(),
            'created'                         => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                         => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                         => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_driver-city_id', 'user_driver', 'city_id');
    }
    
    private function _createSubcategory() : void {
        $this->createTable('subcategory', [
            'id'          => $this->integer()->append('auto_increment')->notNull(),
            'name'        => $this->string(45)->notNull(),
            'image'       => $this->text()->notNull(),
            'user_id'     => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'status'      => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_subcategory-user_id', 'subcategory', 'user_id');
        $this->createIndex('idx_subcategory-category_id', 'subcategory', 'category_id');
    }
    
    private function _createUserProviderSubcategory() : void {
        $this->createTable('user_provider_subcategory', [
            'id'             => $this->integer()->append('auto_increment')->notNull(),
            'price'          => $this->decimal(9, 2)->notNull(),
            'user_id'        => $this->integer()->notNull(),
            'subcategory_id' => $this->integer()->notNull(),
            'status'         => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'        => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'        => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'        => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_provider_subcategory-user_id', 'user_provider_subcategory', 'user_id');
        $this->createIndex('idx_user_provider_subcategory-subcategory_id', 'user_provider_subcategory', 'subcategory_id');
    }
    
    private function _createTutorial() : void {
        $this->createTable('tutorial', [
            'id'              => $this->integer()->append('auto_increment')->notNull(),
            'user_type'       => $this->tinyInteger(4)->notNull(),
            'title'           => $this->string(100)->notNull(),
            'order'           => $this->tinyInteger(4)->notNull(),
            'type'            => $this->tinyInteger(4)->notNull(),
            'stored_location' => $this->string(255)->null(),
            'status'          => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'         => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'         => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'         => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createMenu() : void {
        $this->createTable('menu', [
            'id'        => $this->integer()->append('auto_increment')->notNull(),
            'user_type' => $this->tinyInteger()->notNull(),
            'status'    => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'   => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'   => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'   => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createMenuOption() : void {
        $this->createTable('menu_option', [
            'id'      => $this->integer()->append('auto_increment')->notNull(),
            'name'    => $this->string(100)->notNull(),
            'action'  => $this->string(100)->notNull(),
            'menu_id' => $this->integer()->notNull(),
            'status'  => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted' => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_menu_option-menu_id', 'menu_option', 'menu_id');
    }
    
    private function _createTrip() : void {
        $this->createTable('trip', [
            'id'                          => $this->integer()->append('auto_increment')->notNull(),
            'user_client_id'              => $this->integer()->notNull(),
            'user_driver_id'              => $this->integer()->notNull(),
            'subcategory_id'              => $this->integer()->notNull(),
            'price'                       => $this->decimal(9, 2)->notNull(),
            'receipt_photo'               => $this->text()->notNull(),
            'start_photo'                 => $this->text()->notNull(),
            'end_photo'                   => $this->text()->notNull(),
            'start_point'                 => $this->text()->notNull(),
            'end_point'                   => $this->text()->notNull(),
            'acceptance_date'             => $this->dateTime()->null(),
            'refusal_date'                => $this->dateTime()->null(),
            'cancellation_date'           => $this->dateTime()->null(),
            'start_date'                  => $this->dateTime()->null(),
            'end_date'                    => $this->dateTime()->null(),
            'time_trip'                   => $this->integer()->notNull(),
            'status'                      => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'                     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    
    private function _createNotification() : void {
        $this->createTable('notification', [
            'id'               => $this->integer()->append('auto_increment')->notNull(),
            'trip_id'  => $this->integer()->notNull(),
            'title'            => $this->string(50)->notNull(),
            'text'             => $this->string(1000)->notNull(),
            'send_push'        => $this->boolean()->defaultValue(0)->notNull(),
            'required_reading' => $this->boolean()->defaultValue(0)->notNull(),
            'reading_date'     => $this->dateTime()->null(),
            'status'           => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'          => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_notification-trip_id', 'notification', 'trip_id');
    }
    
    
    private function _createSystemSetting() : void {
        $this->createTable('system_setting', [
            'id'                          => $this->integer()->append('auto_increment')->notNull(),
            'maximum_travel_distance'     => $this->decimal(9, 2)->notNull(),
            'max_minutes_to_cancel'       => $this->integer()->notNull(),
            'cancellation_fee_percentage' => $this->decimal(9, 2)->notNull(),
            'cancellation_fee_amount'     => $this->decimal(9, 2)->notNull(),
            'created'                     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createUserAdministrator() : void {
        $this->createTable('user_administrator', [
            'id'                     => $this->integer()->append('auto_increment')->notNull(),
            'receive_omplaint_email' => $this->boolean()->defaultValue(0)->notNull(),
            'user_id'                => $this->integer()->notNull(),
            'created'                => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_administrator-user_id', 'user_administrator', 'user_id');
    }
    
    private function _createUserNotification() : void {
        $this->createTable('user_notification', [
            'id'              => $this->integer()->append('auto_increment')->notNull(),
            'user_id'         => $this->integer()->notNull(),
            'notification_id' => $this->integer()->notNull(),
            'status'          => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'         => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'         => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'         => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_notification-user_id', 'user_notification', 'user_id');
        $this->createIndex('idx_user_notification-notification_id', 'user_notification', 'notification_id');
    }
    
    private function _createForeignKeys() : void {
        $this->addForeignKey('FK_user_notification-user_id-user-id', 'user_notification', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_notification-notification_id-notification-id', 'user_notification', 'notification_id', 'notification', 'id');
        $this->addForeignKey('FK_user_administrator-user_id-user-id', 'user_administrator', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_pauses_in_execution-trip_id-trip-id', 'pauses_in_execution', 'trip_id', 'trip', 'id');
        $this->addForeignKey('FK_notification-trip_id-trip-id', 'notification', 'trip_id', 'trip', 'id');
        $this->addForeignKey('FK_appraisal-trip_id-trip-id', 'appraisal', 'trip_id', 'trip', 'id');
        $this->addForeignKey('FK_trip-user_client_id-user_client-id', 'trip', 'user_client_id', 'user_client', 'id');
        $this->addForeignKey('FK_trip-user_provider_id-user_provider-id', 'trip', 'user_provider_id', 'user_provider', 'id');
        $this->addForeignKey('FK_trip-trip_id-subcategory-id', 'trip', 'subcategory_id', 'subcategory', 'id');
        $this->addForeignKey('FK_menu_option-menu_id-menu-id', 'menu_option', 'menu_id', 'menu', 'id');
        $this->addForeignKey('FK_helper-user_provider_id-user_provider-id', 'helper', 'user_provider_id', 'user_provider', 'id');
        $this->addForeignKey('FK_user_provider_subcategory-user_id-user-id', 'user_provider_subcategory', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_provider_subcategory-subcategory_id-subcategory-id', 'user_provider_subcategory', 'subcategory_id', 'subcategory', 'id');
        $this->addForeignKey('FK_subcategory-user_id-user-id', 'subcategory', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_subcategory-category_id-category-id', 'subcategory', 'category_id', 'category', 'id');
        $this->addForeignKey('FK_user_provider-city_id-city-id', 'user_provider', 'city_id', 'city', 'id');
        $this->addForeignKey('FK_complaint_response-complaint_id-complaint-id', 'complaint_response', 'complaint_id', 'complaint', 'id');
        $this->addForeignKey('FK_complaint-user_id-user-id', 'complaint', 'user_id', 'user', 'id');
    }
    
    //endregion
    
    //region Rollback
    private function _dropIndex() : void {
        $this->dropIndex('idx_user_notification-user_id', 'user_notification');
        $this->dropIndex('idx_user_notification-notification_id', 'user_notification');
        $this->dropIndex('idx_user_administrator-user_id', 'user_administrator');
        $this->dropIndex('idx_notification-trip_id', 'notification');
        $this->dropIndex('idx_trip-user_client_id', 'trip');
        $this->dropIndex('idx_trip-user_provider_id', 'trip');
        $this->dropIndex('idx_trip-subcategory_id', 'trip');
        $this->dropIndex('idx_helper-user_provider_id', 'helper');
        $this->dropIndex('idx_user_provider_subcategory-user_id', 'user_provider_subcategory');
        $this->dropIndex('idx_user_provider_subcategory-subcategory_id', 'user_provider_subcategory');
        $this->dropIndex('idx_subcategory-user_id', 'subcategory');
        $this->dropIndex('idx_subcategory-category_id', 'subcategory');
        $this->dropIndex('idx_complaint_response-complaint_id', 'complaint_response');
        $this->dropIndex('idx_complaint-user_id', 'complaint');
        $this->dropIndex('idx_user_provider-city_id', 'user_provider');
    }
    
    private function _dropForeignKeys() {
        $this->dropForeignKey('FK_user_notification-user_id-user-id', 'user_notification');
        $this->dropForeignKey('FK_user_notification-notification_id-notification-id', 'user_notification');
        $this->dropForeignKey('FK_user_administrator-user_id-user-id', 'user_administrator');
        $this->dropForeignKey('FK_notification-trip_id-trip-id', 'notification');
        $this->dropForeignKey('FK_trip-user_client_id-user_client-id', 'trip');
        $this->dropForeignKey('FK_trip-user_provider_id-user_provider-id', 'trip');
        $this->dropForeignKey('FK_trip-trip_id-subcategory-id', 'trip');
        $this->dropForeignKey('FK_menu_option-menu_id-menu-id', 'menu_option');
        $this->dropForeignKey('FK_helper-user_provider_id-user_provider-id', 'helper');
        $this->dropForeignKey('FK_user_provider_subcategory-user_id-user-id', 'user_provider_subcategory');
        $this->dropForeignKey('FK_user_provider_subcategory-subcategory_id-subcategory-id', 'user_provider_subcategory');
        $this->dropForeignKey('FK_subcategory-user_id-user-id', 'subcategory');
        $this->dropForeignKey('FK_subcategory-category_id-category-id', 'subcategory');
        $this->dropForeignKey('FK_user_provider-city_id-city-id', 'user_provider');
        $this->dropForeignKey('FK_complaint_response-complaint_id-complaint-id', 'complaint_response');
        $this->dropForeignKey('FK_complaint-user_id-user-id', 'complaint');
        
    }
    
    private function _dropTables() : void {
        $this->dropTable('category');
        $this->dropTable('complaint');
        $this->dropTable('complaint_response');
        $this->dropTable('trip');
        $this->dropTable('subcategory');
        $this->dropTable('user_provider_subcategory');
        $this->dropTable('payment');
        $this->dropTable('tutorial');
        $this->dropTable('menu');
        $this->dropTable('menu_option');
        $this->dropTable('appraisal');
        $this->dropTable('notification');
        $this->dropTable('system_setting');
        $this->dropTable('user_driver');
        $this->dropTable('user_administrator');
        $this->dropTable('user_notification');
    }
    
    //endregion
    
    public function safeUp() {
        $this->_createCategory();
        $this->_createComplaint();
        $this->_createTrip();
        $this->_createPayment();
        $this->_createUserDriver();
        $this->_createSubcategory();
        $this->_createUserProviderSubcategory();
        $this->_createTutorial();
        $this->_createMenu();
        $this->_createMenuOption();
        $this->_createAppraisal();
        $this->_createNotification();
        $this->_createSystemSetting();
        $this->_createUserAdministrator();
        $this->_createUserNotification();
        $this->_createForeignKeys();
    }
    
    public function safeDown() {
        if ($_ENV['DB_HOST'] === 'localhost' || $_ENV['DB_HOST'] === '192.168.2.3') {
            $this->_dropForeignKeys();
            $this->_dropIndex();
            $this->_dropTables();
            $tableName = $this->db->tablePrefix . 'users';
            if (!$this->db->getTableSchema($tableName, TRUE) === NULL) {
                $this->dropTable('migration');
            }
        } else {
            echo "===================== ATTENTION ======================";
            echo " Rollback is not applicable in production environment.";
            echo "======================================================";
        }
    }
}
