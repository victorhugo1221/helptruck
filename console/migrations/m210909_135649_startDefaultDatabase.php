<?php

use console\migrations\InsertDefaultData;
use yii\db\Migration;

class m210909_135649_startDefaultDatabase extends Migration {
    
    //region Create default table
    private function _createAuthAssignment() : void {
        $this->createTable('auth_assignment', [
            'item_name'  => $this->string(64)->notNull(),
            'user_id'    => $this->string(64)->notNull(),
            'created_at' => $this->integer()->null(),
            'primary key (item_name,user_id)',
        ]);
    }
    
    private function _createAuthItem() : void {
        $this->createTable('auth_item', [
            'name'        => $this->string(64)->notNull(),
            'type'        => $this->integer()->notNull(),
            'description' => $this->text()->null(),
            'rule_name'   => $this->string(64)->null(),
            'data'        => $this->text()->null(),
            'created_at'  => $this->integer()->null(),
            'updated_at'  => $this->integer()->null(),
            'primary key(name)',
        ]);
        $this->createIndex('idx_auth_item-rule_name', 'auth_item', 'rule_name');
        $this->createIndex('idx_auth_item-type', 'auth_item', 'type');
    }
    
    private function _createAuthItemChild() : void {
        $this->createTable('auth_item_child', [
            'parent' => $this->string(64)->notNull(),
            'child'  => $this->string(64)->notNull(),
            'primary key (parent, child)',
        ]);
        $this->createIndex('idx_auth_item_child-child', 'auth_item_child', 'child');
    }
    
    private function _createAuthRule() : void {
        $this->createTable('auth_rule', [
            'name'       => $this->string(64)->notNull(),
            'data'       => $this->text()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
            'primary key (name)',
        ]);
    }
    
    private function _createLog() : void {
        $this->createTable('log', [
            'id'          => $this->integer(11)->append('auto_increment')->notNull(),
            'user_id'     => $this->integer(11)->null(),
            'entity_type' => $this->string(45)->notNull(),
            'entity_id'   => $this->integer()->notNull(),
            'action'      => $this->tinyInteger()->defaultValue(1)->notNull(),
            'data'        => $this->text()->notNull(),
            'created'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'primary key (id)',
        ]);
    }
    
    private function _createPage() : void {
        $this->createTable('page', [
            'id'            => $this->integer()->append('auto_increment')->notNull(),
            'identifier'    => $this->string(45)->notNull(),
            'url'           => $this->string(100)->notNull(),
            'title'         => $this->string(100)->notNull(),
            'show_in_menu'  => $this->boolean()->defaultValue(1)->notNull(),
            'order_in_menu' => $this->smallInteger()->defaultValue(1)->notNull(),
            'permissions'   => $this->text()->null(),
            'parent'        => $this->integer()->null(),
            'icon'          => $this->string(100)->null(),
            'user_created'  => $this->integer()->notNull(),
            'created'       => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'       => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'       => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createToken() : void {
        $this->createTable('token', [
            'id'         => $this->integer()->append('auto_increment')->notNull(),
            'user_id'    => $this->integer()->notNull(),
            'uid'        => $this->string(36)->notNull(),
            'active'     => $this->boolean()->defaultValue(1)->notNull(),
            'type'       => $this->integer()->null(),
            'last_nonce' => $this->integer()->defaultValue(0)->notNull(),
            'status'     => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'    => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'    => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'    => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idk_token-user_id', 'token', 'user_id');
    }
    
    private function _createTranslation() : void {
        $this->createTable('translation', [
            'id'                   => $this->integer()->append('auto_increment')->notNull(),
            'key'                  => $this->string(255)->notNull(),
            'pt'                   => $this->text()->null(),
            'en'                   => $this->text()->null(),
            'es'                   => $this->text()->null(),
            'editable_by_customer' => $this->boolean()->notNull()->defaultValue(0),
            'created'              => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'              => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'              => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createUser() : void {
        $this->createTable('user', [
            'id'               => $this->integer()->append('auto_increment')->notNull(),
            'is_administrator' => $this->boolean()->defaultValue(0)->notNull(),
            'name'             => $this->string(150)->notNull(),
            'cpf'              => $this->string(11)->notNull(),
            'email'            => $this->string(255)->notNull(),
            'birth_date'       => $this->date()->null(),
            'cellphone'        => $this->string(11)->null(),
            'security_code'    => $this->string(6)->null(),
            'password'         => $this->string(250)->null(),
            'status'           => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'          => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createUserClient() : void {
        $this->createTable('user_client', [
            'id'                         => $this->integer()->append('auto_increment')->notNull(),
            'user_id'                    => $this->integer()->notNull(),
            'acceptance_of_system_rules' => $this->dateTime()->null(),
            'firebase_token'             => $this->string(255)->null(),
            'avatar'                     => $this->string(255)->null(),
            'status'                     => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'                    => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                    => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                    => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_client-user_id', 'user_client', 'user_id');
    }
    
    private function _createForeignKeys() : void {
        $this->addForeignKey('FK_auth_assignment-item_name_auth_item-name', 'auth_assignment', 'item_name', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_auth_item-rule_name_auth_rule-name', 'auth_item', 'rule_name', 'auth_rule', 'name', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK_auth_item_child-child_auth_item-name', 'auth_item_child', 'child', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_auth_item_child-parent_auth_item-name', 'auth_item_child', 'parent', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_token-user_id_user-id', 'token', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_client-user_id_user-id', 'user_client', 'user_id', 'user', 'id');
    }
    //endregion
    
    //region Insert default pages
    private function _insertHome() : void {
        $this->insert('page', [
            'identifier'    => 'home',
            'url'           => '/',
            'title'         => 'Dashboard',
            'show_in_menu'  => 1,
            'order_in_menu' => 0,
            'permissions'   => 'allUser;',
            'icon'          => 'fas fa-home',
            'user_created'  => 1,
        ]);
    }
    
    private function _insertExit() : void {
        $this->insert('page', [
            'identifier'    => 'exit',
            'url'           => '/user/default/logout',
            'title'         => 'Sair',
            'show_in_menu'  => 1,
            'order_in_menu' => 32767,
            'permissions'   => 'allUser;',
            'icon'          => 'fas fa-sign-out-alt',
            'user_created'  => 1,
        ]);
    }
    
    private function _insertPageAdministrator() : void {
        $pageName = 'Administrador';
        $permissionName = 'Administrator';
        $this->insert('page', [
            'identifier'    => 'administrator',
            'url'           => '/user/administrator',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 1,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-user-shield',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageUserClient() : void {
        $pageName = 'Usuários';
        $permissionName = 'User';
        $this->insert('page', [
            'identifier'    => 'user_client',
            'url'           => '/user/user',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 2,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fa fa-users',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageUserDriver() : void {
        $pageName = 'Motoristas';
        $permissionName = 'UserDriver';
        $this->insert('page', [
            'identifier'    => 'user_driver',
            'url'           => '/user-driver/user-driver',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 3,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-truck',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageTrip() : void {
        $pageName = 'Viagens';
        $permissionName = 'Trip';
        $this->insert('page', [
            'identifier'    => 'trip',
            'url'           => '/trip/trip',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 4,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-road',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageApp() : void {
        $pageName = 'App';
        $permissionName = 'App';
        $this->insert('page', [
            'identifier'    => 'app',
            'url'           => '/app/app',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 6,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-mobile-alt',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageOrder() : void {
        $pageName = 'Pagamentos';
        $permissionName = 'Payment';
        $this->insert('page', [
            'identifier'    => 'payment',
            'url'           => '/payment/payment',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 5,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-file-invoice-dollar',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageNotification() : void {
        $pageName = 'Notificações';
        $permissionName = 'Notification';
        $this->insert('page', [
            'identifier'    => 'notification',
            'url'           => '/notification/notification',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 7,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-bell',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPagePage() : void {
        $pageName = 'Página';
        $permissionName = 'Page';
        $this->insert('page', [
            'identifier'    => 'page',
            'url'           => '/admin-gw/page',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 96,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-pager',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageRbac() : void {
        $this->insert('page', [
            'identifier'    => 'rbac',
            'url'           => '#',
            'title'         => 'RBAC',
            'show_in_menu'  => 1,
            'order_in_menu' => 99,
            'permissions'   => 'sysadmin;',
            'icon'          => 'fa fa-cogs',
            'user_created'  => 1,
        ]);
        
        $this->insert('page', [
            'identifier'    => 'role',
            'url'           => '/rbac/role',
            'title'         => 'Papel',
            'show_in_menu'  => 1,
            'order_in_menu' => 991,
            'parent'        => 11,
            'permissions'   => 'sysadmin;',
            'user_created'  => 1,
        ]);
        
        $this->insert('page', [
            'identifier'    => 'assignment',
            'url'           => '/rbac/assignment',
            'title'         => 'Atribuição',
            'show_in_menu'  => 1,
            'order_in_menu' => 992,
            'parent'        => 11,
            'permissions'   => 'sysadmin;',
            'user_created'  => 1,
        ]);
        
        $this->insert('page', [
            'identifier'    => 'permission',
            'url'           => '/rbac/permission',
            'title'         => 'Permissão',
            'show_in_menu'  => 1,
            'order_in_menu' => 993,
            'parent'        => 11,
            'permissions'   => 'sysadmin;',
            'user_created'  => 1,
        ]);
    }
    
    private function _insertPageTranslation() : void {
        $pageName = 'Tradução';
        $permissionName = 'Translation';
        $this->insert('page', [
            'identifier'    => 'translation',
            'url'           => '/admin-gw/translation',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 96,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};",
            'icon'          => 'fas fa-language',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName);
    }
    
    private function _insertPageLog() : void {
        $pageName = 'Log';
        $permissionName = 'Log';
        $this->insert('page', [
            'identifier'    => 'log',
            'url'           => '/admin-gw/log',
            'title'         => $pageName,
            'show_in_menu'  => 1,
            'order_in_menu' => 96,
            'permissions'   => "sysadmin;manage{$permissionName};create{$permissionName};update{$permissionName};delete{$permissionName};view{$permissionName};",
            'icon'          => 'fas fa-history',
            'user_created'  => 1,
        ]);
        
        $this->_insertPermissions($pageName, $permissionName, FALSE, FALSE, FALSE, TRUE);
    }
    
    private function _insertPermissions(string $pageName, string $permissionName, bool $create = TRUE, bool $update = TRUE, bool $delete = TRUE, bool $view = FALSE) : void {
        
        $this->insert('auth_item', ['name' => "manage{$permissionName}", 'type' => 1, 'description' => "Gerenciar {$pageName}"]);
        $this->insert('auth_item_child', ['parent' => 'sysadmin', 'child' => "manage{$permissionName}"]);
        $this->insert('auth_assignment', ['item_name' => "manage{$permissionName}", 'user_id' => 1]);
        
        if ($create) {
            $this->insert('auth_item', ['name' => "create{$permissionName}", 'type' => 2, 'description' => "Cadastrar {$pageName}"]);
            $this->insert('auth_item_child', ['parent' => "manage{$permissionName}", 'child' => "create{$permissionName}"]);
        }
        
        if ($update) {
            $this->insert('auth_item', ['name' => "update{$permissionName}", 'type' => 2, 'description' => "Atualizar {$pageName}"]);
            $this->insert('auth_item_child', ['parent' => "manage{$permissionName}", 'child' => "update{$permissionName}"]);
        }
        
        if ($delete) {
            $this->insert('auth_item', ['name' => "delete{$permissionName}", 'type' => 2, 'description' => "Deletar {$pageName}"]);
            $this->insert('auth_item_child', ['parent' => "manage{$permissionName}", 'child' => "delete{$permissionName}"]);
        }
        
        if ($view) {
            $this->insert('auth_item', ['name' => "view{$permissionName}", 'type' => 2, 'description' => "Visualizar {$pageName}"]);
            $this->insert('auth_item_child', ['parent' => "manage{$permissionName}", 'child' => "view{$permissionName}"]);
        }
    }
    //endregion
    
    //region Insert default data
    private function _insertSysadminProfilePermission() {
        $this->insert('auth_item', ['name' => 'sysadmin', 'type' => 1, 'description' => 'Administrador do sistema']);
        $this->insert('auth_assignment', ['item_name' => 'sysadmin', 'user_id' => 1]);
    }
    
    private function _insertGwUser() : void {
        $this->insert('user', ['is_administrator' => 1, 'name' => 'Edfy', 'cpf' => '00000000000', 'email' => 'atendimento@edfy.com.br', 'password' => '$2y$13$VAtVDTPrK28YBrMkR7EdE.w0jNtdxpxoHG8U7xn/tJ9JJXCyNXzxm']);
    }
    
    //endregion
    
    //region Rollback
    
    private function _dropForeignKeys() : void {
        $this->dropForeignKey('FK_auth_assignment-item_name_auth_item-name', 'auth_assignment');
        $this->dropForeignKey('FK_auth_item-rule_name_auth_rule-name', 'auth_item');
        $this->dropForeignKey('FK_auth_item_child-child_auth_item-name', 'auth_item_child');
        $this->dropForeignKey('FK_auth_item_child-parent_auth_item-name', 'auth_item_child');
        $this->dropForeignKey('FK_token-user_id_user-id', 'token');
        $this->dropForeignKey('FK_user_client-user_id_user-id', 'user_client');
    }
    
    private function _dropIndex() : void {
        $this->dropIndex('idx_auth_item-rule_name', 'auth_item');
        $this->dropIndex('idx_auth_item-type', 'auth_item');
        $this->dropIndex('idx_auth_item_child-child', 'auth_item_child');
        $this->dropIndex('idk_token-user_id', 'token');
        $this->dropIndex('idx_user_client-user_id', 'user_client');
    }
    
    private function _dropTables() : void {
        $this->dropTable('auth_item');
        $this->dropTable('auth_rule');
        $this->dropTable('auth_assignment');
        $this->dropTable('auth_item_child');
        $this->dropTable('user_client');
        $this->dropTable('user');
        $this->dropTable('log');
        $this->dropTable('page');
        $this->dropTable('token');
        $this->dropTable('translation');
    }
    
    //endregion
    
    public function safeUp() {
        $this->_createAuthAssignment();
        $this->_createAuthItem();
        $this->_createAuthItemChild();
        $this->_createAuthRule();
        $this->_createLog();
        $this->_createPage();
        $this->_createToken();
        $this->_createTranslation();
        $this->_createUser();
        $this->_createUserClient();
        $this->_createForeignKeys();
        
        $this->_insertGwUser();
        $this->_insertSysadminProfilePermission();
        
        $this->_insertHome();
        $this->_insertExit();
        $this->_insertPageAdministrator();
        $this->_insertPageUserClient();
        $this->_insertPageUserDriver();
        $this->_insertPageTrip();
        $this->_insertPageApp();
        $this->_insertPageNotification();
        $this->_insertPageOrder();
        $this->_insertPagePage();
        $this->_insertPageRbac();
        $this->_insertPageTranslation();
        $this->_insertPageLog();
    }
    
    public function safeDown() {
        if ($_ENV['DB_HOST'] === 'localhost' || $_ENV['DB_HOST'] === '192.168.2.3') {
            $this->_dropForeignKeys();
            $this->_dropIndex();
            $this->_dropTables();
            $tableName = $this->db->tablePrefix . 'users';
            if (!$this->db->getTableSchema($tableName, TRUE) === NULL) {
                $this->dropTable('migration');
            }
        } else {
            echo "===================== ATTENTION ======================";
            echo " Rollback is not applicable in production environment.";
            echo "======================================================";
        }
    }
}
