<?php

namespace api\modules\user\controllers;

use api\components\utils\BaseController;
use common\modules\user\models\LoginForm;
use Dersonsena\JWTTools\JWTSignatureBehavior;
use Dersonsena\JWTTools\JWTTools;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;


class UserController extends BaseController {
    public $modelClass = 'common\modules\user\models\User';
    
    public function behaviors() {
        $behaviors['jwtValidator'] = [
            'class' => JWTSignatureBehavior::class,
            'secretKey' => $_ENV['JWT_TOKEN_SECRET'],
            'except' => ['authentication', 'index','teste'],
        ];
        
        $behaviors = ArrayHelper::merge($behaviors, parent::behaviors());
        return $behaviors;
    }
    
    /**
     * @SWG\Post(
     *    path = "/user/authentication",
     *    tags = {"User"},
     *    operationId = "userDevice",
     *     summary="Send the parameters to receive a token. This token is used on all other endpoints.",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	@SWG\Parameter(
     *        in = "body",
     *        name = "body",
     *        required = true,
     *        type = "string",
     *       @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="useremail@mail.com"),
     *              @SWG\Property(property="password", type="string", example="!CW@4d31bI"),
     *          )
     *    ),
     *	@SWG\Response(response = 200, description = "success")
     *)
     */
    public function actionAuthentication() {
        $data  = $this->jsonData;
        
        $model = new LoginForm();
        $model->email = $data['email'];
        $model->password = $data['password'];
        if ($model->validate()) {
            $authenticationService = Yii::$app->getModule('adminGw')->get('authenticationService');
            $user = $authenticationService->loginApp($model);
            if ($user) {
                $token = JWTTools::build($_ENV['JWT_TOKEN_SECRET'])
                    ->withModel($user, ['id', 'name', 'email', 'cpf'])
                    ->getJWT();
                return ['token' => $token];
            }
        }
        $code = new UnauthorizedHttpException();
        return $this->error( $code->getName(), $code->statusCode);
    }
}









