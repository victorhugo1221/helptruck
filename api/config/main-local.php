<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => 'H90CyJvMG0EOCTenhvxeOlQ2-6IwJUIT',
        ],
    ],
];

if (!YII_ENV_TEST) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
