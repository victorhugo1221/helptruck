<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'user' => '/user/user',
    ],
    'extraPatterns' => [
        'OPTIONS authentication' => 'authentication',
        'POST authentication' => 'authentication',
    ],
];
