<?php
return [
    'enablePrettyUrl' => TRUE,
    'enableStrictParsing' => TRUE,
    'showScriptName' => FALSE,
    'rules' => [
        require __DIR__ . '/routes/user.php',
    ],
];