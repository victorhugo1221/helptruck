<?php

use Codeception\Util\HttpCode;

class AuthenticationCest {
    public function tryLoginSucess(ApiTester $I) {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/authentication', ['email' => 'luciano@grupow.com.br', 'password' => 'x']);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }
    
    public function tryLoginWithoutEmail(ApiTester $I) {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/authentication', ['email' => '', 'password' => 'x']);
        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
    }
    
    public function tryLoginWithoutPassword(ApiTester $I) {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/authentication', ['email' => 'atendimento@grupow.com.br', 'password' => '']);
        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
    }
    
    public function tryLoginWithoutEmailAndPassword(ApiTester $I) {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/authentication', ['email' => '', 'password' => '']);
        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
    }
    
    public function tryLoginWithInvalidEmail(ApiTester $I) {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('user/authentication', ['email' => 'umemailinexistente@grupow.com', 'password' => 'x']);
        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
    }
}
