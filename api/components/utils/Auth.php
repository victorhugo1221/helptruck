<?php

namespace api\components\utils;

use common\modules\user\models\User;
use Dersonsena\JWTTools\JWTTools;
use common\modules\user\models\UserClient;
use Exception;

trait Auth {

    public static function generateJWT(User $user): string {
        $token = JWTTools::build($_ENV['JWT_TOKEN_SECRET'],
            [
                'iss'        => $_ENV['JWT_ISSUER'],
                'aud'        => $_ENV['JWT_AUDIENCE'],
                'expiration' => $_ENV['JWT_DURATION_SECONDS']
            ]
        );
        $token->withModel($user, ['id', 'name', 'email', 'cpf']);
        if($user->id > 0){
            $userClient = UserClient::find()->andWhere(['user_id' => $user->id])->one();
            $token->getPayload()->addExtraAttribute("profileId", $userClient->profile_id);
        }
        return $token->getJWT();
    }

    public static function refreshJWT(int $id): string {
        $token = JWTTools::build($_ENV['JWT_TOKEN_SECRET'],
            [
                'iss'        => $_ENV['JWT_ISSUER'],
                'aud'        => $_ENV['JWT_AUDIENCE'],
                'expiration' => $_ENV['JWT_REFRESH_DURATION_SECONDS']
            ]
        );
        $token->getPayload()->addExtraAttribute('id', $id);
        return $token->getJWT();
    }

    public static function decryptToken(string $token) {
        try {
            $decodedToken = JWTTools::build($_ENV['JWT_TOKEN_SECRET'])->decodeToken($token);
            return $decodedToken;
        } catch (Exception $e) {
            return FALSE;
        }
    }
}
