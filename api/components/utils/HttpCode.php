<?php


namespace api\components\utils;


class HttpCode {
    public const CONTINUE_100 = 100;
    public const SWITCHING_PROTOCOLS_101 = 101;
    public const PROCESSING_102 = 102;
    
    public const OK_200 = 200;
    public const CREATED_201 = 201;
    public const ACCEPTED_202 = 202;
    public const NON_AUTHORITATIVE_INFORMATION_203 = 203;
    public const NO_CONTENT_204 = 204;
    public const RESET_CONTENT_205 = 205;
    public const PARTIAL_CONTENT_206 = 206;
    public const MULTI_STATUS_207 = 207;
    public const ALREADY_REPORTED_208 = 208;
    public const IM_USED_226 = 226;
    
    public const MULTIPLE_CHOICES_300 = 300;
    public const MOVED_PERMANENTLY_301 = 301;
    public const FOUND_302 = 302;
    public const SEE_OTHER_303 = 303;
    public const NOT_MODIFIED_304 = 304;
    public const USE_PROXY_305 = 305;
    public const RESERVED_306 = 306;
    public const TEMPORARY_REDIRECT_307 = 307;
    public const PERMANENTLY_REDIRECT_308 = 308;
    
    public const BAD_REQUEST_400 = 400;
    public const UNAUTHORIZED_401 = 401;
    public const PAYMENT_REQUIRED_402 = 402;
    public const FORBIDDEN_403 = 403;
    public const NOT_FOUND_404 = 404;
    public const METHOD_NOT_ALLOWED_405 = 405;
    public const NOT_ACCEPTABLE_406 = 406;
    public const PROXY_AUTHENTICATION_REQUIRED_407 = 407;
    public const REQUEST_TIMEOUT_408 = 408;
    public const CONFLICT_409 = 409;
    public const GONE_410 = 410;
    public const LENGTH_REQUIRED_411 = 411;
    public const PRECONDITION_FAILED_412 = 412;
    public const REQUEST_ENTITY_TOO_LARGE_413 = 413;
    public const REQUEST_URI_TOO_LONG_414 = 414;
    public const UNSUPPORTED_MEDIA_TYPE_415 = 415;
    public const REQUESTED_RANGE_NOT_SATISFIABLE_416 = 416;
    public const EXPECTATION_FAILED_417 = 417;
    public const I_AM_A_TEAPOT_418 = 418;
    public const UNPROCESSABLE_ENTITY_422 = 422;
    public const LOCKED_423 = 423;
    public const FAILED_DEPENDENCY_424 = 424;
    public const RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL_425 = 425;
    public const UPGRADE_REQUIRED_426 = 426;
    public const PRECONDITION_REQUIRED_428 = 428;
    public const TOO_MANY_REQUESTS_429 = 429;
    public const REQUEST_HEADER_FIELDS_TOO_LARGE_431 = 431;
    
    public const INTERNAL_SERVER_ERROR_500 = 500;
    public const NOT_IMPLEMENTED_501 = 501;
    public const BAD_GATEWAY_502 = 502;
    public const SERVICE_UNAVAILABLE_503 = 503;
    public const GATEWAY_TIMEOUT_504 = 504;
    public const VERSION_NOT_SUPPORTED_505 = 505;
    public const VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL_506 = 506;
    public const INSUFFICIENT_STORAGE_507 = 507;
    public const LOOP_DETECTED_508 = 508;
    public const NOT_EXTENDED_510 = 510;
    public const NETWORK_AUTHENTICATION_REQUIRED_511 = 511;
}