<?php

$vendorDir = dirname(__DIR__);

return array (
  'codemix/yii2-localeurls' => 
  array (
    'name' => 'codemix/yii2-localeurls',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@codemix/localeurls' => $vendorDir . '/codemix/yii2-localeurls',
    ),
  ),
  'dersonsena/yii2-jwt-tools' => 
  array (
    'name' => 'dersonsena/yii2-jwt-tools',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@Dersonsena/JWTTools' => $vendorDir . '/dersonsena/yii2-jwt-tools/src',
    ),
  ),
  'lavrentiev/yii2-toastr' => 
  array (
    'name' => 'lavrentiev/yii2-toastr',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@lavrentiev/widgets/toastr' => $vendorDir . '/lavrentiev/yii2-toastr/src',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'moonlandsoft/yii2-tinymce' => 
  array (
    'name' => 'moonlandsoft/yii2-tinymce',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@moonland/tinymce' => $vendorDir . '/moonlandsoft/yii2-tinymce',
    ),
  ),
  'yii2mod/yii2-swagger' => 
  array (
    'name' => 'yii2mod/yii2-swagger',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii2mod/swagger' => $vendorDir . '/yii2mod/yii2-swagger',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.18.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
);
